\documentclass[dvipsnames]{beamer}

\input{talkPreambleNoBeamer.tex}
\mode<presentation>
{
  \usetheme{Warsaw}
  \useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}
\addbibresource{presentations.bib}

\title[CHREST]{CHREST: Numerical Libraries}
\author[M.~Knepley]{\LARGE Matthew~Knepley}
\date[]{}
% - Use the \inst command if there are several affiliations
%\institute[Buffalo]{
%  Computer Science and Engineering\\
%  University at Buffalo
%}
\subject{Rockets}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \vspace*{-2em}
  \begin{center}\includegraphics[width=0.8\textwidth]{figures/logos/CHRESTLogo.png}\end{center}
\end{frame}
%
%\section{Focused Technical Discussion: Numerical Libraries}
% 20 min
% It seems that you are heavily reliant on libraries for performance; why do you believe that PETSc is an appropriate CS framework for the proposed work? (20 min.)
% PrepLibraryDev.tex
% SPEAK: MPI does for this for machines and networks
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Hardware Details

\only<2->{\begin{textblock}{0.25}[0.0,0.0](0.05,0.50)\includegraphics[width=\textwidth]{figures/Hardware/MacAir.jpg}\end{textblock}}
\only<3->{\begin{textblock}{0.25}[0.0,0.0](0.30,0.60)\includegraphics[width=\textwidth]{figures/Hardware/NvidiaC2070.jpeg}\end{textblock}}
\only<4>{\begin{textblock}{0.60}[0.0,0.0](0.58,0.55)\includegraphics[width=\textwidth]{figures/Hardware/ALCF-Theta_111016-1000px-225x153.jpg}\end{textblock}}

\end{frame}
% SPEAK: Matrix and Krylov classes hide architectural optimizations
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Implementation Complexity

\only<2->{\begin{textblock}{0.40}[0.0,0.0](0.05,0.45)\includegraphics[width=\textwidth]{figures/FEM/GPU/threadTranspose2.png}\end{textblock}}
\only<3->{\begin{textblock}{0.35}[0.0,0.0](0.80,0.45)\includegraphics[width=\textwidth]{figures/Mat/BLISLoops.pdf}\end{textblock}}
\only<4>{\begin{textblock}{0.85}[0.0,0.0](0.20,0.65)
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L0_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L1_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L2_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L3_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L4_EV1.png}\end{textblock}}

\end{frame}
% SPEAK: Users implementing their own algorithms will not typically be aware of implementation nuances arrived at through decades of experience.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Accumulate\\
\hskip1em Best Practices

\bigskip

\begin{itemize}\Large
  \item<2->[] Classical Gram-Schmidt orthogonalization with selective reorthogonalization
  \medskip
  \item<3->[] Eigen-estimation for AMG with Krylov bootstrap
\end{itemize}

\medskip

\uncover<4>{\begin{center}\Large Improvement without code changes\end{center}}
\end{frame}
% SPEAK: Libraries greatly simplify the process of building, maintaining, and extending a simulation code. A coherent V\&V strategy demands that we can completely reconstruct the exact build for any computation.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Simplfy\\
\hskip1em Determining Provenance

\bigskip
\bigskip

\Large
\begin{overprint}
\onslide<2>
Rather than making build-time choices,
\onslide<3>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels
\onslide<4>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels\\
\hspace*{2em} with another layer of workflow scripts\\
\onslide<5>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels\\
\hspace*{2em} with another layer of workflow scripts\\
\hspace*{2em} and brittle top-level interfaces,
\onslide<6>\Large
we use packages without modification,
\onslide<7>\Large
we use packages without modification,\\
\hspace*{1em} compiled in a standard way
\onslide<8>\Large
we use packages without modification,\\
\hspace*{1em} compiled in a standard way\\
\hspace*{2em} and controlled entirely via runtime options.
\end{overprint}
\end{frame}
%
% SPEAK: Early Numerical Libraries
%    71 Handbook for Automatic Computation: Linear Algebra, J. H. Wilkinson and C. Reinch
%    73 EISPACK, Brian Smith et.al.
%    79 BLAS, Lawson, Hanson, Kincaid and Krogh
%    90 LAPACK, many contributors 91 PETSc, Gropp and Smith 95 MPICH, Gropp and Lusk
%       All of these packages had their genesis at Argonne National Laboratory/MCS
\begin{frame}{Why PETSc?}
\Large
\setbeamercovered{transparent}

\begin{itemize}
  \item<1-> Dependability \& Maintainability
  \visible<2>{\begin{itemize}
    \item Nearly 30 years of continuous development
  \end{itemize}}
  \item<3-> Portability \& Robustness
  \visible<4>{\begin{itemize}
    \item Tested at every supercomputer installation and 10,000+ users
  \end{itemize}}
  \item<5-> Performance \& Scalability
  \visible<6>{\begin{itemize}
    \item Many Gordon Bell Winners
  \end{itemize}}
  \item<7-> Optimality \& Robustness
  \visible<8>{\begin{itemize}
    \item State-of-the-Art Linear and Nonlinear Solvers, Eigensolvers, Optimization Solvers, Timesteppers
  \end{itemize}}
  \item<9-> Flexibility \& Extensibility
  \visible<10>{\begin{itemize}
    \item More than 8000 citations and hundreds of application packages
    \item Aerodynamics, Arterial Flow, Corrosion, Combustion, Data Mining, Earthquake Mechanics, \ldots
  \end{itemize}}
\end{itemize}
\end{frame}
\end{document}
