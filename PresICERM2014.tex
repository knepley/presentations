\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{mathtools}       % For \mathllap
\usepackage[boxed]{algorithm}
\usepackage{algpseudocode}   % For \Procedure on algorithm

\title[NPC]{Nonlinear Preconditioning in PETSc}
\author[M.~Knepley]{Matthew~Knepley {\bf$\in$} PETSc Team}
\date[ICERM '14]{Challenges in 21st Century Experimental Mathematical Computation\\ICERM, Providence, RI \qquad July 22, 2014}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago
}
\subject{PETSc}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{abstract}

Title: Nonlinear Preconditioning in PETSc

Abstract:
A common pitfall in large, complex simulations is a failure of convergence in the nonlinear solver, typically Newton's
method or partial linearization with Picard's method. We present a framework for nonlinear preconditioning, based upon
the composition of nonlinear solvers. We can define both left and right variants of the preconditioning operation, in
analogy to the linear case. Although we currently lack a convincing convergence theory, we will present a powerful and
composable interface for these algorithms, as well as examples which demonstrate their effectiveness.
\end{frame}
%
\input{slides/PETSc/PETScDevelopers.tex}
%
\begin{frame}{Why Experiment with Solvers?}\Large
\begin{itemize}
  \item<1-> Asymptotic convergence rate
  \begin{itemize}
    \item Rarely get asymptotics in practice %see Magma example

    \item Really want the constant
  \end{itemize}
  \medskip
  \item<2-> Manner of convergence
  \begin{itemize}
    \item Stationary iterative methods decrease high frequency error quickly %means solver works locally

    \item \magenta{\href{http://www.cs.sandia.gov/~rstumin/backtrack.pdf}{Tuminaro, Walker, Shadid, JCP, 180, pp. 549-558 (2002).}}
  \end{itemize}
  \medskip
  \item<3> Robustness % convergence at all
  \begin{itemize}
    \item Line search

    \item Solver composition
  \end{itemize}
\end{itemize}
\end{frame}

\section{Algorithmics}
%
\input{slides/SNES/BasicSystem.tex}
\input{slides/SNES/LeftPrec.tex}
%%\input{slides/SNES/NPrecAdditions.tex}
\input{slides/SNES/AdditiveNPrec.tex}
\input{slides/SNES/LeftNPrec.tex}
\input{slides/SNES/MultiplicativeNPrec.tex}
\input{slides/SNES/RightNPrec.tex}
\input{slides/SNES/NPrecTable.tex}
%
\input{slides/SNES/NRICH.tex}
\input{slides/SNES/NRICH-LP.tex}
%%\input{slides/SNES/LineSearchVariants.tex}
%%\input{slides/SNES/NGMRES.tex}
\input{slides/SNES/NK.tex}
\input{slides/SNES/LP-NK.tex}
\input{slides/SNES/RP-NK.tex}
%%\input{slides/SNES/FAS.tex}
%
%
\section{Experiments}
%
\subsection{Composition}
\input{slides/SNES/Ex16Equations.tex}
\input{slides/SNES/SNESEx16.tex}
\input{slides/SNES/NPCEx16.tex}
%
\subsection{Multilevel}
\input{slides/SNES/Ex19Equations.tex}
\input{slides/SNES/SNESEx19.tex}
\input{slides/SNES/NPCEx19.tex}

\subsection{Magma Dynamics}
\input{slides/Magma/Overview.tex}
\input{slides/Magma/Equations.tex}
\input{slides/Magma/NewtonConvergence.tex}
\input{slides/Magma/NCGNewtonConvergence.tex}
\input{slides/Magma/FASConvergence.tex}
%
\begin{frame}\LARGE
  See discussion in:\\
  \vskip1em
  {\Large\bf Composing scalable nonlinear solvers},\\
  {\large Peter Brune, Matthew Knepley, Barry Smith, and Xuemin Tu,\\
  ANL/MCS-P2010-0112, Argonne National Laboratory, 2012.}\\
  \magenta{\normalsize\href{http://www.mcs.anl.gov/uploads/cels/papers/P2010-0112.pdf}{http://www.mcs.anl.gov/uploads/cels/papers/P2010-0112.pdf}}
\end{frame}
%
\begin{frame}{What Are We Missing?}\Large

We need a short time convergence theory:
\medskip
\begin{itemize}
  \item Most iterations never enter the asymptotic regime
  \smallskip
  \item Most complex solvers are composed
\end{itemize}
\bigskip
We need a viable nonlinear smoother:
\medskip
\begin{itemize}
  \item GS is too expensive for FEM
  \smallskip
  \item NASM is a possibility,
\end{itemize}
\end{frame}
%
\subsection*{Extra}
\input{slides/SNES/NGMRES.tex}
\input{slides/SNES/FAS.tex}
%
\begin{frame}{Other Nonlinear Solvers}\Large
\setlength{\leftmargini}{3em}
\begin{itemize}
  \item[NASM] Nonlinear Additive Schwarz
  \bigskip
  \item[NGS]  Nonlinear Gauss-Siedel
  \bigskip
  \item[NCG]  Nonlinear Conjugate Gradients
  \bigskip
  \item[QN]   Quasi-Newton methods
\end{itemize}
\end{frame}

\end{document}
