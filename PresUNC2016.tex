\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}

\title[Solvation]{Improved Solvation Models using\\Boundary Integral Equations}
\author[M.~Knepley]{Matthew~Knepley and Jaydeep~Bardhan}
\date[UNC6]{Applied Mathematics Colloquium\\Department of Mathematics\\UNC Chapel Hill \quad September 16, 2016}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{SLIC}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}\small
Solvation mechanics is concerned with the, mostly electrostatic, interaction of solute molecules, such as
biomolecules essential for life, with solvent molecules, water and sometimes ions. The action of solvent molecules in
the thin solvation layer around solute molecules is a key determinant of behavior. We will present a range of models of
varying fidelity for this process, including thermodynamic measures, compare to existing approaches, and discuss the
usefulness of these approaches.

The idea is to show this work as an evolution of a model. First, we explore how simple we can make it, then we use the
same techniques (playing with BC) to make it more realistic.
\end{frame}
%
\begin{frame}{Main Point}

\Huge
Solvation computation\\
\qquad can benefit from\\
\bigskip
\pause
operator simplification,\\
\bigskip
\pause
and non-Poisson models.
\end{frame}
%
\input{slides/Bioelectrostatics/Lysozyme.tex}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/SecondKindModel.tex}
%
%
\section{Approximating the Poisson Operator}
%
\begin{frame}{Problem}\Large

Boundary element discretizations of solvation:
\bigskip
\begin{itemize}
  \item can be expensive to solve
  \bigskip
  \item are more accurate than required by intermediate design iterations
\end{itemize}
\end{frame}
%
\subsection{Approximate Operators}
\input{slides/Bioelectrostatics/GeneralizedBorn.tex}
\input{slides/Bioelectrostatics/GBProblems.tex}
\input{slides/Bioelectrostatics/ReactionPotentialDefinition.tex}
\input{slides/Bioelectrostatics/BIBEEIdea.tex}
\input{slides/Bioelectrostatics/BIBEEBoundsStatement.tex}
\input{slides/Bioelectrostatics/BIBEEBoundsProof.tex}
\input{slides/Bioelectrostatics/BIBEEAccuracy.tex}
\input{slides/Bioelectrostatics/CrowdedSolution.tex}
\input{slides/Bioelectrostatics/BIBEEScalability.tex}
%
\subsection{Approximate Boundary Conditions}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/KirkwoodSolution.tex}
\input{slides/Bioelectrostatics/BIBEEBCStatement.tex}
\input{slides/Bioelectrostatics/BIBEEBCProof.tex}
\input{slides/Bioelectrostatics/BIBEEBCSeries.tex}
\input{slides/Bioelectrostatics/BIBEEBCAsymptotics.tex}
\input{slides/Bioelectrostatics/BIBEEInterpolated.tex}
\input{slides/Bioelectrostatics/BasisAugmentation.tex}
%
\begin{frame}{Resolution}\Large

\note[item]{Between 10 and 100x faster}
Boundary element discretizations of the solvation problem:
\bigskip
\begin{itemize}
  \item can be expensive to solve
  \smallskip
  \begin{itemize}
    \item<2-> {\scriptsize \magenta{\bf\href{http://jcp.aip.org/resource/1/jcpsa6/v130/i10/p104108_s1}{Bounding the
          electrostatic free energies associated with linear continuum models of molecular solvation}}, Bardhan,
      Knepley, Anitescu, JCP, 2009}
  \end{itemize}
  \bigskip
  \item are more accurate than required by intermediate design iterations
  \smallskip
  \begin{itemize}
    \item<3-> {\scriptsize
      \magenta{\bf\href{http://www.degruyter.com/view/j/mlbmb.2012.1.issue/mlbmb-2013-0007/mlbmb-2013-0007.xml?format=INT}{Analysis
          of fast boundary-integral approximations for modeling electrostatic contributions of molecular binding}}, Kreienkamp,
      et al., Molecular-Based Mathematical Biology, 2013}
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section{Improving the Poisson Operator}
%
\input{slides/Bioelectrostatics/OriginElectrostaticAsymmetry.tex}
\input{slides/Bioelectrostatics/SLICIdea.tex}
\input{slides/Bioelectrostatics/SLICModel.tex}
%
\begin{frame}{Accuracy of SLIC}{Residues}
  \begin{center}\includegraphics[height=0.85\textheight]{figures/Bioelectrostatics/residuesSLIC}\end{center}
\end{frame}
%
\begin{frame}{Accuracy of SLIC}{Protonation}
  \begin{center}\includegraphics[height=0.85\textheight]{figures/Bioelectrostatics/protonationSLIC}\end{center}
\end{frame}
%
\begin{frame}{Accuracy of SLIC}{Synthetic Molecules}
  \begin{center}\includegraphics[height=0.85\textheight]{figures/Bioelectrostatics/mobleyLinearSLIC}\end{center}
\end{frame}
%
\begin{frame}{Accuracy of SLIC}{Synthetic Molecules}
  \begin{center}\includegraphics[height=0.85\textheight]{figures/Bioelectrostatics/mobleyRing1SLIC}\end{center}
\end{frame}
%
\begin{frame}{Accuracy of SLIC}{Synthetic Molecules}
  \begin{center}\includegraphics[height=0.85\textheight]{figures/Bioelectrostatics/mobleyRing2SLIC}\end{center}
\end{frame}
%
\begin{frame}{Thermodynamics}
  The parameters show linear temperature dependence
  \begin{center}\includegraphics[height=0.9\textheight]{figures/Bioelectrostatics/alphaTempDepSLIC}\end{center}
\end{frame}
% TODO Derivation from MSA
%
\begin{frame}{Model Validation}{Courtesy A. Molvai Tabrizi}
%  TODO Convert tables into LaTeX tables
  \includegraphics[width=\textwidth]{figures/Bioelectrostatics/validationSolventsSLIC}
\end{frame}
%
\begin{frame}{Model Validation}{Courtesy A. Molvai Tabrizi}
%  TODO Convert tables into LaTeX tables
  \includegraphics[width=\textwidth]{figures/Bioelectrostatics/validationSetupSLIC}
\end{frame}
%
\begin{frame}{Model Validation}{Courtesy A. Molvai Tabrizi}
%  TODO Convert tables into LaTeX tables
  \includegraphics[width=\textwidth]{figures/Bioelectrostatics/PropyleneCarbonateSLIC}
\end{frame}
%
\begin{frame}{Model Validation}{Courtesy A. Molvai Tabrizi}
%  TODO Convert tables into LaTeX tables
  \includegraphics[width=\textwidth]{figures/Bioelectrostatics/DimethylFormamideSLIC}
\end{frame}
%
\begin{frame}{Model Validation}{Courtesy A. Molvai Tabrizi}
\Large
  A. Molavi Tabrizi, M.G. Knepley, and J.P. Bardhan,\\
  \textit{Generalising the mean spherical approximation as a multiscale, nonlinear boundary condition at the solute-solvent interface},\\
  Molecular Physics (2016).
\end{frame}
%
\begin{frame}{Thermodynamic Predictions}{Courtesy A. Molvai Tabrizi}
  \begin{center}\includegraphics[width=\textwidth]{figures/Bioelectrostatics/solvationThermoPrediction}\\Experimental Data in Parentheses\end{center}
\end{frame}
\begin{frame}{Thermodynamic Predictions}{Courtesy A. Molvai Tabrizi}
\Large
  A. Molavi Tabrizi, S. Goossens, M.G. Knepley, and J.P. Bardhan,\\
  \textit{Predicting solvation thermodynamics with dielectric continuum theory and a solvation-layer interface condition
    (SLIC).}\\
  Submitted to Journal of Physical Chemistry Letters (2016).
\end{frame}
%
\begin{frame}{Where does SLIC fail?}
\LARGE
\begin{itemize}
  \item Large packing fraction
  \begin{itemize}\Large
    \item No charge oscillation or overcharging
    \item Could use CDFT
  \end{itemize}
  \medskip
  \item No dielectric saturation
  \begin{itemize}\Large
    \item Could be possible with different function
  \end{itemize}
  \medskip
  \item No long range correlations
  \begin{itemize}\Large
    \item Use nonlocal dielectric
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section*{Future Work}
%
\begin{frame}{Future Work}

\begin{itemize}\LARGE
  \item More complex solutes
  \medskip
  \item Mixtures
  \medskip
  \item Integration into community code
  \begin{itemize}\Large
    \item \Large Psi4, QChem, APBS
  \end{itemize}
  \medskip
  \item Integrate into conformational search
  \begin{itemize}\Large
    \item \Large Kavrakis Lab at Rice
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}[plain]

\begin{center}
\Huge\bf Thank You!
\end{center}

\bigskip

\begin{center}
\LARGE \magenta{\href{http://www.caam.rice.edu/~mk51}{http://www.caam.rice.edu/\textasciitilde mk51}}
\end{center}
\end{frame}

\end{document}

I am interested in molecular machines. If we look at Karplus' Nobel speech, he was initially very worried that you would
need quantum mechanics to simulate proteins. However, classical was just fine for most things, although they were still
simulating in vacuum, like tinkertoys. Levitt shows that you need a box of water in order to get the true
energies. Warshel shows that things like catalyst interaction, which chemists initially believe is ``steric'' or what we
call quantum, is actually an electrostatic effect. Thus in order to understand a great part of biology, we just need
classical mechanics but with long range fields and a solvent.

Approximating the Poisson Operator

FEP is amazingly successful, but very expensive. So we step back several hundred years, and suppose that the solvent
with its many water molecules can be approximated with a continuum. We take into account the ability of water molecules
to rotate and reorganize around the solute with a dielectric constant $\epsilon$. This works pretty well, but for
billions of timesteps, its still expensive. So Still comes up with an equations that just expresses the energy of
solvation, namely the energy for dropping the solute into the solvent, the total energy minus the self energy of the
solute.
\begin{align*}
  E_{ii} &= \left( \frac{1}{\epsilon_I} - \frac{1}{\epsilon_{II}} \right) \frac{q^2_i}{R_i}
  E_{ij} &= \left( \frac{1}{\epsilon_I} - \frac{1}{\epsilon_{II}} \right) \frac{q_i q_j}{r^2_{ij} + \sqrt{R_i R_j} e^{-\frac{r^2_{ij}}{4 R_i R_j}}}
\end{align*}
We can look at this energy equations another way as
\begin{align}
  q L q = q C A^{-1} B q
\end{align}
The problem, as Jay and I saw it, was that there was a) no good analysis of the error for GB, and b) no path for systematic
improvement. There was only comparing and then tuning for experimental energies. Moreover, there is the seeming physical
inconsistency that the same atoms have different radii in different molecules, or solvents, or temperatures! The central
idea of GB is that most of the energy is in the action of the charge on itself through the dielectric boundary, not on
other charges (the CFA). So they calculate ``self''-energies for each charge and then fit radii to clean up any
crosstalk. The BIBEE method is very similar but replaces the focus on charges with surface elements. The diagonal
approximation of the surface-to-surface operator only considers the effect of a surface panel on itself, but the induced
charge there arises from all the charges and affects all the charges. Thus we have a localization of the effect, but it
is not tied to a single atom. As we can see from the sphere analysis, this can also be interpreted as a decoupling of
the effect of the dielectric jump and the charge distribution by using a separated representation (similar to that which
is so effective for FMM).

We start analyzing this by reducing to the simplest case, a sphere or the Born ion. (We initially proved the general
case and then went to the sphere, which shows that this is a just-so story rather than a history). The Kirkwood series
solution gives the reaction field in terms of the expansion for the Coulomb field of the solute. Now we make use of the
correspondence between boundary integral equations and partial differential equations by noting that the S2S operator
arises from the Maxwell boudnary condition at the molecular surface. Now we can interpret BIBEE as a deformation of the
boundary condition. This admits a series solution, which can be fully replicated with ellipsoidal harmonics, and we get
some insight into the approximation.

Generalizing, we can try to get bounds on the approximation by comparing the eigenvalues of the true and approximate
operators. This is complicated by the fact that A is non-symmetric. However, it is ``quasi-Hermitian'', according to the
celebrated results of Calderon. This allows us to symmetrize the operator and obtain its spectrum.

Improving the Poisson Operator

It turns out that the Poisson model is not all that good for solvation. In fact, GB can be more accurate than Poisson in
many circumstances because its many parameters can be fit to FEP data rather than PB solutions, disgusing the fact that
the underlying PB method is inaccurate.

The Poisson model (or PB) cannot capture physics which arises from the molecular nature of the solvent. For example,
screening limits that arise from steric packing constraints. These can be famously introduced using Classical Denisty
Functional Theory (CDFT). An excellent review by Gillespie~\cite{Gillespie14?}, shows the pointwise constraints on local
density cannot reproduce first order phenomenology, and we need to constrain some kind of local average instead.

In addition, Poisson does not show Charge-Hydration Asymmetry (CHA) or longer range correlations in the solvent. Longer
correlations can be modeled by existing techniques for nonlocal dielectrics. We have been able to push this forward by
formulatiing a completely integral model using double-reciprocity~\cite{BruneKnepleyBardhan2015}. CHA arises from the
internal structure of water, since the hydrogen can approach the molecular surface closer than the oxygen and thus is a
more effective screener. For CHA, we could use CDFT with Thermodynamic Perturbation Theory to encode the fine-structure
of water, however these are notoriously expensive.

Instead we choose to model this with a local correction to the induced charge which is dependent on the local electric
field. We heuristically model this contribution as a simple step function, so that the transmission condition becomes
\begin{align}
  (1 + f) du_2/dn = du_1/dn
\end{align}
where $f$ is a smoothed step function,
\begin{align}
  f(E_n) = \alpha \tanh(\beta E_n + \gamma) + \mu
\end{align}
Explain all parameters from Mol. Phys. paper. Through our correspondence with boundary integral equations, this boundary
conditions gives rise to a diagonal nonlinear integral operator.
\begin{align}
  BIE with SLIC
\end{align}

This correction, which we call the Solvation-Layer Interface Condition (SLIC), can accurately reproduce single ion
charging energies, and also charging free energies from FEP for a dataset of small synthetic molecules frmo Mobley
et.al. Surprisingly, SLIC can also accurate reproduce entropies and heat capacities. This leads us to believe that it
has captured essential parts of the physics, rather than being merely a parameterization.
