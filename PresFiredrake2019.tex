\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}
\addbibresource{presentations.bib}

% Figure this out
\newcounter{ccount}
\newcounter{num2}
\newcounter{num_prev}

\title[KST]{Understanding Multivariate Computation\\using the Kolmogorov Superposition Theorem}
\author[M.~Knepley]{Matthew~Knepley}
\date[FD19]{Firedrake 2019, Durham University\\Durham, UK \quad September 27, 2019}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{KST}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}
%
\begin{frame}<testing>{Abstract}\small
The Curse of Dimensionality constrains computational methods for high-dimensional problems. Many methods to overcome
these constraints, including neural networks, projection-pursuit, radial basis functions, and ridge functions, can be
explained as approximations of the Kolmogorov Superposition Theorem (KST). This theorem proves the existence of a
representation of a multivariate continuous function as the superposition of a small number of univariate
functions. These univariate functions are defined on a series of grids that are refined during the construction
process. Unfortunately, KST is difficult to use directly because the resulting representation is highly nonsmooth. At
best, the functions involved in the superpositions are Lipschitz continuous and not differentiable, and the best known
constructions are merely Holder continuous. We describe the first known algorithm to construct a Lipschitz KST inner
function. The resulting inner function induced by the spatial decomposition is independent of the multivariate function
being represented, depending only on the spatial dimensions of the domain. This construction could potentially be the
basis for understanding compact approximations of high-dimensional functions.
\end{frame}
%
\input{slides/Collaborators/KST.tex}
%
% SPEAK: CofD is a problem when we are approximating multidimensional functions, but embedded in this description is an
% assumption that we often take for granted. This is the idea that the function increase on any neighborhood can be
% bounded. If this is true, I can figure out what neighborhood size I need and approximate by a constant. The number of
% neighborhoods I need grows exponentially, which is CoD. This property of thie function is exactly Lipschitz continuity.
\input{slides/Approximation/CurseOfDimensionality.tex}
%
\begin{frame}{Main Question}
  \begin{center}\Huge
    Do functions of three variables\\
    exist at all?\footnote[frame]{\tiny P\'olya and Szeg\"o, Problems and Theorems of Analysis, 1925 (German), transl. 1945, reprinted 1978}
  \end{center}
\end{frame}
%
%% TODO Slide with other approx strategies
%
\begin{frame}{Nomography}
  \begin{center}\LARGE {\bf Q}: Can any function of three variables be expressed using functions of only two variables?\footnote[frame]{\tiny Hilbert, G\"ottinger Nachrichten, 1900}\end{center}
  \pause
  \begin{center}\Large Ex: Cardano's Formula for roots of a cubic equation\end{center}
  \pause
  \begin{center}\LARGE {\bf A}: Any \textit{continuous} function of three variables can be expressed using
    \textit{continuous} functions of only two variables.\footnote[frame]{\tiny Arnol'd, Dokl. Akad. Nauk SSSR 114:5, 1957}\end{center}
\end{frame}
%
\begin{frame}{Nomography}
  \begin{center}\LARGE {\bf Q}: Can any function of three variables be expressed using only univariate functions and
    addition?\end{center}
  \pause
  \begin{center}\LARGE {\bf A}: Yes\footnote[frame]{\tiny Kolmogorov, Dokl. Akad. Nauk SSSR 114:5, 1957}, for any continuous $f: [0,1]^n \rightarrow \R$
    \Large
    \begin{align*}
      f(x_1,\dots,x_n) = \sum^{2n}_{q=0} \chi\left(\,\sum^n_{p=1} \psi_{p,q}(x_p) \right).
    \end{align*}
  \end{center}
\end{frame}
%
\begin{frame}{What's going on here?}
  \Large\vspace{-1.5cm}
  \begin{align*}
    \tikzmark{f}f(x_1,\dots,x_n) =  \tikzmark{sum2} \sum^{2n}_{q=0} \tikzmark{chi} \chi_q \tikzmark{comp} \left( \, \tikzmark{sum1}\sum^n_{p=1} \tikzmark{psi}\psi_{p,q}(x_p) \right)
  \end{align*}
%
\begin{tikzpicture}[
  remember picture,
  overlay,
  expl/.style={draw=black,fill=riceGrey!20,rounded corners,inner sep=1.5ex,minimum width=9cm},
  % arrow/.style={draw=black,fill=riceGrey!30,line width=3pt,->,>=latex}
  arrow/.style={draw=black,line width=3pt,->,>=latex}
]
\node<2-2>[expl]
  (fex)
  at (6,-2.5cm)
  {Original function $f : [0,1]^n \rightarrow \R$};
\node<3-3>[expl]
  (psiex)
  at (6,-2.5cm)
  {Inner function $\psi_{p,q} : [0,1] \rightarrow \R$};
\node<4-4>[expl]
  (sumex)
  at (6,-2.5cm)
  {Addition};
\node<5-5>[expl]
  (chiex)
  at (6,-2.5cm)
  {Outer function $\chi: \R \rightarrow \R$};
\node<6-6>[expl]
  (compex)
  at (6,-2.5cm)
  {Function composition};
\draw<2-2>[draw=black, line width=3pt]
  (fex) to[out=90,in=270] ([xshift=1.35cm,yshift=-2ex]{pic cs:f});
\draw<2-2>[draw=black,line width =3pt]
  ([xshift=0cm,yshift=-2ex]{pic cs:f}) -- ([xshift=2.7cm,yshift=-2ex]{pic cs:f});
\draw<3-3>[draw=black,line width=3pt]
  (psiex) to[out=90,in=270] ([xshift=3.25ex,yshift=-2ex]{pic cs:psi});
\draw<3-3>[draw=black,line width =3pt]
  ([xshift=0ex,yshift=-2ex]{pic cs:psi}) -- ([xshift=6.5ex,yshift=-2ex]{pic cs:psi});
\draw<4-4>[arrow]
  (sumex) to[out=90,in=270] ([xshift=2ex,yshift=-3.5ex]{pic cs:sum1});
\draw<4-4>[arrow]
  (sumex) to[out=90,in=270] ([xshift=2ex,yshift=-3.5ex]{pic cs:sum2});
\draw<5-5>[arrow]
  (chiex) to[out=90,in=280] ([xshift=1ex,yshift=-1.6ex]{pic cs:chi});
% \draw<6-6>[arrow]
%   (compex) to[out=90,in=220] ([xshift=1.1ex,yshift=-2.5ex]{pic cs:comp});
\draw<6-6>[draw=black,line width=3pt]
  (compex) to[out=90,in=270] ([xshift=0.35cm,yshift=-4.5ex]{pic cs:psi});
\draw<6-6>[line width=3pt]
  ([xshift=-1.3cm,yshift=-4.5ex]{pic cs:psi}) -- ([xshift=2cm,yshift=-4.5ex]{pic cs:psi});
\end{tikzpicture}
\end{frame}
%
\begin{frame}{2D: $n = 2$}\Large
  \begin{overprint}
    \onslide<1>
    \begin{alignat*}{2}
      f(x, y) &= \chi_0 \big(\psi_{x,0}(x) &&{}+ \psi_{y,0}(y)\big) \\
              &+ \chi_1 \big(\psi_{x,1}(x) &&{}+ \psi_{y,1}(y)\big) \\
              &+ \chi_2 \big(\psi_{x,2}(x) &&{}+ \psi_{y,2}(y)\big) \\
              &+ \chi_3 \big(\psi_{x,3}(x) &&{}+ \psi_{y,3}(y)\big) \\
              &+ \chi_4 \big(\psi_{x,4}(x) &&{}+ \psi_{y,4}(y)\big)
    \end{alignat*}
    \onslide<2>
    \begin{alignat*}{2}
      f(x, y) &= \chi_0 \big(\lambda_x \psi(x)             &&{}+ \lambda_y \psi(y)\big) \\
              &+ \chi_1 \big(\lambda_x \psi(x + \epsilon)  &&{}+ \lambda_y \psi(y + \epsilon)\big) \\
              &+ \chi_2 \big(\lambda_x \psi(x + 2\epsilon) &&{}+ \lambda_y \psi(y + 2\epsilon)\big) \\
              &+ \chi_3 \big(\lambda_x \psi(x + 3\epsilon) &&{}+ \lambda_y \psi(y + 3\epsilon)\big) \\
              &+ \chi_4 \big(\lambda_x \psi(x + 4\epsilon) &&{}+ \lambda_y \psi(y + 4\epsilon)\big)
    \end{alignat*}

    \bigskip

    \begin{center}Single $\psi$ function (Sprecher)\end{center}
    \onslide<3>
    \begin{alignat*}{3}
      f(x, y) &= \chi \big(\lambda_x \psi(x)             &&{}+ \lambda_y \psi(y)\big) &&\\
              &+ \chi \big(\lambda_x \psi(x + \epsilon)  &&{}+ \lambda_y \psi(y + \epsilon)  &&{}+ \delta\big) \\
              &+ \chi \big(\lambda_x \psi(x + 2\epsilon) &&{}+ \lambda_y \psi(y + 2\epsilon) &&{}+ 2\delta\big) \\
              &+ \chi \big(\lambda_x \psi(x + 3\epsilon) &&{}+ \lambda_y \psi(y + 3\epsilon) &&{}+ 3\delta\big) \\
              &+ \chi \big(\lambda_x \psi(x + 4\epsilon) &&{}+ \lambda_y \psi(y + 4\epsilon) &&{}+ 4\delta\big)
    \end{alignat*}

    \bigskip

    \begin{center}Single $\chi$ function (Lorentz) and $\psi$ function (Sprecher)\end{center}
  \end{overprint}
\end{frame}
%
\begin{frame}{Universality}
  \begin{center}\LARGE
  \begin{align*}
    \Psi^q(x_1,\dots,x_n) = \sum_{p=1}^n \psi_{p,q}(x_p)
  \end{align*}

  \bigskip

  \Huge is independent of $f$, so that
  \vspace{-0.25em}
  \begin{align*}
    KST : f \to \chi
  \end{align*}
  \end{center}
\end{frame}
%
\begin{frame}{Simple Example}\Huge
\begin{align*}
  x y = \exp^{\ln x + \ln y}
\end{align*}

\bigskip

\begin{overprint}
\onslide<2>
\begin{center}
  Modulus of continuity of $exp$\\compensates for flatness of $ln$
\end{center}
\onslide<3>
\begin{center}
  Controlling regularity of $\phi$\\is the key to efficiency
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}{Regularity of Inner Functions}\LARGE

\begin{overprint}
\onslide<1>
  \parencite{Vitushkin1964}\\
  Inner functions $\phi$ cannot be smooth

  \bigskip
  \bigskip

  \begin{center}$\mathcal{O}(n^2)$ cross-derviatives in $f$\end{center}

  \bigskip

  \begin{center}$\mathcal{O}(n)$ derivatives in $\phi$ and $\chi$\end{center}
\onslide<2>
  \parencite{DiaconisShahshahani1984}\\
  Exactly characterizes the nullspace\\\quad for projection pursuit %proto-wavelet

  \bigskip
  \bigskip

  \begin{align*}
    f(x_1,\ldots,x_n) \approx \sum^Q_{q=1} \chi_q\left(\,\sum^n_{p=1} \lambda_{pq} x_p \right)
  \end{align*}
\onslide<3>
  \parencite{Sprecher1965}\\
  Explicitly constructs a $\phi$\\\quad that is  H\"older$\left(\frac{\ln 2}{\ln 2n+2}\right)$

  \bigskip
  \bigskip

  \parencite{BraunGriebel2009}\\
  Demonstrates that such a function\\\quad cannot be computed efficiently

\onslide<4>

  \parencite{Actor2018}\\
  Constructs a Lipschitz(2) $\phi$

  \bigskip
  \bigskip

  \begin{center}A remapping of Sprecher's $\phi$\\\quad that is efficiently computable\end{center}
\end{overprint}
\end{frame}
%
%\begin{frame}{Relation to NN}
%\end{frame}
%
\begin{frame}{General Approximations}\LARGE

\begin{itemize}
  \item Need hierarchy
  \bigskip
  \item Need nonlinearity
  \bigskip
  \item Need to control regularity
\end{itemize}
\end{frame}
%
\begin{frame}{General Approximations}\Huge

  \begin{center}
  Function composition is a\\
  \medskip
  powerful nonlinear mechanism\\
  \medskip
  missing from FEM
  \end{center}

\end{frame}
%
\begin{frame}[plain]

\begin{center}
\Huge\bf Thank You!
\end{center}

\bigskip

\begin{center}
\LARGE \magenta{\href{http://cse.buffalo.edu/~knepley}{http://cse.buffalo.edu/\textasciitilde knepley}}
\end{center}
\end{frame}

\end{document}
