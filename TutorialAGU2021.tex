\documentclass[dvipsnames,18pt]{beamer}

\input{talkPreambleNoBeamer.tex}
\usepackage{physics}
\mode<presentation>
{
  \usetheme{Malmoe}
  \usefonttheme{serif}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamerfont{title}{size=\normalsize}
\setbeamerfont{author}{size=\footnotesize,series=\bfseries}
\setbeamerfont{title page}{size=\scriptsize}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{presentations.bib}
\renewcommand*{\bibfont}{\tiny}

% Figure this out
\newcounter{ccount}
\newcounter{num2}
\newcounter{num_prev}

\title[HPC]{Tools for HPC Simulation: PETSc}
\author[M.~Knepley]{Siddhant~Agarwal, Matthew~Knepley, Henry~Tufo, Gabriele~Morra, Dave~Yuen}
\date[AGU21]{AGU Fall Meeting\\New Orleans, LA \qquad December 12, 2021}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{HPC}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}

% Overall Time: One hour
%
% Time: 10 minutes
\section{Why Libraries?}
%
% SPEAK: MPI does for this for machines and networks
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Hardware Details

\only<2->{\begin{textblock}{0.25}[0.0,0.0](0.05,0.50)\includegraphics[width=\textwidth]{figures/Hardware/MacAir.jpg}\end{textblock}}
\only<3->{\begin{textblock}{0.25}[0.0,0.0](0.30,0.60)\includegraphics[width=\textwidth]{figures/Hardware/NvidiaC2070.jpeg}\end{textblock}}
\only<4>{\begin{textblock}{0.60}[0.0,0.0](0.58,0.55)\includegraphics[width=\textwidth]{figures/Hardware/ALCF-Theta_111016-1000px-225x153.jpg}\end{textblock}}

\end{frame}
%
% SPEAK: Matrix and Krylov classes hide architectural optimizations
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Implementation Complexity

\only<2->{\begin{textblock}{0.40}[0.0,0.0](0.05,0.45)\includegraphics[width=\textwidth]{figures/FEM/GPU/threadTranspose2.png}\end{textblock}}
\only<3->{\begin{textblock}{0.35}[0.0,0.0](0.80,0.45)\includegraphics[width=\textwidth]{figures/Mat/BLISLoops.pdf}\end{textblock}}
\only<4>{\begin{textblock}{0.85}[0.0,0.0](0.20,0.65)
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L0_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L1_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L2_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L3_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L4_EV1.png}\end{textblock}}

\end{frame}
%
% SPEAK: Users implementing their own algorithms will not typically be aware of implementation nuances arrived at through decades of experience.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Accumulate\\
\hskip1em Best Practices

\bigskip

\begin{itemize}\Large
  \item<2->[] Classical Gram-Schmidt orthogonalization with selective reorthogonalization
  \medskip
  \item<3->[] Eigen-estimation for AMG with Krylov bootstrap
\end{itemize}

\medskip

\uncover<4>{\begin{center}\Large Improvement without code changes\end{center}}
\end{frame}
%
% SPEAK: Libraries greatly simplify the process of building, maintaining, and extending a simulation code. A coherent V\&V strategy demands that we can completely reconstruct the exact build for any computation.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Simplfy\\
\hskip1em Determining Provenance

\bigskip
\bigskip

\Large
\begin{overprint}
\onslide<2>
Rather than making build-time choices,
\onslide<3>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels
\onslide<4>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels\\
\hspace*{2em} with another layer of workflow scripts\\
\onslide<5>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels\\
\hspace*{2em} with another layer of workflow scripts\\
\hspace*{2em} and brittle top-level interfaces,
\onslide<6>\Large
we use packages without modification,
\onslide<7>\Large
we use packages without modification,\\
\hspace*{1em} compiled in a standard way
\onslide<8>\Large
we use packages without modification,\\
\hspace*{1em} compiled in a standard way\\
\hspace*{2em} and controlled entirely via runtime options.
\end{overprint}
\end{frame}
%
% SPEAK: Early Numerical Libraries
%    71 Handbook for Automatic Computation: Linear Algebra, J. H. Wilkinson and C. Reinch
%    73 EISPACK, Brian Smith et.al.
%    79 BLAS, Lawson, Hanson, Kincaid and Krogh
%    90 LAPACK, many contributors 91 PETSc, Gropp and Smith 95 MPICH, Gropp and Lusk
%       All of these packages had their genesis at Argonne National Laboratory/MCS
\begin{frame}{Why PETSc?}
\Large
\setbeamercovered{transparent}

\begin{itemize}
  \item<1-> Dependability \& Maintainability
  \visible<2>{\begin{itemize}
    \item Nearly 30 years of continuous development
  \end{itemize}}
  \item<3-> Portability \& Robustness
  \visible<4>{\begin{itemize}
    \item Tested at every supercomputer installation and 10,000+ users
  \end{itemize}}
  \item<5-> Performance \& Scalability
  \visible<6>{\begin{itemize}
    \item Many Gordon Bell Winners
  \end{itemize}}
  \item<7-> Optimality \& Robustness
  \visible<8>{\begin{itemize}
    \item State-of-the-Art Linear and Nonlinear Solvers, Eigensolvers, Optimization Solvers, Timesteppers
  \end{itemize}}
  \item<9-> Flexibility \& Extensibility
  \visible<10>{\begin{itemize}
    \item More than 8000 citations and hundreds of application packages
    \item Aerodynamics, Arterial Flow, Corrosion, Combustion, Data Mining, Earthquake Mechanics, \ldots
  \end{itemize}}
\end{itemize}
\end{frame}
%
\begin{frame}{Alternatives to PETSc}\Large

There are other packages\ldots
\medskip
\begin{itemize}
  \item<2-> DUNE, Hypre
  \medskip
  \item<3-> Firedrake/FEniCS, FreeFEM, Deal.II
  \medskip
  \item<4-> PyLith, Underworld, Salvus, SPECFEM, PFLOTRAN
\end{itemize}
\end{frame}
%
\begin{frame}{PETSc Capabilities}

\setbeamercovered{transparent}
\begin{itemize}
  \item<1-2> Parallel linear algebra (sparse and dense)
  \item Linear and nonlinear solvers
  \item<1> Timestepping
  \item<1> Optimization
  \item<1> Eigensolvers
  \item Meshes
  \begin{itemize}
    \item<1> Cartesian
    \item<1> Structured adaptive
    \item Unstructured adaptive
  \end{itemize}
  \item<1> Discretization support
  \begin{itemize}
    \item<1> Finite Difference
    \item Finite Element
    \item<1> Finite Volume
  \end{itemize}
  \item<1> Portability (GPUs)
  \item Profiling and debugging
\end{itemize}
\end{frame}
%
\begin{frame}{Getting More Help}

\begin{itemize}
  \item \href{https://petsc.org/}{\magenta{https://petsc.org}}
  \smallskip
  \item Hyperlinked documentation
  \begin{itemize}
    \item \href{https://petsc.org/release/docs/manual/}{\magenta{Sphinx}} and \href{https://petsc.org/release/docs/manual/manual.pdf}{\magenta{PDF}} Manual
    \item \href{https://petsc.org/release/docs/manualpages/}{\magenta{Manual pages}} for every method
    \item HTML of example code (linked to manual pages)
  \end{itemize}
  \smallskip
  \item New \href{https://docs.petsc.org/en/latest/}{\magenta{Sphinx}} pages for manuals and tutorials
  \smallskip
  \item \href{https://petsc.org/release/faq/}{\magenta{FAQ}}
  \smallskip
  \item Full support at \href{mailto:petsc-maint@mcs.anl.gov}{petsc-maint@mcs.anl.gov}
\end{itemize}
\end{frame}
%
%
% Time: 45 minutes
\section{Hands-On Example}
%
\begin{frame}{Stokes Problem}
\begin{itemize}
  \item Automatic convergence estimation with PetscConvEst
  \smallskip
  \item Dealing with constant parameters
  \smallskip
  \item Debugging convergence
  \smallskip
  \item Changing solvers
  \smallskip
  \item Assessing convergence
  \smallskip
  \item Profiling code
\end{itemize}
\end{frame}
%
%
\section{Discussion Questions}
%
\begin{frame}{Parallel Computing is Not Magic}\Large

  Performance controlled by bandwidth and latency
  \medskip
  \begin{itemize}
    \item Bandwidth is determined by nodes, not cores
    \smallskip
    \item Usually saturates with less than half the cores
    \smallskip
    \item STREAM benchmark is a good measure
  \end{itemize}
\end{frame}
\begin{frame}{Parallel Computing is Not Magic}\Large

  Most effective for
  \medskip
  \begin{itemize}
    \item increasing problem size (weak scaling),
    \smallskip
    \item not faster solutions (strong scaling).
  \end{itemize}

  \bigskip

  Strong scalability is limited by
  \medskip
  \begin{itemize}
    \item Ahmdahl's Law, but
    \smallskip
    \item bandwidth saturation is often confused with this.
  \end{itemize}
\end{frame}
%
\begin{frame}{GPUs are Not Magic}\Large

  Performance improvement is often quite limited:
  \medskip
  \begin{itemize}
    \item Bandwidth only 2x a top end CPU,
    \smallskip
    \item Very high memory latency,
    \smallskip
    \item Small bandwidth to other parts of the machine
  \end{itemize}

  \bigskip

  Toolchain is rapidly evolving and not robust.
\end{frame}
%
\begin{frame}{Machine Learning is Not Magic}\Large

  The sweet spot for Deep Neural Networks is
  \bigskip
  \begin{itemize}
    \item many parameters (millions), and
    \medskip
    \item no knowledge of what is important.
  \end{itemize}
\end{frame}
%
\begin{frame}{Machine Learning is Not Magic}\Large
\begin{tabular}{c|cc}
                        & Few Parameters & Many Parameters \\
  \hline
  Some idea & Logistic Reg. & \blue{\href{https://www.sciencedirect.com/science/article/pii/S0021999115003046?casa_token=-hcfMO0vXYgAAAAA:AXlHXjiTjOzQFumFNO8ar32GY-s3DBAGxmv062SnWX4Oxwf0Q8k8XkgajztsSz5WPdAN4X9Bgw}{Bayesian Inference}} \\
  No idea   & \blue{\href{https://www.nature.com/articles/s41598-019-45685-z}{Logistic Reg.}} & Neural Networks
\end{tabular}
\end{frame}
%
%\setbeamertemplate{bibliography entry title}{}
%\setbeamertemplate{bibliography entry location}{}
%\setbeamertemplate{bibliography entry note}{}
\begin{frame}[allowframebreaks]{References}
  \printbibliography
\end{frame}
%
\end{document}


Scientific Workshop Number and Title: SCIWS26: Progresses on High-Performance Computing in Geosciences

Date and Time: 12/12/2021; 08:00:00 AM - 12:00:00 PM CST (my part is at 9:00am CST, so 10:00am EST)

Location: New Orleans Ernest N. Morial Convention Center

Room: 203-205
Zoom Access Link: Will be sent to you later in the week

I was going over it in my head. I had started to do mostly slide tutorials, but the feedback had been that hands-on was better, so this time I was going to go back to running PETSc examples in front of people to show what is possible. I was going to try and give a broad overview of capabilities, outline what I will not go over (since it is only an hour), and run some examples that indicate how to do things, and then give them resources to investigate further.
