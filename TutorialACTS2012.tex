\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[PETSc]{The\\{\bf P}ortable {\bf E}xtensible {\bf T}oolkit for {\bf S}cientific {\bf C}omputing}
\author[M.~Knepley]{Matthew~Knepley}
\date[ACTS '12]{PETSc Tutorial\\13th Workshop on the DOE ACTS Collection\\LBNL, Berkeley, CA \qquad August 14--17, 2012}
% - Use the \inst command if there are several affiliations
\institute[UC]{
\begin{tabular}{cc}
  Mathematics and Computer Science Division & Computation Institute\\
  Argonne National Laboratory               & University of Chicago
\end{tabular}
}

\subject{PETSc}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}
\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,SNES,KSP,PC,DM,Mat,Vec,IS,PetscSection,PetscInt,PetscScalar,PetscBool,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\frame{
\titlepage
\begin{center}
\includegraphics[scale=0.3]{figures/logos/doe-logo.jpg}\hspace{1.0in}
\includegraphics[scale=0.3]{figures/logos/anl-logo-black.jpg}\hspace{1.0in}
\includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
\end{center}
}
%
\begin{frame}{Main Point}
\Huge
Never believe \textit{anything},\\
\bigskip
\bigskip
\pause
\qquad unless you can run it.
\end{frame}
%
%
\section{Getting Started with PETSc}
%
  \subsection{What is PETSc?}
\begin{frame}{Unit Objectives}

\Large
\begin{itemize}
  \item Introduce PETSc
  \medskip
  \item Download, Configure, Build, and Run an Example
  \medskip
  \item Empower students to learn more about PETSc
\end{itemize}
\end{frame}

%
  \input{slides/PETSc/Tutorial/WhatINeedFromYou.tex}
  \input{slides/PETSc/Tutorial/AskQuestions.tex}
  \input{slides/PETSc/Tutorial/HowWeCanHelp.tex}
%
  \input{slides/PETSc/OriginalImpetus.tex}
  \input{slides/PETSc/RoleOfPETSc.tex}
  \input{slides/PETSc/WhatIsPETSc.tex}
  \input{slides/PETSc/Timeline.tex}
  \input{slides/PETSc/PETScDevelopers.tex}
%
  \subsection{Who uses PETSc?}
  \input{slides/PETSc/WhoUsesPETSc.tex}
  \input{slides/PETSc/Limits.tex}
  \input{slides/PyLith/Overview.tex}
  \input{slides/PyLith/MultipleMeshTypes.tex}
  \input{slides/Magma/Overview.tex}
  %% TODO: Put links to videos on Fracture page
  \input{slides/Fracture/Overview.tex}
  \input{slides/FMM/VortexMethod.tex}
  \input{slides/FMM/GravityAnomaly.tex}
  \input{slides/GradeTwo/Overview.tex}
  \input{slides/PETSc/Surgery.tex}
%
  \subsection{Stuff for Windows}
  \input{slides/PETSc/MSWindows.tex}
%
  \subsection{How can I get PETSc?}
  \input{slides/PETSc/DownloadingPETSc.tex}
  \input{slides/PETSc/CloningPETSc.tex}
  \input{slides/PETSc/UnpackingPETSc.tex}
  \input{slides/PETSc/Tutorial/Exercise1.tex}
%
  \subsection{How do I Configure PETSc?}
  \input{slides/PETSc/ConfiguringPETSc.tex}
  \input{slides/PETSc/ConfiguringFEM.tex}
  \input{slides/PETSc/AutomaticDownloads.tex}
  \input{slides/PETSc/Tutorial/Exercise2.tex}
%
  \subsection{How do I Build PETSc?}
  \input{slides/PETSc/BuildingPETSc.tex}
  \input{slides/PETSc/Tutorial/Exercise3.tex}
  \input{slides/PETSc/Tutorial/Exercise4.tex}
%
  \subsection{How do I run an example?}
  \input{slides/PETSc/RunningPETSc.tex}
  \input{slides/PETSc/RunningPETScWithPython.tex}
  \input{slides/MPI/UsingMPI.tex}
  \input{slides/MPI/MPIConcepts.tex}
  \input{slides/PETSc/CommonViewingOptions.tex}
  \input{slides/PETSc/CommonMonitoringOptions.tex}
%
  \subsection{How do I get more help?}
  \input{slides/PETSc/GettingMoreHelp.tex}
%
%
\section{SNES ex62}
%
\begin{frame}{SNES ex62}{Formulation}
\Large The isoviscous Stokes problem
\begin{equation*}
\begin{array}{ccccc}
  \Delta \vu     & - & \nabla p &=& \vf \\
  \nabla\cdot\vu &   &          &=& 0
\end{array}
\end{equation*}
on the square domain $\Omega = [0,1]^2$.

\medskip
The sides of the box may have
\begin{itemize}
  \item Dirichlet, or
  \item homogeneous Neumann
\end{itemize}
boundary conditions.
\end{frame}
%
\lstset{language=bash,basicstyle=\scriptsize\ttfamily,emph={dim,order},emphstyle=\bf}
\begin{frame}[fragile]{SNES ex62}{Discretization}

We discretize using finite elements on an unstructured mesh:
\begin{equation*}
\begin{array}{ccccc}
  (\nabla\vv, \nabla\vu) & - & (\nabla\cdot\vv, p) &=& -(\vv, \vf) \\
  (q, \nabla\cdot\vu)    &   &                     &=& 0
\end{array}
\end{equation*}

\smallskip
A finite element basis tabulation header is generated using
\begin{lstlisting}
bin/pythonscripts/PetscGenerateFEMQuadrature.py
  dim order dim 1 laplacian   dim order 1 1 gradient   ex62.h
\end{lstlisting}
\smallskip
\begin{itemize}
  \item[dim] The spatial dimension

  \item[order] The order of the Lagrange element
\end{itemize}
The code should be capable of using any FIAT element,\\but has not yet been tested for this.
\end{frame}
%
%
\section{Solvers}
\subsection{Objects}
\input{slides/PETSc/OptionsDatabase.tex}
%
\subsection{Design}
\input{slides/SNES/AlwaysUseSNES.tex}
%
\begin{frame}{What about TS?}
\begin{center}\Huge
  Didn't Time Integration\\Suck in PETSc?
\end{center}
\pause
\medskip
\begin{center}\Large
  Yes, it did \ldots
\end{center}
\pause
\medskip
\begin{center}\LARGE
  until \magenta{\href{http://59a2.org/research/}{Jed}}, \magenta{\href{http://www.mcs.anl.gov/~emconsta/}{Emil}}, and \magenta{\href{http://www.mcs.anl.gov/~brune/}{Peter}} rewrote it $\Longrightarrow$
\end{center}
\end{frame}
%
\input{slides/TS/ARKIMEX.tex}
\input{slides/TS/Methods.tex}
\input{slides/TS/Usage.tex}
\input{slides/SNES/Methods.tex}
\input{slides/PETSc/Functionality.tex}
%
\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,SNES,KSP,PC,DM,Mat,Vec,IS,PetscSection,PetscInt,PetscScalar,PetscBool,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}
\begin{frame}[fragile]{Solver use in SNES ex62}
% Never hardcode a solver
\begin{overprint}
\onslide<1>
\Large Solver code does not change for different algorithms:
\smallskip
\begin{lstlisting}
  SNES           snes;
  Vec            u, r;
  PetscErrorCode ierr;

  ierr = SNESCreate(PETSC_COMM_WORLD, &snes);CHKERRQ(ierr);
  /* Specify residual computation */
  ierr = SNESSetFromOptions(snes);CHKERRQ(ierr); /* Configure solver */
  ierr = SNESSolve(snes, PETSC_NULL, u);CHKERRQ(ierr);
\end{lstlisting}
\smallskip
\begin{itemize}
  \item \red{Never recompile}! all configuration is dynamic
  \item Factories are hidden from the user
  \item Type of nested solvers can be changed at runtime
\end{itemize}
%% Insert link to article for Hans (put on arXiv)
\onslide<2>
\Large I will omit error checking and declarations:
\smallskip
\begin{lstlisting}
  SNESCreate(PETSC_COMM_WORLD, &snes);
  /* Specify residual computation */
  SNESSetFromOptions(snes); /* Configure solver */
  SNESSolve(snes, PETSC_NULL, u);
\end{lstlisting}
\onslide<3>
\Large The configuration API can also be used:
\smallskip
\begin{lstlisting}
  SNESCreate(PETSC_COMM_WORLD, &snes);
  /* Specify residual computation */
  SNESNGMRESSetRestartType(snes, SNES_NGMRES_RESTART_PERIODIC);
  SNESSetFromOptions(snes);
  SNESSolve(snes, PETSC_NULL, u);
\end{lstlisting}
\smallskip
\begin{itemize}
  \item Ignored when not applicable (no ugly check)
  \item Type safety of arguments is retained
  \item No downcasting
\end{itemize}
\onslide<4>
\Large Adding a prefix namespaces command line options:
\smallskip
\begin{lstlisting}
  SNESCreate(PETSC_COMM_WORLD, &snes);
  /* Specify residual computation */
  SNESSetOptionsPrefix(snes, "stokes_");
  SNESSetFromOptions(snes);
  SNESSolve(snes, PETSC_NULL, u);
\end{lstlisting}
\smallskip
\begin{itemize}
  \item[] \code{\normalsize -stokes\_snes\_type qn} changes the solver type,
  \smallskip
  \item[] whereas \code{\normalsize -snes\_type qn} does not
\end{itemize}
\onslide<5>
\Large User provides a function to compute the residual:
\smallskip
\begin{lstlisting}
  SNESCreate(PETSC_COMM_WORLD, &snes);
  SNESSetFunction(snes, r, FormFunction, &user);
  SNESSetFromOptions(snes);
  SNESSolve(snes, PETSC_NULL, u);
\end{lstlisting}
\smallskip
\begin{equation*}
  r = F(u)
\end{equation*}
\smallskip
\begin{itemize}
  \item User handles parallel communication
  \medskip
  \item User handles domain geometry and discretization
\end{itemize}
\onslide<6>
\Large \class{DM} allows the user to compute only on a local patch:
\smallskip
\begin{lstlisting}
  SNESCreate(PETSC_COMM_WORLD, &snes);
  SNESSetDM(snes, user.dm);
  SNESSetFunction(snes, r, SNESDMComputeFunction, &user);
  SNESSetFromOptions(snes);
  SNESSolve(snes, PETSC_NULL, u);

  DMSetLocalFunction(user.dm, (DMLocalFunction1) FormFunctionLocal);
\end{lstlisting}
\smallskip
\begin{itemize}
  \item Code looks serial to the user
  \item PETSc handles global residual assembly
\end{itemize}
\onslide<7>
\Large Optionally, the user can also provide a Jacobian:
\smallskip
\begin{lstlisting}
  SNESCreate(PETSC_COMM_WORLD, &snes);
  SNESSetDM(snes, user.dm);
  SNESSetFunction(snes, r, SNESDMComputeFunction, &user);
  SNESSetJacobian(snes, A, J, SNESDMComputeJacobian, &user);
  SNESSetFromOptions(snes);
  SNESSolve(snes, PETSC_NULL, u);

  DMSetLocalFunction(user.dm, (DMLocalFunction1) FormFunctionLocal);
  DMSetLocalJacobian(user.dm, (DMLocalJacobian1) FormJacobianLocal);
\end{lstlisting}
\smallskip
SNES ex62 allows both
\begin{itemize}
  \item finite difference (JFNK), and
  \item FEM action
\end{itemize}
versions of the Jacobian.
\onslide<8>
\Large The \class{DM} also handles storage:
\smallskip
\begin{lstlisting}
  CreateMesh(PETSC_COMM_WORLD, &user, &user.dm);
  DMCreateGlobalVector(user.dm, &u);
  VecDuplicate(u, &r);
  DMCreateMatrix(user.dm, MATAIJ, &J);
\end{lstlisting}
\smallskip
\begin{itemize}
  \item DM can create local and global vectors
  \smallskip
  \item Matrices are correctly preallocated
\end{itemize}
\end{overprint}
\end{frame}
%
\input{slides/PETSc/Integration/BasicSolverUsage.tex}
\input{slides/PETSc/ProgrammingWithOptions.tex}
\input{slides/PETSc/Integration/3rdPartySolvers.tex}
%
%
\section{FieldSplit}
\input{slides/Optimal/MonolithicOrSplit.tex} % Jed
\input{slides/MultiField/FieldSplitPC.tex}
\input{slides/MultiField/FieldSplitCustomization.tex}
%
%
\input{slides/MultiField/StokesOptionsTour.tex}
\input{slides/PETSc/Examples/SNESex62FieldSplit.tex}
%
% Null spaces
%% Null (Solve) and Near Null spaces (AMG)
%% For a single matrix
%% Attach to IS for FieldSplit
%% (New) Give constructor to DM for recursive FieldSplit
\section{DM}
\subsection{PetscSection and DMComplex}
\lstset{language=C,basicstyle=\ttfamily\normalsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,SNES,KSP,PC,DM,Mat,Vec,IS,PetscSection,PetscInt,PetscScalar,PetscBool,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}
\begin{frame}{What does a DM do?}
\begin{itemize}
  \item Problem Definition
  \begin{itemize}
    \item Discretization/Dof mapping (\class{PetscSection})
    \item Residual calculation
  \end{itemize}
  \bigskip
  \item Decomposition
  \begin{itemize}
    \item Partitioning, \lstinline!DMCreateSubDM()!
    \item \class{Vec} and \class{Mat} creation
    \item Global $\Longleftrightarrow$ Local mapping
  \end{itemize}
  \bigskip
  \item Hierarchy
  \begin{itemize}
    \item \lstinline!DMCoarsen()! and \lstinline!DMRefine()!
    \item \lstinline!DMInterpolate()! and \lstinline!DMRestrict()!
    \item Hooks for resolution-dependent data
  \end{itemize}
\end{itemize}
\end{frame}
%
\lstset{language=C,basicstyle=\ttfamily\normalsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,SNES,KSP,PC,DM,Mat,Vec,IS,PetscSection,PetscInt,PetscScalar,PetscBool,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}
\input{slides/PetscSection/WhatIsIt.tex}
\input{slides/PetscSection/WhyUseIt.tex}
\input{slides/PetscSection/SimpleConstruction.tex}
%
\begin{frame}[fragile]{PetscSection}{How Do I Build One?}

{\Large Low Level Interface}
\begin{center}
\begin{lstlisting}
PetscSectionCreate(PETSC_COMM_WORLD, &s);
PetscSectionSetNumFields(s, 2);
PetscSectionSetFieldComponents(s, 0, 3);
PetscSectionSetFieldComponents(s, 1, 1);
PetscSectionSetChart(s, cStart, vEnd);
for(PetscInt v = vStart; v < vEnd; ++v) {
  PetscSectionSetDof(s, v, 3);
  PetscSectionSetFieldDof(s, v, 0, 3);
}
for(PetscInt c = cStart; c < cEnd; ++c) {
  PetscSectionSetDof(s, c, 1);
  PetscSectionSetFieldDof(s, c, 1, 1);
}
PetscSectionSetUp(s);
\end{lstlisting}
\end{center}
\end{frame}
%
\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,SNES,KSP,PC,DM,Mat,Vec,IS,PetscSection,PetscInt,PetscScalar,PetscBool,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,ADD_VALUES,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}
\begin{frame}{DMComplex}{What is It?}
\begin{center}\Large
  \class{DMComplex} stands for a DM\\modeling a CW Complex
\end{center}
\begin{itemize}
  \item Handles any kind of mesh
  \begin{itemize}
    \item Simplicial
    \item Hex
    \item Hybrid
    \item Non-manifold
  \end{itemize}
  \medskip
  \item Small interface
  \begin{itemize}
  \item Simple to input a mesh using the API
  \end{itemize}
  \medskip
  \item Accepts mesh generator input
  \begin{itemize}
    \item ExodusII, Triangle, TetGen, LaGriT, Cubit
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{DMComplex}{How Do I Use It?}
The operations used in SNES ex62 get and set values from a \class{Vec},\\organized by the \class{DM} and \class{PetscSection}
\medskip
\begin{lstlisting}
DMComplexVecGetClosure(
  DM dm, PetscSection section, Vec v, PetscInt point,
  PetscInt *csize, const PetscScalar *values[])
\end{lstlisting}
\begin{itemize}
  \item Element vector on cell
  \item Coordinates on cell vertices
\end{itemize}
\pause
Used in \lstinline!FormFunctionLocal()!,
\begin{lstlisting}
for(c = cStart; c < cEnd; ++c) {
  const PetscScalar *x;

  DMComplexVecGetClosure(dm, PETSC_NULL, X, c, PETSC_NULL, &x);
  for(PetscInt i = 0; i < cellDof; ++i) {
    u[c*cellDof+i] = x[i];
   }
  DMComplexVecRestoreClosure(dm, PETSC_NULL, X, c, PETSC_NULL, &x);
}
\end{lstlisting}
\end{frame}
%
\begin{frame}[fragile]{DMComplex}{How Do I Use It?}
The operations used in SNES ex62 get and set values from a \class{Vec},\\organized by the \class{DM} and \class{PetscSection}
\medskip
\begin{lstlisting}
DMComplexVecSetClosure(
  DM dm, PetscSection section, Vec v, PetscInt point,
  const PetscScalar values[], InsertMode mode)
DMComplexMatSetClosure(
  DM dm, PetscSection section, PetscSection globalSection, Mat A, PetscInt point,
  PetscScalar values[], InsertMode mode)
\end{lstlisting}
\begin{itemize}
  \item Element vector and matrix on cell
\end{itemize}
\smallskip
\pause
Used in \lstinline!FormJacobianLocal()!,
\begin{lstlisting}
for(c = cStart; c < cEnd; ++c) {
  DMComplexMatSetClosure(dm, PETSC_NULL, PETSC_NULL, JacP, c,
                         &elemMat[c*cellDof*cellDof], ADD_VALUES);
}
\end{lstlisting}
\end{frame}
%
\begin{frame}[fragile]{DMComplex}{How Do I Use It?}

{\Large The functions above are built upon}
\begin{lstlisting}
DMComplexGetTransitiveClosure(
  DM dm, PetscInt p, PetscBool useCone,
  PetscInt *numPoints, PetscInt *points[])
\end{lstlisting}
\begin{itemize}
  \item Returns points \textit{and} orientations
  \item Iterate over points to stack up the data in the array
\end{itemize}
\end{frame}
%
\input{slides/Plex/StokesDoublet.tex}
%
\begin{frame}{DMComplex}{Basic Operations}
\begin{itemize}
  \item Cone
  \begin{itemize}
    \item edge $\longrightarrow$ endpoints
    \item cell $\longrightarrow$ faces
  \end{itemize}

  \item Support
  \begin{itemize}
    \item vertex $\longrightarrow$ edges
    \item face $\longrightarrow$ cells
  \end{itemize}

  \item Transitive Closure
  \begin{itemize}
    \item cell $\longrightarrow$ faces, edges, vertices
  \end{itemize}

  \item Meet
  \begin{itemize}
    \item cells $\longrightarrow$ shared face
  \end{itemize}

  \item Join
  \begin{itemize}
    \item vertices $\longrightarrow$ shared cell
  \end{itemize}
\end{itemize}
\end{frame}
%   
\subsection{Vec and Mat Particulars}
\input{slides/PETSc/VectorOperations.tex}
\input{slides/PETSc/Integration/LocalVectors.tex}
\input{slides/PETSc/Integration/VecGetArray.tex}
%%   Show options for picture of Jacobian for P1/P1 and P2/P1
%%   Change type to MATAIJCUSP

%ACTSTutorial.tex
%FEniCS08Tutorial.tex
%TACCTutorial2009.tex
%FullDayTutorial.tex
%KAUSTTutorial10.tex
%GUCASTutorial.tex
%
%- Look at Jed's usual configuration options
%- Optimized build slide is good (slide 15)
%- MPI slide is good (slide 20-21)
 %- Bill Gropp quote is good (slide 22)
 %- Make a slide that says waht I said in Austin, namely that hardcoding things is wrong and show a simple solve (This goes with ex62)
 %- Ways to Set Options slide is good (slide 25)
%- Slide on using ex5 is good (slide 26-30)
%- Slide on ``foundation of SNES'' is good (Slide )
%- Good multiphysics (Slide 124, 130)
 %- TS IMEX (Slide 136-138)
%- VI (Slide 139-140)

\section*{Conclusions}
\begin{frame}{Conclusions}

{\Large PETSc can help you:}
\begin{itemize}
  \item easily construct a code to test your ideas
  \begin{itemize}
    \item<2-> Lots of code construction, management, and debugging tools
  \end{itemize}
  \medskip
  \item scale an existing code to large or distributed machines
  \begin{itemize}
    \item<3-> Using \code{FormFunctionLocal()} and scalable linear algebra
  \end{itemize}
  \medskip
  \item incorporate more scalable or higher performance algorithms
  \begin{itemize}
    \item<4-> Such as domain decomposition, fieldsplit, and multigrid
  \end{itemize}
  \medskip
  \item tune your code to new architectures
  \begin{itemize}
    \item<5-> Using profiling tools and specialized implementations
  \end{itemize}
\end{itemize}
\end{frame}

\ifx\@restTalk\@empty

\else
  \relax
\fi
\end{document}
