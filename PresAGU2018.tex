%Highlights for PyLith (2-3 slides)
%- Users
%- Papers
%- Longevity
%- Capabilities
%
%That last item leads to a discussion of enhancing those capabilities.
%The main point is that you want to increase capabilities while limiting
%complexity. We at PyLith and PETSc think this means flexible, generalizable interfaces.
%
%Showcase some things PyLith can do that are hard for other packages:
%- Anisotropic mesh refinement that respects internal boundaries
%  - p4est/Deal.II does isotropic
%- The ability to project functions into our FEM space using auxiliary data
%  - FEniCS/Firedrake cannot use auxiliary data in the this way

\documentclass{beamer}
\input{tex/talkPreambleNoBeamer.tex}

\usefonttheme{professionalfonts}
\usefonttheme{serif}
%\setmainfont{Helvetica Neue}

\mode<presentation>
{
  \usetheme{default}
  \beamertemplatenavigationsymbolsempty
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Abstractions}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\title[PyLith]{Abstraction in PyLith}
\author[Matt]{Matthew~Knepley${}^1$, Brad Aagaard${}^2$,\\Charles Williams${}^3$, Nicolas Barral${}^4$}
\date[AGU18]{AGU Fall Meeting: Advances in Computational Geosciences\\Washington D.C. \qquad December 11, 2018}
% - Use the \inst command if there are several affiliations
\institute[UB]{
  ${}^1$University at Buffalo\\
  ${}^2$United States Geological Survey, Menlo Park\\
  ${}^3$GNS Science, Wellington\\
  ${}^4$Imperial College, London\\
}
\subject{PyLith}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.30]{/Users/knepley/PETSc4/pylith/pylith_posters/logos/CIG_CIT.png}\hspace{0.3in}
  \includegraphics[height=0.55in]{figures/logos/UB_Primary.png}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}\LARGE
  \only<1-4>{\begin{textblock}{0.8}(0.1,0.3)\Huge PyLith\ldots\end{textblock}}
  \only<5-8>{\begin{textblock}{0.8}(0.1,0.3)\Huge PyLith supports\ldots\end{textblock}}

  \only<5-8>{\begin{textblock}{0.78}(0,0)\includegraphics[width=\textwidth]{figures/PyLith/subduction_step03_soln.png}\end{textblock}}
  \only<6-7>{\begin{textblock}{0.5}(0.6,0.6)\includegraphics[width=\textwidth]{figures/PyLith/tpv13_ruptime_comparison.pdf}\end{textblock}}
  \only<7-7>{\begin{textblock}{0.4}(0.1,0.7)\includegraphics[width=\textwidth]{figures/PyLith/error_tet4_1000m.png}\end{textblock}}
  \only<8-8>{\begin{textblock}{0.6}(0.25,0.75)\includegraphics[width=\textwidth]{figures/PyLith/beavan_subevents_slip.png}\end{textblock}}

  \only<2>{\begin{textblock}{0.85}(0.2,0.4)simulates crustal deformation\\focused on earthquake faulting,\\both quasi-static and dynamic.\end{textblock}}
  \only<3>{\begin{textblock}{0.85}(0.2,0.4)is actively developed since 2004,\\with >1000 downloads per release.\end{textblock}}
  \only<4>{\begin{textblock}{0.85}(0.2,0.4)has over 180 citations (Scholar),\\and 60 papers based on it (CIG).\end{textblock}}
  \only<5-8>{\begin{textblock}{1.0}(0.2,0.4)nonlinear fault rheologies,\end{textblock}}
  \only<6-8>{\begin{textblock}{1.0}(0.2,0.49)flexible rupture definitions,\end{textblock}}
  \only<7-8>{\begin{textblock}{1.0}(0.2,0.58)scalable FEM solvers,\end{textblock}}
  \only<8-8>{\begin{textblock}{1.0}(0.2,0.67)and output over subsurfaces.\end{textblock}}
  \transdissolve<2-8>
\end{frame}
%
\begin{frame}\LARGE
  {\Huge PyLith 3 will support\ldots}
  \bigskip
  \begin{itemize}
    \item<2->[] higher order elements,
    \item<3->[] multiphysics problems,
    \item<4->[] geometric MG on the full problem,
    \item<5->[] and anisotropic AMR.
  \end{itemize}
\end{frame}
%
\begin{frame}\LARGE
   We want to enhance \blue{functionality},\\\qquad while limiting \red{complexity}.

   \bigskip
   \bigskip

   \visible<2>{We achieve this goal through flexible,\\\qquad generalizable interfaces.}
\end{frame}
%
%
%% SPEAK: I would like to show some things that are easy in PyLith, but hard for other packages.
\section{Equations}
%
\begin{frame}[fragile]\LARGE
  In Pylith 3, you specify equations pointwise.

  \bigskip

  \begin{overprint}
    \onslide<1>
    \begin{align*}
      F(u) = &\int_\Omega \psi \cdot f_0(u, \nabla u, x)\, + \\
             &\int_\Omega \nabla\psi : \vf_1(u, \nabla u, x)\, + \\
             &\sum_\Gamma \int_\Gamma \psi^{bd} \cdot f^{bd}_0(u, \nabla u, x)\, + \\
             &\sum_\Gamma \int_\Gamma \nabla\psi^{bd} : \vf^{bd}_1(u, \nabla u, x)
    \end{align*}
    \onslide<2>
    Linear Isotropic Elasticity ($u$; $\lambda$, $\mu$)
\begin{cprog}
f1_u(...) {
  for (c = 0; c < Nc; ++c) {
    for (d = 0; d < dim; ++d) {
      f1[c*dim+d] += mu*(u_x[c*dim+d] + u_x[d*dim+c]);
      f1[c*dim+c] += lambda*u_x[d*dim+d];
    }
  }
}
\end{cprog}
    \onslide<3>
    Large Deformation Elasticity ($u$, $p$; $\kappa$, $\mu$)
\begin{cprog}
f1_u(...) {
  Cof3D(cofu_x, u_x); Det3D(&detu_x, u_x);
  p = u[uOff[1]] + kappa * (detu_x - 1.0);
  for (c = 0; c < Nc; ++c) {
    for (d = 0; d < dim; ++d) {
      f1[c*dim+d] = mu * u_x[c*dim+d] + p * cofu_x[c*dim+d];
    }
  }}

f0_p(...) {
  Det3D(&detu_x, u_x);
  f0[0] = detu_x - 1.0;
}

f0_bd_u(..., n) {
  const PetscScalar wall_p = a[aOff[1]];
  Cof3D(cofu_x, u_x);
  for (c = 0; c < Nc; ++c) {
    for (d = 0; d < dim; ++d) f0[c] += cofu_x[c*dim+d] * n[d];
    f0[c] *= wall_p;
  }}
\end{cprog}
  \onslide<4>
  Poroelasticity ($u$, $e$, $p_f$; $\lambda$, $\mu$, $\alpha$, $\kappa$)
\begin{cprog}
f1_u(...) {
  p_f = u[uOff[2]];
  for (c = 0; c < Nc; ++c) {
    for (d = 0; d < dim; ++d) {
      f1[c*dim+d] += mu*(u_x[c*dim+d] + u_x[d*dim+c]);
      f1[c*dim+c] += lambda*u_x[d*dim+d];
    }
    f1[c*dim+c] += alpha*p_f;
  }
}

f0_e(...) {
  for (d = 0; d < dim; ++d) divu += u_x[d*dim+d];
  f0[0] = divu - u[uOff[1]];
}

f1_p(...) {
  for (d = 0; d < dim; ++d) f0[d] -= kappa*u_x[uOff_x[2]+d];
}
\end{cprog}
  \end{overprint}
  % Deal.II needs the user to write the element loop and do quadrature which impedes automatic optimization
\end{frame}
%
%
\section{Functions}
%
\begin{frame}[fragile]\LARGE
  In Pylith 3, finite element solutions and\\\quad functions are interchangable.

  \bigskip

  \Large
  %- The ability to project functions into our FEM space using auxiliary data
  \begin{overprint}
    %Bulk projection with bulk data
    \onslide<1>
    Import data from function $f$ into FEM vector $A$
\begin{cprog}
  DMPlexProject(..., f, ..., A)
\end{cprog}
    This works in any dimension.\\
    \onslide<2>
    Import data from function $f$ into FEM vector $A$

    \bigskip

    PyLith's \texttt{spatialdata} can be used as $f$, and\\evaluated at any $x$, independent of the mesh.
    %Bulk projection with bulk data
    \onslide<3>
    Create a boundary condition $C$ from a function $f$\\
    and constitutive model parameters in vector $A$
\begin{cprog}
  DMPlexProject(..., f, ..., A, C)
\end{cprog}
    \texttt{spatialdata} makes boundary/initial condition\\independent of the mesh.
    %Bulk projection with boundary data
    \onslide<4>
    Create a boundary condition $C$ from a function $f$\\
    and \textit{boundary} model parameters in vector $A$,
\begin{cprog}
  DMPlexProject(..., f, ..., A, C)
\end{cprog}
    such as time-dependent boundary displacements.
    %Bulk projection with surface data
    \onslide<5>
    Create a rupture condition $F$ from a function $f$\\
    and \textit{boundary} model parameters in vector $A$,
\begin{cprog}
  DMPlexProject(..., f, ..., A, F)
\end{cprog}
    such as rupture parameters that sequence events.
    %Boundary projection with bulk data
    \onslide<6>
    Output normal stress on fault $S$ from a function $s$\\
    and \textit{bulk} model solution in vector $A$
\begin{cprog}
  DMPlexProject(..., s, ..., A, S)

  s(...) {
    for (c = 0; c < Nc; ++c) {
      for (d = 0; d < dim; ++d) {
        stress[c*dim+d] += mu*(u_x[c*dim+d] + u_x[d*dim+c]);
        stress[c*dim+c] += lambda*u_x[d*dim+d];
      }
      for (d = 0; d < dim; ++d) f0[c] += stress[c*dim+d]*n[d];
    }
  }
\end{cprog}
    \onslide<7>
    Update PyLith state variables $SV$ with function $sv$,\\
    using the old state variables and old solution as\\
    auxiliary data $A$,
\begin{cprog}
  DMPlexProject(..., sv, ..., A, SV)
\end{cprog}
  \end{overprint}

  % FEniCS/Firedrake cannot use auxiliary data in the this way
\end{frame}
%
%
\section{Adaptation}
%
\begin{frame}\LARGE
  In Pylith 3, you can adapt anisotropically\\\quad around fault surfaces.
  \Large
  \begin{overprint}
    \onslide<2>

    \vskip0.5in

    Since we have abstracted
    \begin{itemize}
      \item[] initial conditions
      \item[] boundary conditions
      \item[] fault conditions
      \item[] discretizations
    \end{itemize}
    away from the mesh, we can easily form\\\quad new problems on adapted meshes.
    \onslide<3>

    \vskip0.5in

    Since we have abstracted
    \begin{itemize}
      \item[] initial conditions
      \item[] boundary conditions
      \item[] fault conditions
      \item[] discretizations
    \end{itemize}
    away from the mesh, we can easily form\\new problems on adapted meshes.
    \begin{center}This also enables MG on the full problem.\end{center}
    %% Make anisotropic picture in 2D (squish toward fault, or other direction), also try in 3D
    \onslide<4>

    \vskip0.5in

    \begin{center}
      \includegraphics[width=\textwidth]{figures/PyLith/subduction_step03_soln.png}\\
      2D subduction zone
    \end{center}
    \onslide<5>

    \vskip0.5in

    % ./ex19 -msh /PETSc3/cig/pylith-git/examples/2d/subduction/mesh_tri3.exo -met 4 -hmax 50000. -hmin 5000. -rgLabel "Cell Sets" -init_dm_view vtk:$PWD/mesh.vtk -adapt_dm_view  vtk:$PWD/adapted.vtk -dist_view vtk:$PWD/mesh.vtk -met_view vtk:$PWD/mesh2.vtk -hscale 0.25,0.25
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction2Ddist.png}\\
      2D subduction zone
    \end{center}
    \onslide<6>

    \vskip0.5in

    % ./ex19 -msh /PETSc3/cig/pylith-git/examples/2d/subduction/mesh_tri3.exo -met 4 -hmax 50000. -hmin 5000. -rgLabel "Cell Sets" -init_dm_view vtk:$PWD/mesh.vtk -adapt_dm_view  vtk:$PWD/adapted.vtk -dist_view vtk:$PWD/mesh.vtk -met_view vtk:$PWD/mesh2.vtk -hscale 0.25,0.25
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction2Duniform.png}\\
      2D subduction zone
    \end{center}
    \onslide<7>

    \vskip0.5in

    % ./ex19 -msh /PETSc3/cig/pylith-git/examples/2d/subduction/mesh_tri3.exo -met 4 -hmax 50000. -hmin 5000. -rgLabel "Cell Sets" -init_dm_view vtk:$PWD/mesh.vtk -adapt_dm_view  vtk:$PWD/adapted.vtk -dist_view vtk:$PWD/mesh.vtk -met_view vtk:$PWD/mesh2.vtk -hscale 0.75,0.0375
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction2Dnormal.png}\\
      2D subduction zone
    \end{center}
    \onslide<8>

    \vskip0.5in

    % ./ex19 -msh /PETSc3/cig/pylith-git/examples/2d/subduction/mesh_tri3.exo -met 4 -hmax 50000. -hmin 5000. -rgLabel "Cell Sets" -init_dm_view vtk:$PWD/mesh.vtk -adapt_dm_view  vtk:$PWD/adapted.vtk -dist_view vtk:$PWD/mesh.vtk -met_view vtk:$PWD/mesh2.vtk -hscale 0.0375,0.75
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction2Dtangent.png}\\
      2D subduction zone
    \end{center}
    \onslide<9>

    \vskip0.5in

    % ./ex19 -msh /PETSc3/cig/pylith-git/examples/2d/subduction/mesh_tri3.exo -met 4 -hmax 50000. -hmin 5000. -rgLabel "Cell Sets" -init_dm_view vtk:$PWD/mesh.vtk -adapt_dm_view  vtk:$PWD/adapted.vtk -dist_view vtk:$PWD/mesh.vtk -met_view vtk:$PWD/mesh2.vtk -hscale 0.0375,50000. -dist_exp 1.2,0.0
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction2Dlayer.png}\\
      2D subduction zone
    \end{center}
    \onslide<10>

    \vskip0.5in

    % ./ex19 -msh /PETSc3/cig/pylith-git/examples/2d/subduction/mesh_tri3.exo -met 4 -hmax 50000. -hmin 5000. -rgLabel "Cell Sets" -init_dm_view vtk:$PWD/mesh.vtk -adapt_dm_view  vtk:$PWD/adapted.vtk -dist_view vtk:$PWD/mesh.vtk -met_view vtk:$PWD/mesh2.vtk -hscale 50000.,0.0375 -dist_exp 0.0,1.2
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction2Dneedle.png}\\
      2D subduction zone
    \end{center}
    \onslide<11>
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction3Dadapt.png}\\
      3D subduction zone
    \end{center}
    \onslide<12>
    \begin{center}
      \includegraphics[width=0.85\textwidth]{figures/AMR/subduction3Dsurface.png}\\
      3D subduction zone
    \end{center}
    \onslide<13>
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction3Dvolume.png}\\
      3D subduction zone
    \end{center}
  \end{overprint}
  % p4est/Deal.II does isotropic
\end{frame}
%
\begin{frame}\LARGE
  \begin{center}
    \Huge\bf PyLith
  \end{center}

  \bigskip

  \begin{center}
  \href{https://github.com/geodynamics/pylith/}{https://github.com/geodynamics/pylith/}

  \bigskip

  \href{https://geodynamics.org/cig/software/pylith/}{https://geodynamics.org/cig/software/pylith/}

  \bigskip

  \href{mailto:cig-short@geodynamics.org }{cig-short@geodynamics.org}
  \end{center}
\end{frame}
%
%
\begin{frame}\LARGE
  In Pylith 3, you can adapt anisotropically\\\quad around fault surfaces.

  \vskip0.5in

  \Large
  \begin{overprint}
    \onslide<1>
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction3Ddist.png}\\
      3D subduction zone
    \end{center}
    \onslide<2>
    \begin{center}
      \includegraphics[width=\textwidth]{figures/AMR/subduction3Dadapt.png}\\
      3D subduction zone
    \end{center}
  \end{overprint}
  % p4est/Deal.II does isotropic
\end{frame}

\end{document}
