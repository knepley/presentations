\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\beamertemplatenavigationsymbolsempty

\title[NPC]{Composing Nonlinear Solvers}
\author[M.~Knepley]{Matthew~Knepley}
\date[ICERM]{Numerical Methods for\\Large-Scale Nonlinear Problems and Their Applications\\ICERM, Providence, RI \qquad September 4, 2015}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{Me}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

%
\begin{frame}<testing>{Abstract}
Title: Composing Nonlinear Solvers

Abstract:
We present, in analogy with the linear preconditioning operation, a framework for the composition of nonlinear solvers,
which we call \textit{nonlinear preconditioning}, in order to accelerate convergence and improve solver performance. A
unified software design is proposed, and illustrated with some examples of complex solver organization.

Bio:
Matthew G. Knepley received his B.S. in Physics from Case Western Reserve University in 1994, an M.S. in Computer
Science from the University of Minnesota in 1996, and a Ph.D. in Computer Science from Purdue University in 2000. He
was a Research Scientist at Akamai Technologies in 2000 and 2001. Afterwards, he joined the Mathematics and Computer
Science department at Argonne National Laboratory (ANL), where he was an Assistant Computational Mathematician, and a
Fellow in the Computation Institute at University of Chicago. In 2009, he joined the Computation Institute as a Senior
Research Associate. His research focuses on scientific computation, including fast methods, parallel computing, software
development, numerical analysis, and multi/manycore architectures. He is an author of the widely used PETSc library for
scientific computing from ANL, and is a principal designer of the PyLith library for the solution of dynamic and
quasi-static tectonic deformation problems. He developed the PETSc scalable unstructured mesh support based upon ideas
from combinatorial topology. He was a J. T. Oden Faculty Research Fellow at the Institute for Computation
Engineering and Sciences, UT Austin, in 2008, and won the R\&D 100 Award in 2009, and the SIAM/ACM Prize in
Computational Science and Engineering in 2015 as part of the PETSc team.

\end{frame}
%
%
\section{Composition Strategies}
\input{slides/SNES/BasicSystem.tex}
\input{slides/SNES/LeftPrec.tex}
\input{slides/SNES/AdditiveNPrec.tex}
\input{slides/SNES/LeftNPrec.tex}
\input{slides/SNES/MultiplicativeNPrec.tex}
\input{slides/SNES/RightNPrec.tex}
\input{slides/SNES/NPrecTable.tex}
%
\begin{frame}{Additive Composition}
We can represent the additive update rule
\begin{align}
  \vx_{i+1} = \vx_i + \alpha (\M(\F,\vx_i,\vb) - \vx_i) + \beta (\N(\F,\vx_i,\vb) - \vx_i)
\end{align}
as
\pause
\begin{align}
  \vx_{i+1} = (\M + \N)(\F,\vx_i,\vb)
\end{align}
\end{frame}
%
\begin{frame}{Additive Composition}
If $\alpha = \beta = 1$, this has an identity operation $0$ (the identity map),
\begin{align}
  \vx_{i+1} &= \vx_i + \alpha (\M(\F,\vx_i,\vb) - \vx_i) + \beta (0(\F,\vx_i,\vb) - \vx_i)\\
           &= \vx_i + (\M(\F,\vx_i,\vb) - \vx_i) + \left( \vx_i - \vx_i \right)\\
           &= \M(\F,\vx_i,\vb)
\end{align}
so that $(\M, +)$ is an abelian group.
\end{frame}
%
\begin{frame}{Multiplicative Composition}
We can represent the multiplicative update rule
\begin{align}
  \vx_{i+1} = \M(\F,\,\N(\F,\vx_i,\vb),\,\vb)
\end{align}
as
\pause
\begin{align}
  \vx_{i+1} = (\M * \N)(\F,\vx_i,\vb)
\end{align}
which is clearly associative.
\end{frame}
%
\begin{frame}{Algebraic Structure}
If we combine it multiplicatively
\begin{align}
  \vx_{i+1} = ((\M + \N) * \Q)(\F,\vx_i,\vb)
\end{align}
\begin{overprint}
\onslide<1>
we get the update rule
\begin{align}
  \vx_{i+1} &= \vx_i + \alpha (\M(\F,\Q(\F,\vx_i,\vb),\vb) - \vx_i)\\
           &\qquad + \beta (\N(\F,\Q(\F,\vx_i,\vb),\vb) - \vx_i)
\end{align}
\onslide<2-3>
which we can write as
\begin{align}
  \vx_{i+1} = (\M * \Q + \N * \Q)(\F,\vx_i,\vb)
\end{align}
\end{overprint}
\visible<3>{
Note however that 
\begin{align}
  \Q * (\M + \N) \ne \Q * \M + \Q * \N
\end{align}
which means $(\M, +, *)$ is a \magenta{\href{https://en.wikipedia.org/wiki/Near-ring}{near ring}}.
}
% Near rings are linked to a fast O(d lg d) polynomial decomposition
\end{frame}
%
\begin{frame}{Algebraic Structure}
If we combine it using our left NPC operation
\begin{align}
  \vx_{i+1} = ((\M + \N) \lp \Q)(\F,\vx_i,\vb)
\end{align}
\begin{overprint}
\onslide<1>
we get the update rule
\begin{align}
  \vx_{i+1} &= \vx_i + \alpha (\M(\vx-\Q(\F,\vx_i,\vb),\vx_i,\vb) - \vx_i)\\
           &\qquad + \beta (\N(\vx-\Q(\F,\vx_i,\vb),\vx_i,\vb) - \vx_i)
\end{align}
\onslide<2->
which we can write as
\begin{align}
  \vx_{i+1} = (\M \lp \Q + \N \lp \Q)(\F,\vx_i,\vb)
\end{align}
we we again have a near ring.
\end{overprint}
\end{frame}
%
\begin{frame}{Algebraic Structure}
In the same way, we can combine it with our right NPC operation
\begin{align}
  \vx_{i+1} = ((\M + \N) \rp \Q)(\F,\vx_i,\vb)
\end{align}
\begin{overprint}
\onslide<1>
and get the update rule
\begin{align}
  \vx_{i+1} &= \vx_i + \alpha (\M(\F(\Q(\F,\vx_i,\vb)),\vx_i,\vb) - \vx_i)\\
           &\qquad + \beta (\N(\F(\Q(\F,\vx_i,\vb)),\vx_i,\vb) - \vx_i)
\end{align}
\onslide<2->
which we can write as
\begin{align}
  \vx_{i+1} = (\M \rp \Q + \N \rp \Q)(\F,\vx_i,\vb)
\end{align}
we we again have a near ring.
\end{overprint}
\end{frame}
%
%
\section{Solvers}
\subsection{Richardson}
\input{slides/SNES/NRICH.tex}
\input{slides/SNES/NRICH-LP.tex}
\subsection{Newton}
\input{slides/SNES/NK.tex}
\input{slides/SNES/LP-NK.tex}
\input{slides/SNES/RP-NK.tex}
\subsection{Generalized Broyden}
\input{slides/SNES/GB.tex}
\input{slides/SNES/LP-GB.tex}
\input{slides/SNES/RP-GB.tex}
%
\begin{frame}{Ideas}
Show
\begin{itemize}
  \item NR -L FAS to get line search
  \item NGMRES -R FAS to get Oosterlee+Washio
  \item Telescope out to NGMRES -R NR -L FAS to add a line search
  \item NR -L Anderson -L FAS to get mixing
\end{itemize}
\end{frame}
%
%
\section{Software Organization}
%
\begin{frame}[fragile]{Generic Solve Routine}
\begin{cprog}
SNESSolve(SNES snes) {
  SNESComputeInitialResidual(snes, X, F, &fnorm); /* Apply left  NPC */
  /* Test for convergence */
  /* Outer Iteration */
  for (i = 0; i < maxits; ++i) {
    SNESApplyNPCRight(snes, X, B, F, &fnorm);     /* Apply right NPC */
    SNESComputeUpdate(snes, X, F, Y);
    /* Check for divergence or iterative failure */
    SNESComputeNextResidual(snes, X, F, &fnorm);  /* Apply left  NPC */
    SNESRestart(snes, &restartCount, X, F);
    /* Test for convergence */
  }
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Richardson Update}
\begin{cprog}
SNESComputeUpdate_NRichardson(SNES snes, Vec X, Vec F, Vec Y)
{
  VecCopy(F, Y);
  VecNorm(F, NORM_2, &fnorm);
  SNESLineSearchApply(snes->linesearch, X, F, &fnorm, F);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Newton Update}
\begin{cprog}
SNESComputeUpdate_Newton(SNES snes, Vec X, Vec F, Vec Y)
{
  /* Solve J Y = F, where J is Jacobian matrix */
  SNESComputeJacobian(snes, X, snes->jacobian, snes->jacobian_pre);
  KSPSetOperators(snes->ksp, snes->jacobian, snes->jacobian_pre);
  KSPSolve(snes->ksp, F, Y);
  /* Compute new solution */
  VecAXPY(X, -1.0, Y);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Generalized Broyden Update}
\begin{cprog}
SNESComputeUpdate_GenBroyden(SNES snes, Vec X, Vec F, Vec Y)
{
  /* Minimize || f_k - F_k gamma_k || to get gamma_k */
  /* Now gamma_k = H^{-1} b where H = F^T_k F_k and b = F^T_k f_k */
  /* Save old solution for restart checks */
  ierr = VecCopy(X, XM);CHKERRQ(ierr);
  ierr = VecCopy(F, FM);CHKERRQ(ierr);
  /* Update solution */
  for (i = 0; i < l; ++i) gammaSum += gamma[i];
  ierr = VecScale(X, 1.0 - gammaSum);CHKERRQ(ierr);
  ierr = VecMAXPY(X, l, gamma, DeltaX);CHKERRQ(ierr);
  if (andersonBeta != 0.0) {
    ierr = VecAXPY(X, (1.0 - gammaSum)*andersonBeta);CHKERRQ(ierr);
    for (i = 0; i < l; ++i) gamma[i] *= andersonBeta;
    ierr = VecMAXPY(X, l, gamma, DeltaF);CHKERRQ(ierr);
  }
  /* Calculate update */
  ierr = VecCopy(X, Y);CHKERRQ(ierr);
  ierr = VecAXPY(Y, -1.0, XM);CHKERRQ(ierr);
}
\end{cprog}
\end{frame}
%
%
\section{Some Results}
%
\begin{frame}
\begin{itemize}
  \item ex19 from prior talk

  \item magma problem
\end{itemize}
\end{frame}
%
%
\section{PETSc}
%
\begin{frame}[fragile]{What is PETSc?}
\begin{overprint}
\onslide<1>
\begin{center}\LARGE PETSc is one of the most popular software libraries in scientific computing.\end{center}
\onslide<2>
Knepley, Karpeev, Sci. Prog., 2009. Brune, Knepley, Scott, SISC, 2013.\\
\vskip-1ex
\includegraphics[width=0.40\textwidth]{figures/Plex/TetrahedronDoubletInterpolated.pdf}
\hfil
\includegraphics[width=0.40\textwidth]{figures/Coarsening/MeshDecimation.png}
\onslide<3>
\begin{center}Brune, Knepley, Smith, and Tu, SIAM Review, 2015.\end{center}
\vskip-1ex
\begin{tabular}{l|c|c|l}
  Type            & Sym      & Statement & Abbreviation \\
  \hline
  Additive        & $+$      & $\vx + \alpha(\M(\F,\vx,\vb)-\vx)$                            & $\M + \N$\\
                  &          & $\phantom{\vx} + \beta(\N(\F,\vx,\vb)-\vx)$                   & \\
  Multiplicative  & $*$      & $\M(\F,\N(\F,\vx,\vb),\vb)$                                   & $\M * \N$\\
  Left Prec.      & $\lp$    & $\M(\vx - \N(\F,\vx,\vb),\vx,\vb)$                            & $\M \lp \N$\\
  Right Prec.     & $\rp$    & $\M(\F(\N(\F,\vx,\vb)),\vx,\vb)$                              & $\M \rp \N$\\
  Inner Lin. Inv. & $\lin$   & $\vy = \vJ(\vx)^{-1}\vr(\vx) = \krylov(\vJ(\vx),\vy_0,\vb)$    & $\NEWT\lin\krylov$\\
\end{tabular}
%\includegraphics[width=0.5\textwidth]{figures/solutions/SNESEx16/elasticitydeformation.png}
\onslide<4>
\begin{center}Aagaard, Knepley, and Williams, J. of Geophysical Research, 2013.\end{center}
\vskip-1ex
\includegraphics[width=0.50\textwidth]{figures/PyLith/beavan_subevents_slip.png}
\includegraphics[width=0.50\textwidth]{/PETSc3/cig/kinrup/figs/savageprescott_soln.png}
\onslide<5>
\begin{center}Knepley and Terrel, Transactions on Mathematical Software, 2012.\end{center}
\vskip-1ex
\includegraphics[width=0.55\textwidth]{figures/FEM/GPU/ConcurrentElements.png}
\hfil
\includegraphics[width=0.35\textwidth]{figures/FEM/GPU/threadTranspose2.png}
\end{overprint}

\Large As a principal architect since 2001, I developed\\
\large
\begin{itemize}
  \item<2-> unstructured meshes (model, algorithms, implementation),
  \medskip
  \item<3-> nonlinear preconditioning (model, algorithms),
  \medskip
  \item<4-> FEM discretizations (data structures, solvers optimization),
  \medskip
  \item<5-> optimizations for multicore and GPU architectures.
\end{itemize}
\end{frame}

\end{document}

- Difference between Anderson and NGMRES for preconditioning
  - Show examples from paper

- Can we prove anything with Homer's stuff

- Magma example with FAS

- PyLith example with FAS

- Manning example with Anderson (Nathan)

- Bio example with Anderson (Jay)

Talk Outline:

Start out with History:

  - What kinds of solvers have been tried
    - One step solvers: Steepest Descent (Richardson), Newton, NCG, Quasi-Newton (like secant)
    - Multistep solvers: Anderson mixing, NGMRES, Multipoint (sadly untried)

  - How have solvers worked together?
    - Warm start: Using one solver and then another
    - Mixing steps: Levenberg-Marquardt (I think)
    - Subsolves: Paper by Homer

  - Can we formalize this?
    - Left and Right nonlinear preconditioning

  - It Worked For Me Once
    - Magma dynamics
    - The important thing is to make it understandable and easy to try out

  - Example: Difference between Anderson Mixing and NGMRES when Composing

Define Anderson Mixing (PETSc-style)

  He compares it to Khabaza, which is just a slow way of doing GMRES

Anderson begins with a nonlinear system
\begin{align}
  F z = 0,
\end{align}
which is transformed to a fixed point problem
\begin{align}
  z = G z,
\end{align}
and finally becomes an iteration
\begin{align}
  z^{l+1} = G z^l.
\end{align}
However the sequence $\{z^l\}$ is slowly convergent, so we want to define a new sequence $\{x^l\}$ with better
properties. The residual $r$ is $F z = G z - z$.

He creates a linear function of $\theta$ interpolating between $r^{l-1}$ and $r^{l}$, and then minimizes the residual in $\theta$.
u^l = x^l + \theta^l (x^{l-1} - x^l) 
v^l = y^l + \theta^l (y^{l-1} - y^l) = G x^l + \theta^l (G x^{l-1} - G x^l)
v^l - u^l = G x^l + \theta^l (G x^{l-1} - G x^l) - x^l - \theta^l (x^{l-1} - x^l)
          = r^l + \theta^l (r^{l-1} - r^l)

Then he updates $x^{l+1}$,
\begin{align}
  x^{l+1} &= u^l + \beta (v^l - u^l) \\
         &= x^l + \theta^l_* (x^{l-1} - x^l)  + \beta (r^l + \theta^l_* (r^{l-1} - r^l)) \\
\end{align}
The $\beta$ term is justified as adding ``new directions'' to the space.

  Look at Peter's SIAMCSE13.pdf

What is NGMRES?

Look at nonlinear-solvers-whitepaper.pdf and search email for from:(Peter Brune) Anderson mixing

From email thread ``Composable solvers; let's do this.'':
> Barry:
>     My conclusion is that M_{left}(x,b) does represent the action of a stationary method (given a current solution, give me a "better" one), but M_{right}(x,b) "only" represents a change of variable.>  Am I right or am I wrong (if I am wrong please explain where the "full" action of the stationary method appears with right preconditioning)?

  Peter:
So; when you enter a given iteration of the right-preconditioned method, you apply x~ = M(x,b) and then continue your solve (the Newton step, etc) based upon x~.  For instance, this is natural for preconditioning NGMRES because you have this "step" x, which is the action of M(x,b) and F(M(x,b)) is needed to construct the minimization problem.  For Newton-like methods, you basically end up doing things that look like alternating between newton and something else.

From email thread ``ok there is a tiny bit of dishonesty'':

Peter:
The unpreconditioned steps are quite different.  For NGMRES you have (X + F(X), F(X + F(X)) as the solution residual pair at each stage (requiring another function evaluation), for ANDERSON one has (X + \beta F(X), F(X)) a the pair.  The "mixing" comes from \beta.  NGMRES is funadmentally right preconditioned; anderson left.


Can we understand how to compose these things by example?
FAS
 - Left:
 - Right:

NCG
 - Left:
 - Right:

NASM
 - Left:
 - Right:

GS
 - Left:
 - Right:

 - Can use Saad's Bratu example

IDEA:

Use Ptak's Newton convergence rate function to show improvement in the short time convergence by composition with a
linearly convergent function. Basically what I want to show is that if I compose the two iterations, I can change the
constant which gives the crossover to quadratic convergence.
