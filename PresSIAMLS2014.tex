\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{mathtools}       % For \mathllap
\usepackage[boxed]{algorithm}
\usepackage{algpseudocode}   % For \Procedure on algorithm

\title[DFT]{A Computational Viewpoint on Classical Density Functional Theory}
\author[M.~Knepley]{Matthew~Knepley and Dirk Gillespie}
\date[LS '14]{Geometric Modeling in Biomolecular Systems\\SIAM Life Sciences 14, Charlotte, NC \qquad August 6, 2014}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{abstract}
Classical Density Functional Theory (CDFT) has become a powerful tool for invesitgating the physics of molecular scale
biophysical systems. It can accurately represent the entropic contribution to the free energy. When augmented with an
electrostatic contribution, it can treat complex systems such as ion channels. We will look at the basic computational
steps involved in the Rosenfeld formulation of CDFT, using the Reference Fluid Density (RFD) method of Gillespie. We
present a scalable and accurate implementation using a hybrid numerical-analytic Fourier method.
\end{frame}
%
%
\section{CDFT Intro}
%
\begin{frame}{What is CDFT?}
\begin{overprint}
\onslide<1>
\begin{quote}\huge
A fast, accurate theoretical tool to understand the fundamental physics of inhomogeneous fluids
\end{quote}
\begin{center}\includegraphics[width=0.5\textwidth]{figures/DFT/Fig1b.jpg}\end{center}
\onslide<2>
\huge For concentration $\rho_i(\vx)$ of species $i$, solve
\medskip
\begin{align*}
  \min_{\rho_i(\vx)} \Omega[\{\rho_i(\vx)\}]
\end{align*}
\medskip
where $\Omega$ is the free energy.
\onslide<3>
\huge For concentration $\rho_i(\vx)$ of species $i$, solve
\medskip
\begin{align*}
  \frac{\delta\Omega}{\delta\rho_i(\vx)} = 0
\end{align*}
\medskip
which are the Euler-Lagrange\\equations.
\onslide<4>

{\Large DFT}
\begin{itemize}
  \item Computes ensemble-averaged quantities directly
  \bigskip
  \item Can have physical resolution in time ($\mu$s) and space (\AA)
  \bigskip
  \item Requires an accurate $\Omega$
  \bigskip
  \item Requires sophisticated solver technology
  \bigskip
  \item \red{Can predict experimental results!}
\end{itemize}
\bigskip
For example,
\begin{center}
  \magenta{\href{http://pubs.acs.org/doi/abs/10.1021/jp052471j}{D. Gillespie, L. Xu, Y. Wang, and G. Meissner,\\J. Phys. Chem. B 109, 15598, 2005}}
\end{center}
\end{overprint}
\end{frame}
%
%
\section{Model}
%
\begin{frame}{Equilibrium}
\begin{align*}
  \rho_i(\vx) = \exp\left(\frac{\mu^{\mathrm{bath}}_i - \mu^{\mathrm{ext}}_i(\vx) - \mu^{\mathrm{ex}}_i(\vx)}{kT}\right)
\end{align*}
where
\begin{align*}
  \mu^{\mathrm{ex}}_i(\vx) &= \mu^{\mathrm{HS}}_i(\vx) + \mu^{\mathrm{ES}}_i(\vx) \\
                         &= \mu^{\mathrm{HS}}_i(\vx) + \mu^{\mathrm{SC}}_i(\vx) + z_i e \phi(\vx) \\
\end{align*}
and
\begin{align*}
  -\epsilon \Delta \phi(\vx) = e \sum_i \rho_i(\vx)
\end{align*}
\end{frame}
%
\begin{frame}{Details}\Large
The theory and implementation are detailed in

\bigskip

Knepley, Karpeev, Davidovits, Eisenberg, Gillespie,\\
\magenta{\href{http://jcp.aip.org/resource/1/jcpsa6/v132/i12/p124101_s1}{An Efficient Algorithm for Classical Density
    Functional Theory in Three Dimensions: Ionic Solutions}},\\
JCP, 2012.
\end{frame}
%
\subsection{Hard Sphere Repulsion}
\begin{frame}{Hard Spheres (Rosenfeld)}
\begin{align*}
  \mu^{\mathrm{HS}}_i(\vx) = kT \sum_\alpha \int \frac{\partial\Phi^{\mathrm{HS}}}{\partial n_\alpha}(n_\alpha(\vx')) \omega^\alpha_i(\vx-\vx')\,d^3x'
\end{align*}
where
\begin{align*}
  \Phi^{\mathrm{HS}}(n_\alpha(\vx')) &= -n_0 \ln (1 - n_3) + \frac{n_1 n_2 - \vn_{V1}\cdot\vn_{V2}}{1 - n_3} \\
                                   &+ \frac{n^3_2}{24\pi (1 - n_3)^2} \left( 1 - \frac{\vn_{V2}\cdot\vn_{V2}}{n^2_2} \right)^3
\end{align*}
\end{frame}
%
\begin{frame}{Hard Sphere Basis}
\begin{align*}
  n_\alpha(\vx) = \sum_i \int \rho_i(\vx') \omega^\alpha_i(\vx-\vx')\,d^3x'
\end{align*}
where
\begin{align*}
  \omega^0_i(\vr) &= \frac{\omega^2_i(\vr)}{4\pi R^{2}_i} &
    \omega^1_i(\vr) &= \frac{\omega^2_i(\vr)}{4\pi R_i} \\
  \omega^2_i(\vr) &= \delta(|\vr| - R_i) &
    \omega^3_i(\vr) &= \theta(|\vr| - R_i) \\
  \vec{\omega}^{V1}_i(\vr) &= \frac{\vec{\omega}^{V2}_i(\vr)}{4\pi R_i} &
    \vec{\omega}^{V2}_i(\vr) &= \frac{\vr}{|\vr|}\delta(|\vr| - R_i)
\end{align*}
\end{frame}
%
\begin{frame}{Hard Sphere Basis}

All $n_\alpha$ integrals may be cast as convolutions:
\begin{align*}
  n_{\alpha}(\vx) &= \sum_i \int \rho_i(\vx') \omega^\alpha_i(\vx'-\vx) d^3x' \\
                 &= \sum_i \mathcal{F}^{-1}\left(\mathcal{F}\left(\rho_i\right) \cdot \mathcal{F}\left(\omega^\alpha_i\right)\right) \\
                 &= \sum_i \mathcal{F}^{-1}\left(\hat{\rho}_i \cdot \hat{\omega^\alpha_i}\right)
\end{align*}
and similarly
\begin{align*}
  \mu^{\mathrm{HS}}_i(\vx) = kT \sum_\alpha \mathcal{F}^{-1}\left(\hat{\frac{\partial\Phi^{\mathrm{HS}}}{\partial n_\alpha}} \cdot \hat{\omega^\alpha_i}\right)
\end{align*}
\end{frame}
%
\begin{frame}{Hard Sphere Basis}{Spectral Quadrature}\large

There is a fly in the ointment:
\begin{itemize}
  \item standard quadrature for $\omega^\alpha$ is very inaccurate ($\mathcal{O}(1)$ errors),
  \medskip
  \item and destroys conservation properties, e.g. total mass
\end{itemize}
\bigskip
\pause
\bigskip
We can use \blue{spectral quadrature} for accurate evaluation,
\begin{itemize}
  \item combining FFT of density, $\hat\rho_i$,
  \medskip
  \item with \blue{analytic FT} of weight functions.
\end{itemize}
\end{frame}
%
\begin{frame}{Hard Sphere Basis}{Spectral Quadrature}

\begin{align*}
  \hat\omega^0_i(\vk) &= \frac{\hat\omega^2_i(\vk)}{4\pi R^{2}_i} &
    \hat\omega^1_i(\vk) &= \frac{\hat\omega^2_i(\vk)}{4\pi R_i} \\
  \hat\omega^2_i(\vk) &= \frac{4\pi R_i \sin(R_i |\vk|)}{|\vk|} &
    \hat\omega^3_i(\vk) &= \frac{4\pi}{|\vk|^3} \left( \sin(R_i |\vk|) - R_i |\vk| \cos(R_i |\vk|) \right) \\
  \hat\omega^{V1}_i(\vk) &= \frac{\hat\omega^{V2}_i(\vk)}{4\pi R_i} &
    \hat\omega^{V2}_i(\vk) &= \frac{-4\pi\imath}{|\vk|^2} \left(\sin(R_i |\vk|)
                          - R_i |\vk|\cos(R_i |\vk|) \right) \hat{k}
\end{align*}
\end{frame}
%
\begin{frame}{Hard Sphere Basis}{Numerical Stability}

Recall that
\begin{align*}
  \Phi^{\mathrm{HS}}(n_\alpha(\vx')) &= \ldots + \frac{n^3_2}{24\pi (1 - n_3)^2} \left( 1 - \frac{\vn_{V2}\cdot\vn_{V2}}{n^2_2} \right)^3
\end{align*}
and note that we have analytically
\begin{align*}
  \frac{\left| n^{V2}(x) \right|^2}{\left| n^2(x) \right|^2} \le 1.
\end{align*}
However, discretization errors in $\rho_i$ near sharp geometric features can produce large values for this term, which
prevent convergence of the nonlinear solver. Thus we \blue{explicitly} enforce this bound.
\end{frame}
%
\subsection{Bulk Fluid Electrostatics}
\begin{frame}{Bulk Fluid (BF) Electrostatics}
\begin{align*}
  \mu^{\mathrm{SC}}_i = \mu^{\mathrm{ES},bath}_i - \sum_j \int_{|\vx-\vx'| \leq R_{ij}}
    \left( c^{(2)}_{ij}(\vx,\vx') + \psi_{ij}(\vx,\vx') \right) \Delta\rho_j(\vx')\,d^3x'
\end{align*}
\begin{overprint}
\onslide<1>
Using $\lambda_k = R_k + \frac{1}{2\Gamma}$, where $\Gamma$ is the MSA screening parameter, we have
\begin{align*}
  c^{(2)}_{ij}\left(\vx,\vx'\right) + \psi_{ij}\left(\vx,\vx'\right) = \frac{z_i z_j e^2}{8\pi\epsilon}
    \Bigg(&\frac{|\vx-\vx'|}{2\lambda_i\lambda_j} - \frac{\lambda_i + \lambda_j}{\lambda_i\lambda_j} +\\
    &\frac{1}{|\vx-\vx'|} \left(\frac{\left(\lambda_i - \lambda_j\right)^2}{2\lambda_i\lambda_j} + 2\right) \Bigg)
\end{align*}
\onslide<2>
\begin{center}\Huge It's a convolution too!\end{center}
\onslide<3>
\begin{align*}
  \mathcal{F}\left(\Delta\rho_j\right) = \mathcal{F}\left(\rho_j - \rho_{\mathrm{bath}}\right) = \mathcal{F}\left(\rho_j\right) - \mathcal{F}\left(\rho_{\mathrm{bath}}\right)
\end{align*}
\begin{itemize}
  \item $\mathcal{F}\left(\rho_j\right)$ was already calculated
  \item $\mathcal{F}\left(\rho_{\mathrm{bath}}\right)$ is constant
  \item $\mathcal{F}\left(c^{(2)}_{ij}\left(\vx,\vx'\right) + \psi_{ij}\left(\vx,\vx'\right)\right)$ is constant
\end{itemize}
so we only calculate the inverse transform on each iteration.
\onslide<4>
\begin{center}\Huge FFT is also inaccurate!\end{center}
\onslide<5>
\begin{align*}
  \hat c^{(2)}_{ij} + \hat\psi_{ij} = \frac{z_i z_j e^2}{\epsilon|\vk|} \left(\frac{1}{2\lambda_i\lambda_j} I_1 -
  \frac{\lambda_i + \lambda_j}{\lambda_i\lambda_j} I_0 + \left(\frac{\left(\lambda_i - \lambda_j\right)^2}{2\lambda_i\lambda_j} + 2\right) I_{-1} \right)
\end{align*}
where
\begin{align*}
  I_{-1} &= \frac{1}{|\vk|} \left(1 - \cos(|\vk| R)\right) \\
  I_0   &= -\frac{R}{|\vk|} \cos(|\vk| R) + \frac{1}{|\vk|^2} \sin(|\vk| R) \\
  I_1   &= -\frac{R^2}{|\vk|} \cos(|\vk| R) + 2 \frac{R}{|\vk|^2} \sin(|\vk| R) - \frac{2}{|\vk|^3} \left(1 - \cos(|\vk| R)\right)
\end{align*}
\end{overprint}
\end{frame}
%
\subsection{Reference Fluid Density Electrostatics}
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}

Expand around $\rho_i^{\mathrm{ref}}\left(\vx\right)$, an inhomogeneous reference density profile:
\begin{align*}
  \mu^{SC}_i&\left[\left\{\rho_k\left(\vy\right)\right\}\right] \approx
    \mu^{SC}_i\left[\left\{\rho_k^{\mathrm{ref}}\left(\vy\right)\right\}\right] \\
  &- kT\sum_i \int c_i^{\left(1\right)}\left[\left\{\rho_k^{\mathrm{ref}}\left(\vy\right)\right\};\vx\right]
      \Delta\rho_i\left(\vx\right) d^3x \\
  &- \frac{kT}{2} \sum_{i,j} \iint
    c_{ij}^{\left(2\right)}\left[\left\{\rho_k^{\mathrm{ref}}\left(\vy\right)\right\};\vx,\vx'\right]
      \Delta\rho_i\left(\vx\right) \Delta\rho_j\left(\vx'\right) d^3x\,d^3x'
\end{align*}
with
\begin{align*}
   \Delta\rho_i\left(\vx\right) = \rho_i\left(\vx\right) - \rho_i^{\mathrm{ref}}\left(\vx\right)
\end{align*}
\end{frame}
%
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}
\begin{align*}
  \rho^{\mathrm{ref}}_i\left[\left\{\rho_k\left(\vx'\right)\right\};\vx\right] = \frac{3}{4\pi R_{SC}^{3}\left(\vx\right)}
    \int_{\left\vert\vx' - \vx\right\vert \leq R_{SC}\left(\vx\right)} \alpha_i\left(\vx'\right) \rho_i\left(\vx'\right) d^3x'
\end{align*}
\pause
\Large
Choose $\alpha_i$ so that the reference density is
\begin{itemize}
  \item charge neutral, and

  \item has the same ionic strength as $\rho_i$
\end{itemize}
\pause
This can model gradient flow
\end{frame}
%
\begin{frame}{Reference Fluid Density (RFD) Electrostatics}
We can rewrite this expression as an averaging operation:
\begin{align*}
  \rho^{ref}(\vx) = \int \rho(\vx') \frac{\theta\left(|\vx' - \vx| - R_{SC}(\vx)\right)}{\frac{4\pi}{3} R^3_{SC}(\vx)} dx'
\end{align*}
where
\begin{align*}
  R_{SC}(\vx) = \frac{\sum_i \tilde\rho_i(\vx) R_i}{\sum_i \tilde\rho_i(\vx)} + \frac{1}{2\Gamma(\vx)}
\end{align*}
\pause
We close the system using
\begin{align*}
  \Gamma_{\mathrm{SC}}\left[\rho\right](\vx) = \Gamma_{\mathrm{MSA}}\left[\rho^{\mathrm{ref}}(\rho)\right](\vx).
\end{align*}
\end{frame}
%
%
\section{Verification}
%
\begin{frame}{Consistency checks}
\begin{itemize}\Large
  \item Check $n_\alpha$ of constant density against analytics
  \bigskip
  \item Check that $n_3$ is the combined volume fraction
  \bigskip
  \item Check that wall solution has only 1D variation
\end{itemize}
\end{frame}
%
\begin{frame}{Sum Rule Verification}{Hard Spheres}
\begin{align*}
  \beta P^{HS}_{\mathrm{bath}} = \sum_i \rho_i(R_i)
\end{align*}
where
\begin{align*}
  P^{HS}_{\mathrm{bath}} = \frac{6kT}{\pi} \left( \frac{\xi_{0}}{\Delta} + \frac{3\xi_{1}\xi_{2}}{\Delta^{2}}
    + \frac{3\xi_{2}^{3}}{\Delta^{3}} \right)
\end{align*}
using auxiliary variables
\begin{align*}
  \xi_{n} &= \frac{\pi}{6} \sum_{j} \rho^{bath}_j \sigma_{j}^{n} \qquad n \in \{0,\ldots,3\} \\
  \Delta  &= 1-\xi_{3}
\end{align*}
\end{frame}
%
\begin{frame}{Sum Rule Verification}{Hard Spheres}
\begin{overprint}
\onslide<1>
Relative accuracy and Simulation time for $R = 0.1\mathrm{nm}$
\onslide<2>
Volume fraction ranges from $10^{-5}$ to $0.4$ (very difficult for MC/MD)
\end{overprint}
\begin{center}
\includegraphics[width=\textwidth]{figures/DFT/sumRule.png}
\end{center}
\pause
% domain is divided into cubes which are 0.05nm x 0.05nm x 0.00625nm.
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}
\begin{center}
\begin{tabular}{ll}
$R_{\mathrm{cation}}$ & $0.1\mathrm{nm}$ \\
$R_{\mathrm{anion}}$  & $0.2125\mathrm{nm}$ \\
Concentration       & $1\mathrm{M}$ \\
Domain              & $2\times2\times6\,\mathrm{nm}^3$ and periodic \\
Uncharged hard wall & $z=0$ \\
Grid                & \alt<2>{$21\times21\times{}$\red{161}}{$21\times21\times161$}
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}

\begin{center}
Cation Concentrations for 1M concentration\\
\includegraphics[width=\textwidth]{figures/DFT/table1_2_cation.png}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}

\begin{center}
Anion Concentrations for 1M concentration\\
\includegraphics[width=\textwidth]{figures/DFT/table1_2_anion.png}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}

\begin{center}
Mean Electrostatic Potential for 1M concentration\\
\includegraphics[width=\textwidth]{figures/DFT/table1_2_phi.png}
\end{center}
\end{frame}
%
\begin{frame}{Ionic Fluid Verification}{Charged Hard Spheres}\Large

These results were first reported in 1D in\\
{\magenta{\href{http://iopscience.iop.org/0953-8984/17/42/002/}{Density functional theory of the electrical double layer: the RFD functional}},\\
J. Phys.: Condens. Matter 17, 6609, 2005}.
\end{frame}
%
\begin{frame}{Main Points}
Real Space vs. Fourier Space
\begin{itemize}
  \item $\mathcal{O}(N^2)$ vs. $\mathcal{O}(N \lg N)$

  \item Accurate quadrature only available in Fourier space
\end{itemize}
\pause
Electrostatics
\begin{itemize}
  \item Bulk Fluid (BF) model can be qualitatively wrong

  \item Reference Fluid Density (RFD) model demands complex algorithm
\end{itemize}
\pause
Solver convergence
\begin{itemize}
  \item Picard was more robust

  \item Newton rarely entered the quadratic regime

  \item Still no multilevel alternative (interpolation?)
\end{itemize}
\end{frame}
%
\begin{frame}{Conclusion}
The theory and implementation are detailed in\\
{\magenta{\href{http://jcp.aip.org/resource/1/jcpsa6/v132/i12/p124101_s1}{An Efficient Algorithm for Classical Density Functional Theory in Three Dimensions: Ionic Solutions}}, JCP, 2012}.
\begin{overprint}
\onslide<1>
\begin{center}\includegraphics[width=\textwidth]{figures/DFT/rhoK__40R__150K__1e-4Ca.png}\end{center}
\onslide<2>
\begin{center}\includegraphics[width=\textwidth]{figures/DFT/Delta_muHSK__40R__150K__1e-4Ca.png}\end{center}
\end{overprint}
\note{Problem: 1D DFT is great, predicts I-V curves for 100+ solutions including multivlaent species, but cannot see details inside the channel.}
\end{frame}

\end{document}

- Mean Field Electrostatics
  - White Bear paper
    - Show complexity for computation
  - Paper on GPU implementation
  - Paper on Spectral Quadrature
- RFD
  - Citation of Dirk/Deszo paper
  - Dirk paper with all the fits to experiment

\only<1>{\begin{textblock}{0.5}[0.5,0.5](0.8,0.5)\includegraphics[width=\textwidth]{figures/DFT/ryanodineReceptor.jpg}\end{textblock}}
