% 
%                        UC Letter Head Style	
%
%   \documentclass[12pt]{mcsLetter}
%   \thephoneextension{3118}
%   \theemailextension{bsmith}
%   \begin{document}
%      \theaddress{Name of Addressee \\
%                  MCS Division, Building 221 \\
%                  9700 South Cass Ave \\
%                  Argonne, IL 60439}
%      \theopening{Dear Addressee,}
%        Here is the text body.
%      \theclosing{Barry Smith}
%      \thecc{this cc is optional}
%   \end{document}
%
%
\LoadClass{letter}
\def \extension{9174}
\def \ename{postmaster}
%\input psfig
\oddsidemargin 0.25in
\evensidemargin 0.25in
\textwidth=6in
\textheight 8.8in
\topmargin 0in
\headheight 0in
\headsep 0in
\pagestyle{empty}
%
\newcommand{\thephoneextension}[1]{
   \def \extension{#1}
}  
\newcommand{\theemailextension}[1]{
   \def \ename{#1}
}
%
%	macro for address
%
\newcommand{\theaddress}[1]{
  \parskip=4pt
  \parindent=0pt
  \begin{center}
      {\Large \bf UNIVERSITY OF CHICAGO}
  \end{center}
  \footnotetext{
    \centerline{\includegraphics[height=1in]{figures/logos/uc-logo-official.jpg}}
     \vspace{-.8in}
%     \begin{center}
%        {\large \sf The University of Chicago} 
%     \end{center}
  }
  {\obeylines 
    Computation Institute        \hfill Telephone: (773) 834-\extension \\
    5640 S. Ellis Avenue, RI 405 \hfill Faxphone:  (773) 834-6818 \\
    Chicago, IL 60637            \hfill Email: \ename @ci.uchicago.edu \\
    \vskip -.30in
    \hbox{ } \hfill \today 
  }

  \medskip
  \parindent=.0in
  \parskip=8pt
  {\obeylines 
      #1
      \vskip.05in
  }
  \parindent=.4in
}
%
%	macro for opening
%
\newcommand{\theopening}[1]{
  {\obeylines \parindent=0in \parskip=0pt
     #1
  } 
  \medskip
}
%
%	macro for closing
%
\newcommand{\theclosing}[1]{
   \vskip.2in
   {\parindent=2.5in
      \parskip=0pt
      Sincerely,
      \vskip 0.37in
      \parbox{3in}{#1} 
   } 
   \vspace{.3in}
}
%
\newcommand{\thecc}[1]{
   \vskip .1in
   \parindent=.0in
   CC: #1
}
%
%	suppress footnotes
%
\renewcommand{\thefootnote}{}
\renewcommand{\footnoterule}{ }
