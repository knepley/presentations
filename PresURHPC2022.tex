\documentclass[dvipsnames,20pt]{beamer}

\input{talkPreambleNoBeamer.tex}
\mode<presentation>
{
  \usetheme{Malmoe}
  \usefonttheme{serif}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamerfont{title}{size=\normalsize}
\setbeamerfont{author}{size=\footnotesize,series=\bfseries}
\setbeamerfont{title page}{size=\scriptsize}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{/PETSc3/petsc/petsc-dev/doc/petsc.bib}
\addbibresource{presentations.bib}
\renewcommand*{\bibfont}{\tiny}

% Figure this out
\newcounter{ccount}
\newcounter{num2}
\newcounter{num_prev}

\title[TAS]{How to Choose an Algorithm}
\author[M.~Knepley]{Matthew~Knepley}
\date[FLHPC]{Finger Lakes HPC Meeting\\University of Rochester\\Rochester, NY \quad October 14, 2022}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{TAS}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}
\begin{frame}
%
\begin{center}
  RELACS People
\end{center}

\bigskip

\begin{center}
\includegraphics[height=1.5in]{figures/PETSc/MatthewKnepley321x480.jpg}\hspace{1.0in}
\includegraphics[height=1.5in]{figures/PETSc/JustinChang.png}
\end{center}

\end{frame}
%
\begin{frame}<abstract>

  Performance modeling is a venerable activity, and many useful models exist by which to judge algorithmic performance. Here we present a novel analysis which takes into account accuracy as well as traditional metrics. This allows us to compare algorithms which have markedly different convergence behavior. We will illustrate this concept with some examples, and suggest lines of future inquiry.

\end{frame}
%
\begin{frame}
\transdissolve[duration=0.2]

  How do we choose an algorithm?

\vspace*{1in}
\pause

\qquad \textit{We choose the fastest one\ldots}

\end{frame}
%
\begin{frame}
\transdissolve[duration=0.2]

  Timing is tricky. It's sensitive to

  \vspace*{0.333in}
  \pause

\qquad \textit{machine characteristics}

  \vspace*{0.333in}
  \pause

\qquad \textit{problem details}

\end{frame}
%
\begin{frame}

  Proxy measures can simplify design:

%\vspace*{0.333in}
\pause

\begin{center}
\def\arraystretch{1.5}
\begin{tabular}{ll}
\textit{Computation} & (HPL)      \pause\\ % Not a good predictor for most algorithms on modern machines
\textit{Bandwidth}   & (Roofline) \pause\\ % Can compare similar algorithms, where flops or bytes have equal contribution to accuracy
\textit{Latency}     & (LogP)     \pause\\
\textit{Concurrency} &
\end{tabular}
\end{center}

\end{frame}
%
\begin{frame}

  These models can answer\ldots

\vspace*{0.333in}

\begin{overprint}
\onslide<2-3>
\begin{quote}
  Does this implementation scale weakly? \only<3>{strongly?}
\end{quote}
\onslide<4>
\begin{quote}
  Is one implementation more efficient than another on this machine?
\end{quote}
\end{overprint}
\end{frame}
%
\begin{frame}

  What about questions like...

\vspace*{0.333in}

\begin{overprint}
\onslide<2>
\begin{quote}
  Should I discretize this\\problem with CG or DG?
\end{quote}
\onslide<3>
\begin{quote}
  Should I solve using the\\Picard or Newton Method?
\end{quote}
\end{overprint}
\end{frame}
%
\begin{frame}

  The key notion we are missing is

\bigskip
\pause

  \begin{center}\LARGE\textit{accuracy}\end{center}

\bigskip
\pause

    It distinguishes algorithms with different convergence behavior\\
    \parencite{ChangFabienKnepleyMills2018}
\end{frame}
%
\begin{frame}

\medskip
\begin{center}
\includegraphics[height=0.9\textheight]{figures/Performance/TAS/TAS_model_concept.pdf}
\end{center}

\only<2->{\begin{textblock}{0.3}(0.865,0.85)Problem\end{textblock}}
\only<3->{\begin{textblock}{0.3}(0.465,0.0)Machine\end{textblock}}
\only<4->{\begin{textblock}{0.3}(0.05,0.85)Algorithm\end{textblock}}

\end{frame}
%
\begin{frame}

% Person vs Problem, leaves out Machine which is why it is the oldest analysis
\begin{center}
Mesh Convergence Diagram\\
\includegraphics[height=0.6\textheight]{figures/Performance/TAS/3Dcgvsdg_doados.pdf}
\end{center}

\begin{overprint}
\onslide<2>
  \begin{center}1/error vs. size\end{center}
\onslide<3>
  \begin{center}\textit{Does my Algorithm solve\\this Problem?}\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}

% Strong and weak scaling both look at computation rate (dof/s) as the concurrency is varied
% We take this idea and instead plot computation rate against time at constant concurrency
%    Shows weak scaling at the far right
%    Shows strong scaling at the far left, and also minimum runtime
%    Tell atmospheric simulation story (column solves are Amdahl bottleneck), IPCC 5 simulated years/day will not be met with a bigger machine
\begin{center}
Static Scaling Diagram\\
\only<1-2,4->{\includegraphics[height=0.6\textheight]{figures/Performance/TAS/3Dcgvsdg_dofsec.pdf}}
\only<3>{\includegraphics[height=0.6\textheight]{figures/Performance/SummitReport1/jed_VecAXPY_CPU_vs_GPU.png}}
\end{center}

\begin{overprint}
\onslide<2-4>
  \begin{center}size/time vs. time\end{center}
\onslide<5>
  \begin{center}\textit{Is my Algorithm efficient on\\this Machine?}\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}

  How should we measure accuracy?

\vspace*{0.333in}
\pause

\qquad accuracy rate $\frac{e}{T}$

\vspace*{0.333in}
\pause

\qquad Marginal accuracy rate falls off\\
\qquad steeply with problem size

\end{frame}
%
\begin{frame}

  Consider an optimal PDE solver:

\vspace*{0.333in}
\pause

\begin{center}
    $T = W h^{-d}$ and $e = C h^\alpha$
\end{center}

\pause

The error-time has a simple form
\begin{align*}
       &-\log(e \cdot T) \\
  {} = &-\log\left(C h^\alpha W h^{-d} \right) \\
  {} = &(d - \alpha) \log(h) - \log(C W)
\end{align*}
\end{frame}
%
\begin{frame}

\begin{center}
Efficacy Diagram\\
\includegraphics[height=0.6\textheight]{figures/Performance/TAS/3Dcgvsdg_errtime.pdf}
\end{center}

\begin{overprint}
\onslide<2>
  \begin{center}1/error-time vs. time\end{center}
\onslide<3>
  \begin{center}$\frac{1/\mathrm{error}}{\mathrm{size}} \times \frac{\mathrm{size}/\mathrm{time}}{\mathrm{time}} = \frac{1/(\mathrm{error}\cdot\mathrm{time})}{\mathrm{time}}$\end{center}
\onslide<4>
  \begin{center}\textit{Does my Algorithm solve this Problem efficiently on this Machine?}\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}

\begin{center}
  Efficacy vs. Static Scaling\\
  \includegraphics[width=\textwidth]{figures/Performance/TAS/HDG_efficacy_static.pdf}
  \parencite{Fabien2019}
\end{center}
\end{frame}
%
\begin{frame}

  What else could we analyze?

\vspace*{0.333in}

\begin{overprint}
\onslide<2>
  Communication-Avoiding (CA)\\
  algorithms have exciting\\
  lower bounds

  \bigskip

  \parencite{BallardDemmelHoltzSchwartz2011}
\onslide<3>
  CA TSQR is a great success

  \bigskip

  \parencite{DemmelGrigoriHoemmenLangou2012}
\onslide<4>
  CA Krylov not a success
\onslide<5>
  CA Krylov not a success

  \bigskip

  {\small Accuracy depends on coarse grid\\communication in preconditioner}
\end{overprint}
\end{frame}
%
\begin{frame}

Future Questions:

\vspace*{0.333in}

\begin{overprint}
\onslide<2>
\begin{quote}
  Is there a variational\\characterization of\\optimal algorithms?
\end{quote}
\onslide<3>
\begin{quote}
  Can we think of error-time as an Algorithmic Action?
\end{quote}
\end{overprint}
\end{frame}
%
%\setbeamertemplate{bibliography entry title}{}
%\setbeamertemplate{bibliography entry location}{}
%\setbeamertemplate{bibliography entry note}{}
\begin{frame}[allowframebreaks]{References}
  \printbibliography
\end{frame}
%
\begin{frame}<outline>

How can we understand algorithmic performance?
  - Time, but it is complex since it is sensitive to
    - machine architecture
    - input characteristics
  - Computation (flops)
    - Linpack Benchmark (Top500)
    - Not a good predictor for most algorithms on modern machines
  - Bandwidth
    - Explained by Roofline Model
    - Can compare similar algorithms, where flops or bytes have equal contribution to accuracy
  - Can add other resources (latency in LogP, concurrency, \ldots)
  - What questions can these models answer?
    - Does my implementation scale weakly? strongly?
    - Is one implementation more efficient than another?
  - What about...
    - Is CG or DG the most efficient discretization for this problem?
    - Is Picard or Newton's Method solving more efficiently?
  - The key notion were are missing is Accuracy
    - Needed to distinguish algorithms with different convergence behavior
  - TAS (show picture)
    - Mesh convergence
    - Static scaling (gives weak and strong)
    - Efficacy
  - Model for optimal PDE solver
    - Show that this give linear efficacy
  - Does this help us understand anything?
    - Comparison of CG and DG for smooth problem
    - Maurice's elasticity paper
    - Communication Avoiding (CA) algorithms
      - Exciting lower bounds (cite Ballard and Demmel)
      - CA TSQR great success (cite Laura)
      - CA Krylov not a success, since accuracy depends on coarse grid communication
  - Is there a variational characterization of optimal algorithms?
    - Can we think of (error time) as an Algorithmic Action?
\end{frame}
\end{document}
