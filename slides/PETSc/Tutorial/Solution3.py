#!/usr/bin/env python
totalLinearIterates = {}
totalLinearIterates['gmres-ilu']    = [(16, 8),  (64, 21), (256, 35), (1024, 64),  (4096, 124),  (16384, 339)]
totalLinearIterates['gmres-jacobi'] = [(16, 4),  (64, 18), (256, 61), (1024, 212), (4096, 1420), (16384, 6833)]
totalLinearIterates['gmres-ml']     = [(16, 12), (64, 12), (256, 13), (1024, 18),  (4096, 15),   (16384, 15)]

from matplotlib import pylab
d = []
for key,style in zip(totalLinearIterates.keys(), ['ro-', 'go-', 'bo-']):
  d.append([a[0] for a in totalLinearIterates[key]])
  d.append([a[1] for a in totalLinearIterates[key]])
  d.append(style)
pylab.loglog(*d)
pylab.title('Preconditioner Strength as a Function of Problem Size for SNES ex5')
pylab.xlabel('Problem Size')
pylab.ylabel('Total Linear Iterations')
pylab.legend(totalLinearIterates.keys(), 'upper left', shadow = True)
pylab.savefig('Solution3.png')
pylab.show()
