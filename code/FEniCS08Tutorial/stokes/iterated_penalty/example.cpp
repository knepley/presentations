#include <dolfin.h>

using namespace dolfin;

int main()
{
  Vector one(1.0);
  Vector iter(0.0);

  iter.vec() += 2.0 * one.vec();

  cout << iter.one() << endl;
}
