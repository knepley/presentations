\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}

\title[Configurability]{Runtime Configurability in PETSc}
\author[M.~Knepley]{Matthew~Knepley {\bf$\in$} PETSc Team}
\date[CS\&E]{SIAM Conference on Parallel Processing and Scientific Computing\\Portland, OR \quad February 20, 2014}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\lstset{language=C,basicstyle=\ttfamily\scriptsize,stringstyle=\ttfamily\color{green},commentstyle=\color{brown},morekeywords={MPI_Comm,TS,SNES,KSP,PC,DM,Mat,Vec,VecScatter,IS,PetscSF,PetscSection,PetscObject,PetscInt,PetscScalar,PetscReal,PetscBool,InsertMode,PetscErrorCode},emph={PETSC_COMM_WORLD,PETSC_NULL,SNES_NGMRES_RESTART_PERIODIC},emphstyle=\bf}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>{abstract}

Title: Runtime Configurability in PETSc

Abstract:
The ability to dynamically configure a deeply nested hierarchy of objects is a key feature in the design of PETSc. This
allows the user to assemble an optimal solver tailored to problem characteristics without changing the application code,
and also allows easy comparison among competing methods. We will demonstrate the efficacy of this approach for both
linear and nonlinear systems. Moreover, we extend this paradigm to encompass residual evaluation and other key
simulation operations.
\end{frame}
%
\input{slides/PETSc/PETScDevelopers.tex}

\begin{frame}{What can be Configured in PETSc?}\LARGE
\begin{itemize}
  \item<2-> Object behavior
  \begin{itemize}\Large
    \item<3-4> Tolerances, subspace sizes, preallocation, \ldots
    \item<4> Eisenstat-Walker tolerances for \class{SNES}
  \end{itemize}
  \bigskip
  \item<5-> Concrete object types
  \begin{itemize}\Large
    \item<6-7> \class{MATAIJ}, \class{KSPGMRES}, \class{SNESFAS}, \class{DMPLEX}
    \item<7>   User-defined types
  \end{itemize}
  \bigskip
  \item<8-> Object organization
  \begin{itemize}\Large
    \item<9-11>  Linear \& Nonlinear preconditioners
    \item<10-11> Number of splits in block methods
    \item<11>    Explicit/Semi-implicit/Implicit divison for \class{TS}
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Why Configure at Runtime?}\LARGE
\begin{overprint}
  \onslide<1>
  Vectors and Matrices
  \smallskip
  \begin{itemize}
    \item architecture % jagged diagonal for vectorization on Earth simulator
    \medskip
    \item problem/discretization % block matrices
  \end{itemize}
  \onslide<2-3>
  Solvers
  \smallskip
  \begin{itemize}
    \item the equation and boundary conditions % Monotonicity
    \medskip
    \item domain % Re-entrant corner
    \medskip
    \item discretization % Smoothers differ for FEM and FDM
    \bigskip
    \item solution evolution
    \begin{itemize}\Large
      \item<3> nonlinear feedback
      \smallskip
      \item<3> dynamic instabilities
      \smallskip
      \item<3> strong or emergent anisotropy
      \smallskip
      \item<3> resonance
    \end{itemize}
  \end{itemize}
  \onslide<4>\Large
  Arguments against concrete types in applications:
  \bigskip
  \bigskip
  \begin{center}
    \hbox{\bf Programming Languages for Scientific Computing}
    \smallskip
    Encyclopedia of Applied and Computational Mathematics, Springer, 2012.\\
    \smallskip
    \magenta{\href{http://arxiv.org/abs/1209.1711}{http://arxiv.org/abs/1209.1711}}
  \end{center}
\end{overprint}
\end{frame}
%
%
\section{Configuring PETSc}
\input{slides/PETSc/UserSolve.tex}
%% CUT: Put in Fieldsplit examples for a longer talk
\input{slides/SNES/SNESEx19.tex}
\input{slides/SNES/NPCEx19.tex}
%
%
\section{Extending PETSc}
\subsection{Creating a new Class Implementation}
\begin{frame}[fragile]{Creating a Preconditioner (\class{PC})}\LARGE
\begin{overprint}
  \onslide<1>
  Include the private header for access to the \class{PC} struct:
  \bigskip
\begin{lstlisting}[basicstyle=\ttfamily\large]
  #include<petsc-private/pcimpl.h>
\end{lstlisting}
  \onslide<2>
  Define a struct for the concrete type:
  \bigskip
\begin{lstlisting}[basicstyle=\ttfamily\large]
  typedef struct {
    /* The maximum and actual half-bandwidth */
    PetscInt  kmax, k;
    /* The limit and actual norm fraction */
    PetscReal frac, f;
    /* The banded approximation */
    Mat B;
    /* The embedded PC */       
    PC pc;
  } PC_Banded;
\end{lstlisting}
  \onslide<3>
  Register a constructor for the concrete type:
  \bigskip
\begin{lstlisting}[basicstyle=\ttfamily\large]
  PCRegister("banded", PCCreate_Banded)
\end{lstlisting}
  \onslide<4>
  Define the constructor:
\begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
PetscErrorCode PCCreate_Banded(PC pc)
{
  PC_Banded     *b;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  /* Create concrete type struct */
  /* Setup function table for class */
  /* Setup concrete type-specific functions */
  /* Setup subobjects */
  PetscFunctionReturn(0);
}
\end{lstlisting}
  \onslide<5>
  Define the constructor:
\begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
PetscErrorCode PCCreate_Banded(PC pc)
{
  PetscFunctionBegin;
  ierr = PetscNewLog(pc, &b);CHKERRQ(ierr);
  pc->data = (void *) b;
  b->kmax = 50;
  b->frac = 0.95;
  /* Setup function table for class */
  /* Setup concrete type-specific functions */
  /* Setup subobjects */
  PetscFunctionReturn(0);
}
\end{lstlisting}
  \onslide<6>
  Define the constructor:
\begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
PetscErrorCode PCCreate_Banded(PC pc)
{
  PetscFunctionBegin;
  /* Create concrete type struct */
  pc->ops->apply               = PCApply_Banded;
  pc->ops->applytranspose      = NULL;
  pc->ops->setup               = PCSetUp_Banded;
  pc->ops->reset               = PCReset_Banded;
  pc->ops->destroy             = PCDestroy_Banded;
  pc->ops->setfromoptions      = PCSetFromOptions_Banded;
  pc->ops->view                = PCView_Banded;
  pc->ops->applyrichardson     = NULL;
  pc->ops->applysymmetricleft  = NULL;
  pc->ops->applysymmetricright = NULL;
  /* Setup concrete type-specific functions */
  /* Setup subobjects */
  PetscFunctionReturn(0);
}
\end{lstlisting}
  \onslide<7>
  Define the constructor:
\begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
PetscErrorCode PCCreate_Banded(PC pc)
{
  PetscFunctionBegin;
  /* Create concrete type struct */
  /* Setup function table for class */
  PetscObjectComposeFunction((PetscObject) pc,
    "PCBandedSetMaxHalfBandwidth_C", PCBandedSetMaxHalfBandwidth_Banded);
  PetscObjectComposeFunction((PetscObject) pc, 
    "PCBandedSetNormFraction_C", PCBandedSetNormFraction_Banded);
  /* Setup subobjects */
  PetscFunctionReturn(0);
}
\end{lstlisting}
  \onslide<8>
  Define the constructor:
\begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
PetscErrorCode PCCreate_Banded(PC pc)
{
  PetscFunctionBegin;
  /* Create concrete type struct */
  /* Setup function table for class */
  /* Setup concrete type-specific functions */
  {
    const char *prefix;

    PCCreate(PetscObjectComm((PetscObject) pc), &b->pc);
    PetscObjectGetOptionsPrefix((PetscObject) pc, &prefix);
    PetscObjectSetOptionsPrefix((PetscObject) b->pc, prefix);
    PetscObjectAppendOptionsPrefix((PetscObject) b->pc, "banded_");
  }
  PetscFunctionReturn(0);
}
\end{lstlisting}
  \onslide<9>
  Define concrete type-specific functions:
\begin{lstlisting}[basicstyle=\ttfamily\scriptsize]
PetscErrorCode PCBandedSetMaxHalfBandwith(PC pc, PetscInt kmax)
{
  PetscErrorCode ierr;

  PetscFunctionBegin;
  PetscValidHeaderSpecific(pc, PC_CLASSID, 1);
  PetscTryMethod(pc, "PCBandedSetMaxHalfBandwith_C",
    (PC, PetscInt), (pc, kmax));
  PetscFunctionReturn(0);
}
\end{lstlisting}
\end{overprint}
\end{frame}
%
\subsection{Distributing your new Implementation}
\begin{frame}{Distributing a Shared Library}\LARGE
\begin{overprint}
  \onslide<1>
  Package Initialization
  \begin{itemize}
    \item Register classes,
    \item concrete type constructors,
    \item logging events,
    \item and finalizer with \function{PetscRegisterFinalize()}
  \end{itemize}
  \onslide<2>
  Package Finalization
  \begin{itemize}
    \item Destroy list of constructors,
    \item and class memory allocations
  \end{itemize}
  \onslide<3>
  Package Loading
  \begin{itemize}
    \item Library located using\\{\Large\code{-dll\_append/prepend <libname>}}
    \item PETSc calls\\{\Large\function{PetscDLLibraryRegister\_<libname>()}}
    \item No recompiling or relinking of\\PETSc libraries or user code
  \end{itemize}
\end{overprint}
\end{frame}

\end{document}
