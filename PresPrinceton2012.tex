\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\usepackage{listings}

\title[Princeton]{Getting Modern Algorithms into the Hands of Working Scientists on Modern Hardware}
\author[M.~Knepley]{Matthew~Knepley}
\date[GAP]{Bridging the Gap Between the\\Geosciences and Mathematics, Statistics, and Computer Science\\Princeton, NJ \quad October 1--2, 2012\vskip-0.15in}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{abstract}

Title: Getting Modern Algorithms into the Hands of Working Scientists on Modern Hardware

I will talk about the role that scientific libraries play in allowing computational scientists to
both attack problems with efficient, scalable algorithms, and leverage new computational
resources. Good library design, encoding the relevant mathematical abstractions, allows
easy incorporation of multilevel, multiphysics algorithms into existing user code, as well
as massive parallelism and hybrid computing.

\end{frame}
%
\begin{frame}{Impact of this Project}\LARGE
The main impact of \blue{mathematics} will be\\
\bigskip
\pause\Huge
\ design/analysis of algorithms\\
\medskip
\pause
\ for simulation \& data analysis\\
\pause
\bigskip\bigskip\LARGE
This is where CS comes in \ldots
% Code is the transmission mechanism from mathematicians/comp. scientisits to rest of the field
\end{frame}
%
%
\section{Computational Science}
\begin{frame}{Big Idea}\Huge
The best way to create robust,\\
\medskip\pause
efficient and scalable,\\
\medskip\pause
maintainable scientific codes,\\
\pause
\begin{center}
is to use \red{libraries}.
\end{center}
\end{frame}
%
\begin{frame}{Why Libraries?}
\begin{itemize}\Large
  \item<1-> Hides hardware details
  \begin{center}
    \item \magenta{\href{http://www.mcs.anl.gov/mpi}{MPI}} does for this for machines and networks
  \end{center}
  \bigskip
  \item<2-> Hide Implementation Complexity
  \begin{center}
    \item \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc}} does for this Matrices and Krylov Solvers
  \end{center}
\end{itemize}
\end{frame}
% SAY: Q: Why is it just not good enough to make a fantastic working code?
%      A: Extensibility. You need to give users the ability to reform your approach to fit their problem.
%
\input{slides/GPU/WhyGPU.tex}
%
\subsection{Linear Algebra}
\input{slides/PETSc/GPU/VecCUDA.tex}
\input{slides/PETSc/GPU/MatCUDA.tex}
\input{slides/PETSc/GPU/Solvers.tex}
\input{slides/PETSc/GPU/PFLOTRANExample.tex}
\input{slides/PETSc/GPU/Example.tex}
%
\subsection{FEM Integration}
\input{slides/FEM/InterfaceMaturity.tex}
\input{slides/FEM/IntegrationModel.tex}
\input{slides/PETSc/FEMIntegration.tex}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_lap2d_block_gtx580.png} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Laplacian Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{/PETSc3/papers/gpu/gpu-fem-quadrature-paper/ex52_time.png} \\
  \Large Linear scaling for both GPU and CPU integration
\end{tabular}
\end{center}
\end{frame}
\begin{frame}{2D $P_1$ Rate-of-Strain Performance}
\begin{center}
\begin{tabular}{c}
  \includegraphics[width=0.8\textwidth]{figures/FEM/GPU/block_2D_P1_Elasticity} \\
  \Large Reaches \red{100} GF/s by 100K elements
\end{tabular}
\end{center}
\end{frame}
%
\begin{frame}{General Strategy}
\begin{itemize}\Large
  \item Vectorize
  \medskip
  \item Overdecompose
  \medskip
  \item Cover memory latency with computation
  \begin{itemize}
    \item Multiple cycles of writes in the kernel
  \end{itemize}
  \smallskip
  \item User must \blue{relinquish control of the layout}
\end{itemize}
\bigskip
\magenta{\href{http://arxiv.org/abs/1103.0066}{Finite Element Integration on GPUs}}, accepted ACM TOMS,\\
\quad Andy Terrel and Matthew Knepley.\\
{\bf Finite Element Integration with Quadrature on the GPU}, to SISC,\\
\quad Robert Kirby, Matthew Knepley, Andreas Kl\"ockner, and Andy Terrel.
\end{frame}
%   Needs decomposition by user
%
%% \input{slides/ScientificComputing/RobustDevelopment.tex}
%
%
\section{Mathematics}
\begin{frame}[fragile]{Composable System for Scalable Preconditioners}{Stokes and KKT}

The saddle-point matrix is a canonical form for handling constraints:
\begin{itemize}
  \item Incompressibility
  \item Contact
  \item Multi-constituent phase-field models
  \item Optimal control
  \item PDE constrained optimization
\end{itemize}

\smallskip

\includegraphics[width=0.4\textwidth]{./figures/Magma/ridgePlot.pdf}

\begin{textblock}{0.4}[0,1](0.35,0.92)
  \includegraphics[width=8cm]{./figures/Magma/ridgePorosity.jpg}\\
  {\tiny \qquad\qquad Courtesy R.~F. Katz}
\end{textblock}
\begin{textblock}{0.2}[1,0](0.9,0.3)
  {\tiny Courtesy M. Spiegelman}
  \includegraphics[width=3cm]{./figures/Magma/porousChannels.jpg}
\end{textblock}

% SAY: we are actively working on all these problems in PETSc.
\end{frame}
%
\begin{frame}{Composable System for Scalable Preconditioners}{Stokes and KKT}

There are \textit{many} approaches for saddle-point problems:
\begin{itemize}
  \item Block preconditioners % SAY: diagonal, triangular, physics-based
  \medskip
  \item Schur complement methods % SAY: many partial factorizations, different choices for $A$ and $S$ solvers
  \medskip
  \item Multigrid with special smoothers % SAY: also called seregated KKT smoothers
\end{itemize}
\begin{textblock}{0.2}[0.9,0.5](0.72,0.47)
  \begin{equation*}
    \begin{pmatrix} F & B & M \\ B^T & 0 & 0 \\ N & 0 & K\end{pmatrix} \begin{pmatrix} {\bf u}\\p\\T \end{pmatrix} = \begin{pmatrix} {\bf f}\\0\\q \end{pmatrix}
  \end{equation*}
\end{textblock}

\bigskip

However, today it is hard to \red{compare} \& \red{combine} them and combine in a {\bf hierarchical} manner.
\pause
For instance we might want,
\smallskip
\begin{overprint}
\onslide<3>
  \begin{tabular}{@{\hspace{3ex}}p{42em}}
  a Gauss-Siedel iteration between blocks of $({\bf u},p)$ and $T$,\\
  and a full Schur complement factorization for ${\bf u}$ and $p$.
  \end{tabular}
\onslide<4>
  \begin{tabular}{@{\hspace{3ex}}p{42em}}
  an upper triangular Schur complement factorization for ${\bf u}$ and $p$,\\
  and geometric multigrid for the ${\bf u}$ block.
  \end{tabular}
\onslide<5>
  \begin{tabular}{@{\hspace{3ex}}p{42em}}
  algebraic multigrid for the full $({\bf u},p)$ system,\\
  using a block triangular Gauss-Siedel smoother on each level,\\
  and use identity for the $(p,p)$ block.
  \end{tabular}
\end{overprint}

\end{frame}
%
\begin{frame}{Approach for efficient, robust, scalable linear solvers}

\vspace{-0.1in}
{\footnotesize
\begin{block}{\bf Need solvers to be:}
\begin{itemize}
  \item {\bf Composable}: separately developed solvers may be easily combined, by non-experts, to form a more powerful solver
  \smallskip
  \item {\bf Nested}: outer solvers call inner solvers
  \smallskip
  \item {\bf Hierarchical}: outer solvers may iterate over all variables for a global problem, while nested inner solvers handle smaller subsets of physics, smaller physical subdomains, or coarser meshes
  \smallskip
  \item {\bf Extensible}: users can easily customize/extend
\end{itemize}
\end{block}
}
\bigskip
\magenta{\href{http://www.mcs.anl.gov/uploads/cels/papers/P2017-0112.pdf}{Composable Linear Solvers for Multiphysics}}, IPDPS, 2012,\\
\quad J. Brown, M. G. Knepley, D. A. May, L. C. McInnes and B. F. Smith.
\end{frame}
%
%%\input{slides/PETSc/OrganizingPrinciple.tex}
%   Reusable pieces allow you to create algorithms not present in the package itself (Ex: Elman PCs, SIMPLE)
%   Add SIMPLE to Stokes Tour
\input{slides/MultiField/StokesOptionsTour.tex}
\input{slides/PETSc/ProgrammingWithOptions.tex}
%
\begin{frame}{Composability}
\Large \blue{Composable} interfaces allow the nested, hierarchical interaction of different components,
\pause
\begin{itemize}
  \item {\bf analysis} (discretization)
  \medskip
  \item {\bf topology} (mesh)
  \medskip
  \item {\bf algebra}  (solver)
\end{itemize}
\pause
so that non-experts can produce powerful simulations with modern algorithms.
\medskip
\pause
\begin{center}
  \magenta{\href{http://59a2.org/research/}{Jed Brown}} will discuss this interplay\\
  in the context of multilevel solvers
\end{center}
\end{frame}
%
%\begin{frame}{Code Changes for Modern Mathematics}
%
%\begin{center}\Large
%  More control will pass from user to library/compiler
%\end{center}
%\begin{itemize}
%  \item<2-> Kernels will be generated by the library
%  \begin{itemize}
%    \item[Ex]<3-> Autogenerated FEM integration
%  \end{itemize}
%
%  \item<4-> Partitioning will be controlled by the library
%  \begin{itemize}
%    \item[Ex]<5-> Partition for MPI and then for GPU
%  \end{itemize}
%
%  \item<6-> Communication will be managed by the library
%  \begin{itemize}
%    \item[Ex]<7-> Marshalling to GPU
%  \end{itemize}
%
%  \item<8-> Assembly will be controlled by the algorithm
%  \begin{itemize}
%    \item[Ex]<9-> Substructuring (\code{PCFieldSplit})
%  \end{itemize}
%\end{itemize}
%\end{frame}
%
%
\section*{Conclusions}
\begin{frame}{Main Points}

\Large
\begin{itemize}
  \item<2-> Libraries encapsulate the Mathematics
  \begin{itemize}
    \item \large Users will give up more Control
  \end{itemize}

  \bigskip

  \item<3-> Multiphysics demands Composable Solvers
  \begin{itemize}
    \item \large Each piece will have to be Optimal
  \end{itemize}
\end{itemize}

\bigskip

\begin{center}
\visible<4>{\emph{Change alone is unchanging\\--- Heraclitus, 544--483 \sc{BC}}}
\end{center}
%
\end{frame}

\end{document}
