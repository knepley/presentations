\documentclass{beamer}

\input{tex/talkPreamble.tex}
\usepackage{physics}
\usepackage{multimedia}
\usetikzlibrary{thplex,trees}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxcitenames=4, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{presentations.bib}
\renewcommand*{\bibfont}{\tiny}

\title[PETSc]{Thoughts on Composing Nonlinear Solvers}
\author[Matt]{Matthew~Knepley}
\date[DD25]{Domain Decomposition XXV\\St. John's, Newfoundland, CA \qquad July 27, 2018}
% - Use the \inst command if there are several affiliations
\institute[Buff]{
  Computer Science and Engineering\\
  University at Buffalo\\
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.15]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[height=0.65in]{figures/logos/UB_Primary.png}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}<testing>{Abstract}
Title: Thoughts on Composing Nonlinear Solvers

Abstract:
We present, in analogy with the linear preconditioning operation, a framework for the composition of nonlinear solvers,
which we call nonlinear preconditioning, in order to accelerate convergence and improve solver performance. A central problem
for this operation is that no rules of thumb exist for choosing the operations or composition strategy, and theory has been an
unreliable guide. We explore a possible alternative, the nondiscrete induction of Ptak, which could allow us to understand the
short time behavior of composed iterations.

\end{frame}
%
\begin{frame}<testing>{Ideas}
  Give updated talk from Brown:
    Explain composition
    Near rings
    Rates of convergence
    1D examples
    Proof for Banach spaces
    Large examples

    Partner talk on Implementation
    Skeleton of SNESSolve
    Impls for known solvers
    How does DD fit in?
  Intrastructure:
    Replicate Xiao-Chuan's work in PETSc
      %http://shihua-gong.org/files/poster/2017NE-IN-poster.pdf
      %https://www.researchgate.net/publication/315637371_A_scalable_nonlinear_fluid-structure_interaction_solver_based_on_a_Schwarz_preconditioner_with_isogeometric_unstructured_coarse_spaces_in_3D/references?citedPublicationKey=PB%3A50948431&citationKey=CIT%3A3284007161
      He already applies it to optimal control
    Extend it to multiple levels
  Application:
    Control problem for Yakutat
  Theory:
    Push forward on NDI
\end{frame}
%
\begin{frame}<testing>{NDI}
  - Explain problem of asymptotics for evaluating convergence
    - Composing linears is linear
      - Cannot evaluate constant
    - Composing quadratic with linear is quadratic
      - Cannot evaluate approach to quadratic basin
  - Explain relation of NDI to closed graph theorem
    - Show easy proof
    - Summarize results of others
  - Explain strategy for proving rate for composition
    - Show proof for one direction
    - Explain problem for other direction
  - Show example of composition
    - Richardson with Newton
      - Show difference if you swap, goes back to difference in Lipschitz constant of composed function
    - Extend example to Richardson with Newton for p-Laplacian or nonlinear Stokes
    - Try composing NGS, or one of Cai's DD solvers, with Newton
      - What does Cai say about the rate?
      - What can I say about the rate?
  - Can we evaluate the Lipschitz constant?
    - NO: You get two iterations which do have different basins, but you still cannot tell until it hits the basin
    - MAYBE: Can I find an iteration function that makes 'a' visible in some toplevel constant?
    - Can we get it by comparing composed and uncomposed iterations?
    - Can we get it by comparing two different compositions?
    - Can we get a local Lipscitz constant by comparing two local compositions?
  - Do any of my multilevel ideas about Nash-Moser work?

  - What makes a good nonlinear solver?
    - Robustness
      - When preconditioning Newton with Richardson, can we think of 'c' as capturing the local Lipschitz constant, so
        that it remove this dependence from Newton and make the convergence basin a universal size?
      - Does this require Richardson at all scales, namely FAS?
    - Convergence Rate
      - Experiments
        - Bratu
        - p-Laplacian
        - Nonlinear Stokes
        - One from Cai?
      - Look at FFT of error for Newton
        - Does it damp the high freq, or do something more complicated?
      - Show that NGS is linear
        - Does it capture the local Lipschitz constant in its 'c'?
      - Compose with Newton
        - Does it replicate the 1D behavior?
    - Scalability
      - Newton convergence rate is independent of size for PDE (Allgower?)
    - When does using Newton make sense, since there is an imbalance between the linear decrease in discretization error
      with hierarchy, and the superlinear decrease in algebraic error with Newton
\end{frame}
%
\begin{frame}<testing>{Schedule}
\begin{itemize}
  \item Week  0 (4/30)\\
    Send in Abstract
  \item Week  1 (5/7)\\
    Review slides from ICERM\\
    Read NDI paper thoroughly\\
    Repeat 1D tests
  \item Week  2 (5/14)
    Rought out slides for DD25\\
    Go over Cai papers for examples I can do\\
    Start nonlinear PATCH\\
  \item Week  3 (5/21)
    Type up existing proof nicely\\
    Try to reproduce NDI convergence with Newton for Bratu\\
    Try to get linear convergence with Picard for Bratu\\
  \item Week  4 (5/28)
    Finish draft intro slides for DD25\\
    Try to reproduce NDI convergence with Newton for ex77\\
    Try to get linear convergence with Picard for ex77\\
  \item Week  5 (6/4)
    Extend NDI proof for other direction\\
    Run nonlinear PATCH for Bratu\\
    Implement a Cai example\\
  \item Week  6 (6/11)
    Run nonlinear PATCH for VV Stokes\\
    Run nonlinear PATCH for Cai example\\
  \item Week  7 (6/18)
  \item Week  8 (6/25)
  \item Week  9 (7/2)
  \item Week 10 (7/9)
  \item Week 11 (7/16)
\end{itemize}
\end{frame}
%
\input{slides/NPC/PioneeringWork.tex}
%
%
\section{Composition Strategies}
\input{slides/SNES/BasicSystem.tex}
\input{slides/SNES/LeftPrec.tex}
\input{slides/SNES/AdditiveNPrec.tex}
\input{slides/SNES/LeftNPrec.tex}
\input{slides/SNES/MultiplicativeNPrec.tex}
\input{slides/SNES/RightNPrec.tex}
\input{slides/SNES/NPrecTable.tex}
%
%
\section{Solvers}
\subsection{Richardson}
\input{slides/SNES/NRICH.tex}
\input{slides/SNES/NRICH-LP.tex}
\subsection{Newton}
\input{slides/SNES/NK.tex}
\input{slides/SNES/LP-NK.tex}
\input{slides/SNES/RP-NK.tex}
\subsection{Generalized Broyden}
\input{slides/SNES/GB.tex}
\input{slides/SNES/LP-GB.tex}
\input{slides/SNES/RP-GB.tex}
%
%
\section{Examples}
\begin{frame}
\begin{center}\Huge
I ran NPC on some problem\\

and it worked.
\end{center}
\end{frame}
%
%
\section{Convergence}
%
\subsection{Nondiscrete Induction}
\input{slides/NDI/RateOfConvergence.tex}
\input{slides/NDI/NDI.tex}
\input{slides/NDI/Newton.tex}
%
\begin{frame}{Chord Method}
If we define the approximate set
\begin{align}\label{eq:chordZ}
  Z(r) = \{u \in X; \|u - u_0\| \le \sigma_{\mathcal{R}}(r_0) - \sigma_{\mathcal{R}}(r), \|f'^{-1}(u_0) f(u)\| \le r \},
\end{align}
we get a rate of convergence
\begin{align}
  \omega_{\mathcal{R}}(r) = \frac{1}{2} k r^2 + r \left( 1 - \sqrt{1 - 2 k (r_0 - r)} \right)
\end{align}
and corresponding estimate function
\begin{align}
  \sigma_{\mathcal{R}}(r) = \frac{1}{k} \sqrt{a^2 + 2 k r} - a.
\end{align}
\end{frame}
\begin{frame}{Chord Method}

For rate of convergence,
\begin{align}
  \omega_{\mathcal{R}}(r) = \frac{1}{2} k r^2 + r \left( 1 - \sqrt{1 - 2 k (r_0 - r)} \right)
\end{align}
\begin{overprint}
\onslide<1>
For $r \approx r_0$,
\begin{align}
  \omega_{\mathcal{R}}(r) &\approx k r_0 r - \frac{1}{2} k r^2
\end{align}
\onslide<2>
For $r \approx r_0$,
\begin{align}
  \omega_{\mathcal{R}}(r) &\approx k r_0 r - \frac{1}{2} k r^2
\end{align}
For $r \ll r_0$,
\begin{align}
  \omega_{\mathcal{R}}(r) &\approx (1 - k a) r + \frac{1}{2} k r^2.
\end{align}
\onslide<3>
The later constant is greater than the former,
\begin{align}
  1 - \sqrt{1 - 2 k r_0}  &> k r_0 \\
  1 - k r_0               &> \sqrt{1 - 2 k r_0} \\
  1 - 2 k r_0 + k^2 r^2_0 &> 1 - 2 k r_0 \\
  k^2 r^2_0               &> 0
\end{align}
\onslide<4>
\begin{center}\LARGE
  Convergence \blue{decelerates} nearer\\
  to the solution.
\end{center}
\end{overprint}
\end{frame}
\begin{frame}{Chord Method}

Vertical lines at onset of asymptotic convergence ($\|x_{n+1}-x_n\| \approx a/e$)
\begin{center}
  \includegraphics[width=0.8\textwidth]{figures/SNES/ChordDeceleration.png}
\end{center}
\end{frame}
%
%
\subsection{Nonlinear Preconditioning}
\input{slides/SNES/NPCLeftVsRight.tex}
\input{slides/NDI/RP.tex}
\input{slides/NDI/NonAbelian.tex}
\input{slides/NDI/AsymmetryExample.tex}
%
\subsection{Theory}
\input{slides/NDI/ComposedRates.tex}
\input{slides/NDI/MultidimensionalInductionTheorem.tex}
\input{slides/NDI/ComposedNewtonConv.tex}
%
\subsection{PDE Example}
\input{slides/SNES/SNESEx19.tex}
\input{slides/SNES/SNESEx19Deceleration.tex}
\input{slides/SNES/SNESEx19Stagnation.tex}
\input{slides/SNES/SNESEx19NewtonNRich.tex}
%
\subsection{Exploration}
\input{slides/SNES/NPCEx19.tex}
%
%% TODO Example of looking at convergence of residuals ala Cai
%
% ex19_matt_bad diverges
%  run with -snes_monitor_residual draw
%  Both Omega and T really suck, and X velocity has a boundary layer it appears
%
%
\section{Further Questions}
\begin{frame}
\begin{center}
  \bf\LARGE Further Questions
\end{center}
\begin{itemize}
  \item What is $\omega$ for NASM? Nonlinear FETI-DP?
  \bigskip
  \item What are the Rules of Thumb for NPC?
  \bigskip
  \item Can a composed iteration have a larger region of convergence?
  \bigskip
  \item What API makes sense for simulations?
\end{itemize}
\end{frame}
%
%
\input{slides/PETSc/ProgrammingWithOptions.tex}
%% TODO Draw a picture of this solver
\input{slides/PETSc/MagmaFASOptions.tex}
%
%
\section*{Algebra}
\input{slides/SNES/AdditiveAlgebra.tex}
\input{slides/SNES/MultiplicativeAlgebra.tex}
\input{slides/SNES/NearRings.tex}
\input{slides/SNES/PolynomialDecomposition.tex}
\end{document}
