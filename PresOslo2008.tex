\documentclass{beamer}

\input{tex/talkPreamble.tex}
\beamertemplatenavigationsymbolsempty

\title[Theory]{Theoretical Foundations}
\author[M.~Knepley]{Dmitry Karpeev~\inst{1,2}, Matthew~Knepley~\inst{1,2}, and Robert Kirby~\inst{3}}
\date[Simula '08]{Foundations of Finite Element Computing\\Simula Research, Oslo, Norway\\August 3-10, 2008}
% - Use the \inst command if there are several affiliations
\institute[ANL,TTU]{
  \begin{tabular}{cc}
  \inst{1}Mathematics and Computer Science Division & \inst{2}Computation Institute\\
  Argonne National Laboratory               & University of Chicago
  \end{tabular}\\
  \inst{3}
  Department of Computer Science\\
  Texas Tech University
}

\subject{Finite Element Computing}

\AtBeginPart{\frame{\partpage}}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

%\includeonlylecture{Introduction}

\begin{document}

\begin{frame}
\titlepage
\begin{center}
\includegraphics[scale=0.3]{figures/logos/doe-logo.jpg}\hspace{1.0in}
\includegraphics[scale=0.3]{figures/logos/anl-logo-black.jpg}\hspace{1.0in}
\includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
\end{center}
\end{frame}

% Possible order of talks:
%
% Monday:
% 1) Overview (Matt) 1/2 hour
% 2) Sieve Concepts (Dmitry) 1 hour
% 1/2 hour Discussion
%
% Tuesday:
% 3) FEM Transformations (Rob) 1 1/2 hours
% 1/2 hour Discussion
% 
% Wednesday:
% 4) Sieve Design (Dmitry) 1 1/2 hours
% 1/2 hour Discussion
%
% Thursday:
% 5) Sieve Implementation (Matt) 1 hour
% 6) FMM in Sieve (Matt) 1/2 hour
% 1/2 hour Discussion
%
% Friday:
% 7) Open

%Rob's comments:
%I suggest a chiastic structure to the presentations (MGK: I don't think this is understandable)
%  starting globally (variational forms, Sieves),
%  local things via restriction of global things.
%    This is natural in that you think of finite element spaces merely as a convenient way of constructing subspaces for
%    abstract Galerkin's method
%   This is where to insert the FIAT theory for element construction + transformation.
%  global level and use Sieves to assemble the pieces that FIAT constructs.
%    This might be where things like FMM & MG
% MGK: I think splitting the global section reduces its impact
% MGK: Should we have a section on library design?
%      see slides/MPI/LibraryDesign.tex

% Indirection is a unifying theme
%   Index-directed composition
%   Dmitry: Expose more
%     Give the user a fundamental map object which can be composed
%   Matt: Hide more
%     Allow complex indices (like stencil, element), which is enabled by composition
% Performance makes implementation nontrivial
%   Does interface bifurcate based upon the implementation?
%   Ex. Composition of visitors vs. pullback
% How do we get representational flexibility?
%   We can provide a set of ops that supports a large univers of computations
%     Compare to previous schemes
%   Ex. Express everything as operations on Sifters

\lecture{Introduction}{Introduction}
\part{Introduction}
\begin{frame}<testing>
\frametitle{Abstract}

    Programmability remains the main challenge in high performance scientific software today. The lack of common
conceptual pieces among various methods and application domains clearly contributes to this problem. We will present an
abstract formulation of the finite element paradigm: restrict--compute--complete, and discuss implementations 
of this principle, some undergoing active research, that conform to a common compact interface. The implementations will
be specialized to the different application domains. Not only does this approach significantly reduce the complexity of
common FEM usage, but also the developed components can be reused in many other domains, such as optimal direct solvers
of the Fast Multipole Method type.  

   We discuss a conceptualization of finite element computing in the usual terms, breaking it into locally homogeneous
computational kernels, and also the methodology for systematic assembly of local computation into a global whole. We
will discuss contemporary techniques that take advantage of the ubiquity of the local kernels and optimize their use.
We also show that the same paradigm of global assembly of local pieces can be used to conceptualize FEM computation,
parallel data management and hierarchical solvers.
\end{frame}
%
\section{Scientific Computing}
\input{slides/ScientificComputing/Problems.tex}
\input{slides/ScientificComputing/SystemsInteraction.tex}
\input{slides/ScientificComputing/FutureCompilers.tex}
\input{slides/Automation/Spiral.tex}
\input{slides/Automation/FLASH.tex}
\input{slides/ScientificComputing/HierarchicalRepresentation.tex}
%
\section{Hierarchy}
\input{slides/Sieve/HierarchicalDesign.tex}
\input{slides/Optimal/ManifoldExample.tex}
\input{slides/Optimal/FEMExample.tex}
\input{slides/Sieve/GlobalAndLocal.tex}
\input{slides/Optimal/Payoff.tex}
\input{slides/Optimal/WhyOptimalAlgorithms.tex}
\input{slides/Sieve/OverviewII.tex}
\input{slides/Sieve/DoubletNavigation.tex}
\input{slides/Section/DoubletSectionNavigation.tex}
\input{slides/Completion/DoubletDistribution.tex}
\input{slides/Overlap/DoubletOverlap.tex}
\input{slides/Overlap/CompletionUses.tex}
\section*{Conclusions}
\begin{frame}
\frametitle{Benefits}
\begin{center}
  {\Large Better mathematical abstractions\\bring concrete benefits}
\end{center}

\begin{itemize}
  \item Vast reduction in complexity
  \begin{itemize}
    \item Declarative, rather than imperative, specification

    \item Dimension independent code
  \end{itemize}

  \bigskip

  \item Opportunites for optimization
  \begin{itemize}
    \item Higher level operations missed by traditional compilers

    \item Single communication routine to optimize
  \end{itemize}

  \bigskip

  \item Expansion of capabilities
  \begin{itemize}
    \item Easy model definition

    \item Arbitrary elements

    \item Complex geometries and embedded boundaries
  \end{itemize}
\end{itemize}
\end{frame}
%
%%%%
\lecture{Global Computation: Theory}{GlobalTheory}
\part{Global Computation: Theory}
\section{Hierarchy}
\input{slides/Sieve/HierarchicalDesign.tex}
\input{slides/Optimal/Payoff.tex}
\input{slides/Optimal/WhatIsOptimal.tex}
\input{slides/Optimal/WhyOptimalAlgorithms.tex}
\input{slides/Optimal/ManifoldExample.tex}
\input{slides/Optimal/FEMExample.tex}
%
\section{Representing Topology}
\input{slides/Sieve/OverviewII.tex}
\input{slides/Sieve/BasicOperations.tex}
\input{slides/Sieve/SieveDef.tex}
\input{slides/Sieve/DoubletNavigation.tex}
\input{slides/MeshDistribution/MeshDual.tex}
\subsection{Mesh Distribution}
\input{slides/MeshDistribution/MeshDistribution.tex}
\input{slides/MeshDistribution/MeshPartition.tex}
\input{slides/Completion/DoubletDistribution.tex}
\input{slides/MeshDistribution/SectionDistribution.tex}
\input{slides/MeshDistribution/SieveDistribution.tex}
\input{slides/MeshDistribution/DistributionExample2D.tex}
\input{slides/MeshDistribution/DistributionExample3D.tex}
%
\section{Representing Functions}
\input{slides/Section/Sections.tex}
\input{slides/Section/BasicOperations.tex}
\input{slides/Section/Duality.tex}
\input{slides/Section/DoubletSectionNavigation.tex}
%
\section{Mapping Interpretation}
\input{slides/Section/Mapping.tex}
\input{slides/Section/Composition.tex}
%
\section{Connecting Sieves}
\input{slides/Overlap/SievesOfSieves.tex}
\input{slides/Overlap/DoubletOverlap.tex}
\input{slides/Overlap/Completion.tex}
\input{slides/Overlap/CompletionUses.tex}
%%%%
\lecture{Global Computation: Implementation}{GlobalImpl}
\part{Global Computation: Implementation}
\section{Interfaces}
\input{slides/Sieve/HierarchicalInterfaces.tex}
\input{slides/Sieve/UnstructuredInterfaceBefore.tex}
\input{slides/Sieve/CombinatorialTopology.tex}
\input{slides/Sieve/UnstructuredInterfaceAfter.tex}
\input{slides/Sieve/HierarchicalAbstractions.tex}
\input{slides/FMM/FMMInSieve.tex}
\input{slides/Optimal/MGInSieve.tex}
%
\section{Mapping}
\input{slides/Sieve/Implementation/Traversal.tex}
\input{slides/Sieve/Implementation/VisitorComposition.tex}
%
\section{Completion}
\input{slides/MeshDistribution/SectionDistribution.tex}
\input{slides/Section/Implementation/Completion.tex}
\input{slides/Section/Implementation/CompletionDiagram.tex}
\input{slides/Section/Implementation/SectionHierarchy.tex}
\input{slides/Overlap/Implementation/PartialAssembly.tex}
% Picture of partial assembly for matvec
%
\section{Optimization and the Sieve Programming Model}
\subsection{Automation}
\input{slides/Automation/Kernels.tex}
\input{slides/Automation/DenseLinearAlgebra.tex}
\input{slides/Automation/DFT.tex}
\input{slides/Automation/SparseLinearAlgebra.tex}
\input{slides/Automation/PerformanceInsights.tex}
\input{slides/Automation/Sieve.tex}
\subsection{Parallelism}
\input{slides/MPI/MPICHG2.tex}
\input{slides/MPI/MPIHierarchy.tex}
\subsection{Completion}
\input{slides/Overlap/Implementation/Optimization.tex}
%% Slicing for optimization of mapping composition
\subsection{Interval Sieves}
\input{slides/Sieve/Implementation/IntervalSieve.tex}
\input{slides/Sieve/Implementation/ISieve.tex}
%% Dmitry's ASieves
\input{slides/Section/Implementation/ISection.tex}
%
\section{Finite Elements}
\input{slides/FEM/ResidualCode.tex}
%
\section{Boundary Conditions}
\input{slides/FEM/BoundaryConditions.tex}
\input{slides/FEM/DualBasisApplication.tex}
\input{slides/FEM/DirichletAssembly.tex}
\input{slides/FEM/DirichletValues.tex}
\input{slides/FEM/DirichletProjector.tex}
%%%%
\lecture{Local Computation: Theory}{LocalTheory}
\part{Local Computation: Theory}
\section{FIAT}
% Rob:
%   Factoring analytic and geometric pieces
%   Linear algebraic formulation
%   Handling moments
%   Handling jets (gradients)
%   Handling tensors
%
\section{Models of Local Computation}
%   From Optimizing the Evaluation of Finite Element Matrices with Rob/Matt/Anders/Ridg
\input{slides/FEM/FormDecomposition.tex}
\input{slides/FEM/FormDecompositionMultiLinear.tex}
\input{slides/FEM/FormDecompositionIsoparametric.tex}
%
\section{Dof Kinds}
% Rob: Theory of Transformations
%   Handling kinds of dof
%     Factor local assembly into transform/point and integration
%
\section{Boundary Conditions}
\input{slides/FEM/DirichletAssembly.tex}
\input{slides/FEM/DirichletValues.tex}
\input{slides/FEM/DirichletProjector.tex}
%
\section{Weak Form Languages}
\input{slides/FFC/FFC.tex}
%%%%
\lecture{Local Computation: Implementation}{LocalImpl}
\part{Local Computation: Implementation}
\section{Serial Performance}
\input{slides/Performance/Streams.tex}
\input{slides/Performance/SMVAnalysis.tex}
\input{slides/Performance/ImprovingSerialPerformance.tex}
\input{slides/FEM/PerformanceTradeoffs.tex}
\section{FIAT}
\input{slides/FEM/FIAT.tex}
\input{slides/FEM/FIATIntegration.tex}
\subsection{Implementation}
\subsection{Optimization}
% Rob:
%   Level 3 BLAS
%
\section{FErari}
\input{slides/FErari/Overview.tex}
\subsection{Problem Statement}
\input{slides/FEM/FormDecomposition.tex}
\input{slides/FEM/FormDecompositionMultiLinear.tex}
\input{slides/FEM/FormDecompositionIsoparametric.tex}
\input{slides/FEM/ElementMatrixFormation.tex}
\input{slides/FEM/ElementMatrixComputation.tex}
\input{slides/FErari/AbstractProblem.tex}
\subsection{Plan of Attack}
\input{slides/FErari/ComplexityReducingRelations.tex}
\input{slides/FErari/BinaryAlgorithm.tex}
% Picture of Prim's algorithm, and go over complexity analysis
\input{slides/FErari/Coplanarity.tex}
% Picture of randomized projection (can we give complexity analysis? If not, give numbers)
\subsection{Results}
\input{slides/FErari/Results.tex}
\subsection{Mixed Integer Linear Programming}
\input{slides/FErari/MINLPModeling.tex}
\input{slides/FErari/MINLPFormulation.tex}
\input{slides/FErari/MINLPFormulationSparse.tex}
\input{slides/FErari/MINLPResults.tex}
%
\section{Scheduling and Asynchronous Computation}
%   Inspector/Executor
%     Make a picture of a schedule and then the subsequent execution (could use Paolo's talk from FEniCS)
%     Make a Wikipedia page for this paradigm
%   FLAME/FLASH
%     SuperMatrix
%%%%
\lecture{Fast Methods}{FastMethods}
\part{Fast Methods}
\section{The Fast Multipole Method}
\subsection{Spatial Decomposition}
\input{slides/FMM/FMMInSieve.tex}
\input{slides/FMM/TreeImplementation.tex}
\input{slides/FMM/TreeInterface.tex}
\subsection{Data Decomposition}
\input{slides/FMM/Sections.tex}
% Maybe a picture
\subsection{Serial Implementation}
\input{slides/FMM/EvaluatorInterface.tex}
\input{slides/FMM/KernelInterface.tex}
\subsection{Parallel Spatial Decomposition}
%   Picture of parallel decomposition of tree
\input{slides/FMM/ParallelTreeImplementation.tex}
%   Picture of partitioning graph
\input{slides/FMM/ParallelTreeInterface.tex}
\input{slides/FMM/ParallelDataMovement.tex}
\input{slides/FMM/ParallelEvaluator.tex}
\subsection{Parallel Performance}
%   Performance data
\input{slides/FMM/RecursiveParallelism.tex}
%
%\section{The MultiLevel Method}
%   Skeel's MLM
%   Local meshes as Sieves
%   Sections for fields
%   Completion
%
\section{Multigrid}
\subsection{Structured}
%   DAs, what is right and wrong
%     Slide from Sandia Santa Fe talk
\input{slides/DA/DAIsMoreThanAMesh.tex}
\input{slides/DA/LocalNumbering.tex}
\input{slides/DA/LocalFunction.tex}
\input{slides/DA/BratuResidual.tex}
\input{slides/DA/LocalJacobian.tex}
\input{slides/DA/SetValuesStencil.tex}
\input{slides/DA/UpdatingGhosts.tex}
\input{slides/Optimal/DMMGIntegrationWithSNES.tex}
\input{slides/Optimal/Structured.tex}
\subsection{Unstructured}
\input{slides/Optimal/Unstructured.tex}
\input{slides/Poisson/ReentrantRefinement.tex}
\input{slides/Optimal/NonOptimalMesh.tex}
\input{slides/UnstructuredMG/WhyMultigridII.tex}
\input{slides/UnstructuredMG/UMGRequirements.tex}
\input{slides/UnstructuredMG/FBC.tex}
\input{slides/UnstructuredMG/DecimationAlgorithm.tex}
\input{slides/UnstructuredMG/SieveAdvantages.tex}
\input{slides/UnstructuredMG/ExampleProblem3D.tex}
\input{slides/UnstructuredMG/GMGPerformanceIII.tex}
\input{slides/UnstructuredMG/HierarchyQualityII.tex}
%   Easy relation to MLM
%%%%
%\lecture{Sample Application: The Bratu Problem}{Bratu}
%\part{Sample Application: The Bratu Problem}
% Can use tutorial slides
% Illustrate step by step the implementation of FEM concepts with Sieve
%%%%
%\lecture{Sample Application: Active Suspensions}{Microtubles}
%\part{Sample Application: Active Suspensions}
% Discussion of microtubles and bacteria
% Ion channels and baths
%%%%
\lecture{Sample Application: Fault Mechanics}{PyLith}
\part{Sample Application: Fault Mechanics}
\section{Formulation}
% Discussions of PyLith
\input{slides/PyLith/StrikeSlipBenchmark.tex}
%  Quasistatic equations
%    picture
%  Dynamic equations
%    picture
\section{Mesh Handling}
\input{slides/PyLith/MultipleMeshTypes.tex}
% Picture of mesh, fault mesh, ground surface, and maybe boundary
\section{Parallelism}
\input{slides/PyLith/Parallelism.tex}
% Scalability data
\section{Fault Handling}
% Description of new system of equations with faults
\input{slides/PyLith/CohesiveCells.tex}
\input{slides/PyLith/MeshSplitting.tex}
\section{Coupling}
% Coupling
%   Proposed structure

\end{document}
