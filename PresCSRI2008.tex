\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[Parallelism]{Parallelism for the Very Large and Very Small}
\author[M.~Knepley]{Matthew~Knepley}
\date[CSRI '08]{Scientific Libraries: MPI \& Multicore Issues and Plans\\Sandia CSRI Workshop\\Bishop's Lodge, NM\\June 3, 2008}
% - Use the \inst command if there are several affiliations
\institute[ANL]{
  Mathematics and Computer Science Division\\
  Argonne National Laboratory}

\subject{Parallelism in Libraries}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\frame{
\titlepage
\begin{center}
\includegraphics[scale=0.3]{figures/logos/doe-logo.jpg}\hspace{1.0in}
\includegraphics[scale=0.3]{figures/logos/anl-logo-black.jpg}\hspace{1.0in}
\includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
\end{center}
}

% Workshop Questions:
%   Where does MPI suck?
%   What do I move to, and how do I do it?
%   Do I need new algorithms?
% Notes
%   My answer to multicore parallelism is code generation, because I think it needs to be algorithm specific (reside in libraries)
%     HCE and Koch advocate systems under MPI for multicore parallelism
%% MPI is the Newtonian Mechanics of Communication Interfaces
%%   Its easy, general, and imperfect
%%   For instance, Lagrangian mechanics is more general and maybe more beautiful
\input{slides/MPI/WhereAreTheProblems.tex}

% Idea is to draw an analogy with problems that lead to new physics
\section{Multicore Parallelism}
%   Very Small: Quantum <--> Multicore
\input{slides/MPI/VerySmall.tex}
\input{slides/Automation/CodeGeneration.tex}
\input{slides/Automation/Spiral.tex}
\input{slides/Automation/FLASH.tex}
%%%     Work by Michelle Strout
%%%     Work by Manticore
\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
  \item Circumscribe algorithmic domain

  \bigskip

  \item Specialize to algorithm/hardware with code generation

  \bigskip

  \item Runtime decisions informed by high level information
\end{itemize}
\end{frame}
%   Very Large: General Relativity <--> Hierarchy
\section{Multiprocessor Parallelism}
\input{slides/MPI/VeryLarge.tex}
\input{slides/Sieve/HierarchicalDesign.tex}
%% The point here is that algorithms looks like this as well (its not just a hardware thing)
%%\input{slides/Sieve/GlobalAndLocal.tex}
\input{slides/MPI/MPICHG2.tex}
\input{slides/Sieve/OverviewII.tex}
\input{slides/Overlap/CompletionUses.tex}
\input{slides/Sieve/HierarchicalInterfaces.tex}
%%%\input{slides/Sieve/DoubletNavigation.tex}
%%%     Work by Barry on DA
\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
  \item Have concise, abstract, flexible interface for hierarchy

  \bigskip

  \item Need support for interaction with communication primitives

  \bigskip

  \item Specialized networks cannot currently implement sophisticated tree algorithms
\end{itemize}
\end{frame}
\section{Conclusion}
\begin{frame}
\frametitle{Conclusions}

\begin{itemize}
  \item Multicore performance should be improved with:
  \begin{itemize}
    \item Better code generation and runtime tools

    \medskip

    \item Algorithmic specificity
  \end{itemize}

  \bigskip

  \item Multiprocess scalability should be improved with:
  \begin{itemize}
    \item Explicitly hierarchical interfaces/libraries

    \medskip

    \item Better interaction of algorithms with communication
  \end{itemize}
\end{itemize}
\end{frame}

\section{Questions and Answers}
\begin{frame}
\frametitle{Question 15}
\begin{center}
  Are there extensions that can be made to MPI so that MPI is more amenable to writing scalable applications and to
  building next-generation libraries and languages?
\end{center}
\end{frame}
\begin{frame}
\frametitle{Answer 15}
\begin{center}
  Hierarchy is the key notion in nearly every optimal algorithm known. For example, the solvers Multigrid (MG), Fast
Multipole Method (FMM), and FETI are all based upon a hierarchical decomposition of the problem domain, which is then used to
aggregate the effects of local, usually linear, operations. Use of these algorithms, particularly in large community
codes, has been impeded by the high cost of implementation. However, they are arguably key to the scalability and
efficiency of application codes on next-generation exascale architectures. Simple hierarchical extensions to MPI could
greatly ease the implementation process, and result in much faster and more scalable applications. For instance, a
hierarchical relation between communicators could be directly mapped onto tree algorithms, such as FMM.

Moreover, modern implementations of MPI must reduce the data stored per node in order to avoid explosion of local storage
for the implementation. Space efficient implementations of MPI would make use of these same hierarchical
structures. Exposing these relations to the user will make them more aware of the cost certain operations would impose,
such as global operations over the communicator.

  Finally, it might be possible to extend the hierarchy interface to accomodate lower level extensions as well. For
instance, the programmer could interact with threads in a hybrid MPI+Thread models through a lightweight ``child
communicator'' and operations, such as dynamic task scheduling, would be confined to this communicator.
\end{center}
\end{frame}

\section{Questions and Answers}
\begin{frame}
\frametitle{Question 23}
\begin{center}
  HPC is small compared to the commercial software market. What are commercial leaders like Microsoft and Google doing
  to prepare for an era of multicore/manycore parallelism, and how will this affect the scientific HPC world?
\end{center}
\end{frame}
\begin{frame}
\frametitle{Answer 23}
\begin{center}
  The most significant development in the commercial software market for HPC is the recent practicality of outsourced
computation. The enabling technology for this development is exactly the same as that which enabled the huge growth in
portable numerical libraries over the past two decades, namely abstraction of a large set of community problems to a
common algorithmic domain. With a common algorithmic language, users can encode individual problems which can then be
run by any computation service.

  The best known example of this paradigm in the Google MapReduce implementation. However, other large players now offer
much the same service to any computing customer, for instance Yahoo with Hadoop and Amazon with EC2. Outsourced
computing greatly expands the notion of computing facilities, today embodied by the national centers for computation
such as NERSC. This also opens the door to centralized storage of {\emph and} computation on large scientific data
sets. In essence, bringing computing to the data, rather than data to the computing.

  There is a great opportunity to expose the commonality between HPC and business computations. For example, Rich Vuduc
and collaborators have shown that FMM, a scalable HPC solver, and k-NN, used extensively in analysis of commercial data,
have a common structure which they term ``generalized MapReduce''.
\end{center}
\end{frame}

\section{Questions and Answers}
\begin{frame}
\frametitle{Question 29}
\begin{center}
  Will hierarchical problem decomposition (I call it fractal or self-similar computing) get around the billion thread
  programming problem (nobody is smart enough to develop billion thread codes that do anything significant)?
\end{center}
\end{frame}
\begin{frame}
\frametitle{Answer 29}
\begin{center}
  % Key point: change your algorithm
  Yes, hierarchy is the key to better, more scalable algorithms. However, without sufficient computation to occupy each
thread, we will not make efficient use of the machine. Migration to algorithms which have better balance between
computation and communication/memory bandwidth will likely entail refactoring current applications and production of
high quality middleware encapsulating both the dependency structure for computations and the task scheduling and
dispatch procedure.

  Machine hierarchies will also play a role in managing exascale execution. We believe that specialized networks will be
a key component of scalable performance for these algorithms. For example, the reduction network on BG/L allows Krylov
methods to continue scaling to thousands of processors while utilizing many dot products. Extension of these networks to
support scans with matrix operations would enable an even wider array of scalable algorithms, such as FMM or MG.
\end{center}
\end{frame}

\begin{frame}
\frametitle{A New Standard?}
\begin{center}
  MPI provides a good interface for data parallel algorithms. However, the extensions to task parallelism are confusing,
incomplete, and sometimes slow. OpenMP does provide an interface for task parallelism. However, it does not abstract the
main operations and relegates much of the user control to environment variables, rather than an API. Moreover, basic
operations are absent. For example, we would like the system to accept a computation DAG from the application and use
this to schedule taks dynamically at runtime. Thus, we might consider an effort to produce a standard, similar to MPI,
which encapsulated task parallel algorithms.
\end{center}
\end{frame}

\end{document}
