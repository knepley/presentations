\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[GPU]{Tree-based methods on GPUs}
\author[M.~Knepley]{Felipe~Cruz\inst1 and Matthew~Knepley\inst2${}^,$\inst3}
\date[LSU]{Department of Mathematics, LSU\\Baton Rouge, LA \qquad Sept 25, 2009}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  \inst1
  Department of Mathematics\\
  University of Bristol

  \smallskip

  \inst2
  Computation Institute\\
  University of Chicago

  \smallskip

  \inst3
  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}

\subject{Multicore FMM}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\section{Introduction}

%% New Slides
% 1) Timeline for development of PetFMM
% 2) CPU to MultiGPU 
% 3) Why its PetFMM?
% 4) What to do about limited CPU bandwidth for multiGPU on same board
% 5) What about Nehalem performance? Larrabee?
%
\begin{frame}<testing>
\frametitle{Abstract}
We examine the performance of the Fast Multipole Method on heterogeneous computing devices, such as a CPU attached to an
Nvidia Tesla 1060C card. The inherent bottleneck imposed by the tree structure is ameliorated by a refactoring of the
algorithm which exposes the fine-grained dependency structure. Also, common reduction operations are refactored in order
to maximize throughput. These optimizations are enabled by our concise yet powerful interface for tree-structured
algorithms. Examples of performance are shown for problems arising from vortex methods for fluids, and extensions to the
Stokes problem and linear elasticity are discussed.
\end{frame}
%
\begin{frame}<testing>
\frametitle{Main Idea}
Although computational results are the \emph{sine quibus non} for scientific computing, in this talk I want to
concentrate on the interface. Enabling applications depends more on programmability than on performance I contend. In
this, I am following Robert van de Geijn and his FLAME philosophy. Our extenstion of the FLASH interface to tree-based
solvers will greatly expand the universe of optimal methods available on multicore architectures.
\end{frame}
%
%%%\begin{frame}
%%%\begin{center}
%%%  \includegraphics[width=4in]{figures/signatures/knepleySignatureChinese}
%%%\end{center}
%%%\end{frame}
%
\input{slides/ScientificComputing/Challenge.tex}
\input{slides/ScientificComputing/StructureVsTradeoffs.tex}
%%\input{slides/Automation/CodeGeneration.tex}
\input{slides/ScientificComputing/HierarchicalRepresentation.tex}
\input{slides/Automation/Spiral.tex}
\input{slides/Automation/FLASH.tex}
%
%
\section{Short Introduction to FMM}
%
\input{slides/FMM/Applications.tex}
\input{slides/FMM/Basics.tex}
\input{slides/FMM/PetFMM.tex}
\input{slides/FMM/PetFMM_Performance.tex}
%
\subsection{Spatial Decomposition}
\input{slides/FMM/SpatialDecomposition.tex}
\input{slides/FMM/FMMInSieve.tex}
%%\input{slides/FMM/TreeImplementation.tex}
%%\input{slides/FMM/TreeInterface.tex}
%
\subsection{Data Decomposition}
\input{slides/FMM/Sections.tex}
%
\section{Serial Implementation}
%
\subsection{Control Flow}
\input{slides/FMM/ControlFlow.tex}
%
\subsection{Interface}
\input{slides/FMM/EvaluatorInterface.tex}
\input{slides/FMM/KernelInterface.tex}
%
%
\section{Multicore Interfaces}
%
\subsection{GPU Programming}
\input{slides/GPU/GPUvsCPU.tex}
\input{slides/Performance/Streams.tex}
\input{slides/Performance/SMVAnalysis.tex}
\input{slides/Performance/ImprovingSerialPerformance.tex}
\input{slides/GPU/GeneralProgramming.tex}
\input{slides/GPU/Modularity.tex}
\input{slides/GPU/Locality.tex}
%% Slide on explicit Work Queue interface
%
\subsection{FLASH}
\input{slides/Automation/FLASH_Design.tex}
\input{slides/Automation/FLASH_Cholesky.tex}
%
\subsection{PetFMM}
\input{slides/FMM/PetFMM_GPU.tex}
\input{slides/FMM/PetFMM_GPU_Classes.tex}
% TODO: Add in Reduce task to this slide
\input{slides/FMM/PetFMM_Tasks.tex}
\input{slides/FMM/PetFMM_Transform.tex}
\input{slides/FMM/PetFMM_Reduce.tex}
\input{slides/FMM/PetFMM_GPU_Timing.tex}
\input{slides/FMM/PetFMM_Timing.tex}
%
%
\section{Multicore Implementation}
%
\subsection{Complexity Analysis}
\input{slides/FMM/Complexity_GG.tex}
%%\input{slides/FMM/DistributedFMM.tex}
%
\subsection{Redesign}
\input{slides/FMM/ParticlesPerCell.tex}
\input{slides/FMM/PetFMM_Stages.tex}
\input{slides/FMM/MissingConcurrency.tex}
%% TODO: Slide with exact numbers from paper
\input{slides/FMM/MissingBandwidth.tex}
%
\subsection{MultiGPU}
%% Put in discussion of partitioning and distribution
\input{slides/FMM/ParallelTreeImplementation.tex}
\input{slides/FMM/ParallelTreeGraph.tex}
\input{slides/FMM/ParallelDecomposition.tex}
\input{slides/FMM/ParallelLoadBalance.tex}
\input{slides/FMM/ParallelDecompositionSpiral.tex}
%%\input{slides/FMM/ParallelTreeInterface.tex}
\input{slides/FMM/ParallelDataMovement.tex}
%%\input{slides/FMM/ParallelEvaluator.tex}
%
\begin{frame}{GPU Interaction}

  Since our parallelism is hierarchical
\begin{itemize}
  \item Local (serial) tree interface is preserved

  \bigskip

  \item GPU code can be reused locally \red{without change}

  \bigskip

  \item Multiple GPUs per node can also be used
\end{itemize}
\end{frame}
%
%
\section*{Conclusions}
\begin{frame}
\frametitle{What's Important?}
\begin{center}
    {\Large Interface improvements bring concrete benefits}
\end{center}

\begin{itemize}
  \item Facilitated code reuse
  \begin{itemize}
    \item Serial code was largely reused

    \item Test infrastructure completely reused
  \end{itemize}

  \bigskip

  \item Opportunites for performance improvement
  \begin{itemize}
    \item Overlapping computations

    \item Better task scheduling
  \end{itemize}

  \bigskip

  \item Expansion of capabilities
  \begin{itemize}
    \item Could now combine distributed and multicore implementations

    \item Could replace local expansions with cheaper alternatives
  \end{itemize}
\end{itemize}
\end{frame}

\end{document}
