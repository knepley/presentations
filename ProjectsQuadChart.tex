\documentclass{beamer}

\mode<presentation>
{
  %%\usetheme[right]{Hannover}
  %%\usetheme{Goettingen}
  %%\usetheme{Dresden}
  \usetheme{default}
  %\useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamercovered{transparent}
}
\beamertemplatenavigationsymbolsempty
\setbeamercolor{coloredboxstuff}{fg=white,bg=blue}
\setbeamertemplate{headline}
{%
\hbox{\includegraphics[height=0.35in]{figures/logos/UB_SEAS.pdf}
\begin{beamercolorbox}[ht=0.35in]{coloredboxstuff}\hfil{\large Matthew Knepley, CSE}\end{beamercolorbox}}%
}

\setbeamertemplate{footline}
{%
%\begin{beamercolorbox}{people in head/foot}
%\end{beamercolorbox}%
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc} % Note that the encoding and the font should match. If T1 does not look nice, try deleting this line
\usepackage{amsfonts, amsmath, subfigure, multirow}
\usepackage{hyperref}
\newlength\LL \settowidth\LL{1000}

%\usepackage{beamerseminar}
\usepackage{graphicx}
\usepackage{array}
\usepackage{ulem}
\usepackage{listings}
\usepackage{xfrac}
\usepackage{xspace}
\usepackage{colortbl}
\usepackage{alltt}

% Color macros
\newcommand{\black}{\textcolor{black}}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\green}{\textcolor{green}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\brown}{\textcolor{brown}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\yellow}{\textcolor{yellow}}

\begin{document}

\begin{frame}{Breaking the Curse of Dimensionality}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
  The so-called ``curse of dimensionality'' says that the computational work explodes exponentially as the
  dimensionality of the inputs grows. This has stymied simulation of high-dimensional physical systems, such as
  plasma physics, for decades. Recently, very high dimensional classification problems have been solved using
  neural networks, a particular form of nonlinear function representation. However, neural networks provide
  little insight into the solution itself. Moreover, forms such as the Kolmogorov representation have not been used
  successfully for computation due to the lack of smoothness in the basis.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
The Kolmogorov Superposition Theorem (KST) allows us to represent a continuous multi-dimensional function $f$ as
the composition of one-dimensional functions in the following form:
$$f(x_1,\ldots,x_n) = \sum^{2n+1}_{q=1} g_q\left(\sum^n_{p=1} \lambda_p \phi(x_p + q \epsilon) \right)$$
where the function $\phi$ is universal in that it does not depend on $f$.
%We have developed an optimal complexity, Lipschitz continuous representation for $\phi$, and are currently working to develop a computable representation for \(g_q\).
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
A computational understanding of the structure of continuous functions will give us a powerful tool to
understand the operation of neural networks, and other complex nonlinear classifiers. Our work should enable
the compression of the deep neural networks currently in use, and selection of optimal activiation
functions. This would have broad impact across the entire field of machine learning and data
assimilation. Moreover, it could be possible to solve high dimensional physical systems using one dimensional
tools as well.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
We have successfully tackled the initial question of a definition for $\phi$ based on a reparameterization
of the H\"older continuous form of K\"oppen. We are currently experimenting with both finite element
and Chebyshev polynomial representations for $g_q$. Since the KST form was shown to be a 3 layer,
feed-forward neural network by Hecht-Neilsen, we should be able to reformulate problems for which neural
networks are quite effective, such as classification, in terms of the KST representation.\end{block}
\end{minipage}
\end{frame}
%
\begin{frame}{Certification of Parallel Mesh Operations}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
Unstructured mesh operations, which underpin much of modern finite element modeling, are error-prone and often slow. These problems are exacerbated by the introduction of parallelism.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
We have developed a topological abstraction which can be reduced to operations on a certain Directed Acyclic Graph
(DAG). We have proved our main parallel algorithms correct informally. We will reexpress those proofs in the Coq logic
programming language in order to generate machine-checkable proofs. In addition, we will explore automatic generation of
the implementation code itself, which is a much thornier problem.
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
Automatic certification of parallel mesh algorithms would enable {\bf more sophisticated} mesh manipulation on more
complex meshes, such as those used in fluid-structure interaction for additive manufacturing. It would allow much
{\bf faster and cheaper} code development. It would also open the door to the use of unstructured meshes in
{\bf critical systems}.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
We would like to formalize the poset (Hasse diagram) representing meshes in Coq. This should allow us to prove correct the parallel algorithms for distribution and overlap generation (and all other basic operations). It might also let us reason about the complexity.
\end{block}
\end{minipage}
\end{frame}
%
\begin{frame}{Parallel Unstructured Mesh Adaptivity}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
Although unstructured meshes are indispensible technology for both industrial and academic simulation of engineered systems based on continuum mechanics, no free, community software framework exists for this technology. Moreover, parallel adaptation which scales to the largest machines available remains out of reach.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
We have developed a topological abstraction from computational meshes which reduces them to certain Directed Acyclic Graphs (DAG). Rephrasing mesh operations as graph operations allows us to write code which is independent of the mesh dimension, cell shape, approximation order, and equation. This makes the code easier to optimize, portable and maintainable, and far more flexible than previous libraries. We are using this framework to add support for parallel adaptivity, leveraging the Pragmatic library from Imperial College London.
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
Automatic mesh adaptivity for large, parallel finite element models would open the door to predictive simulations for complex, real-world engineering systems. In particular, we are applying this technology to additive manufacturing systems, as well as wave turbines for renewable energy generation. Scientifically, it enables simulations with unprecedented accuracy by individual investigators. We have examined the impact of earthquakes in California and New Zealand, and will simulate quantum field theories in curved spacetime necessary to interpret results from the CERN supercollider.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
We have already added parallel unstructured mesh support to the PETSc libraries, supporting parallel loading of many mesh formats, parallel mesh redistribution and load balancing, and assembly of complex multiscale, multiphysics problems on the mesh. We have produced proof-of-concept adaptivity interfaces, which we will subsequently scale up to leadership-class supercomputers for our target applications.
\end{block}
\end{minipage}
\end{frame}
%
\begin{frame}{Theory of Robust Solution for Nonlinear Problems}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
Discretization of continuum physics problems, and related optimization problems, generates large, sparse nonlinear systems of algebraic equations. The solution of these systems can be very challenging, and few rules-of-thumb exist for designing robust solvers, in sharp distinction to the linear case.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
The theory of non-discrete induction (NDI) offers the possibility of predicting short-time (non-asymptotic) convergence behavior for complex, multi-solver nonlinear iterations. We will apply this theory to the composition of nonlinear solvers, termed nonlinear preconditioning in our SIAM Review article, and create simple rules for assembling robust nonlinear solvers by composition, in analogy with the quite successful practice in the linear case.
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
An effective theory for composition of nonlinear solvers would be a powerful step toward automation of predictive modeling and simulation in modern engineering. Integrated into modeling platforms such as Fluent and ABAQUS, in which PETSc already resides, it could automatically solve models far in the nonlinear regime, currently inaccessible to users, which would be a necessity for future systems such as fusion power.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
In the PETSc libraries, we have already implemented a flexible system allowing composition of nonlinear iterations, enabling easy construction of optimal multilevel solvers for real-world physical systems. We have preliminary results using NDI to predict convergence rates, verified in one dimension. We will extend our theory to Banach spaces, and obtain empirical results from simple multiphysics systems, such as Stokes flow and viscoplastic extensions.
\end{block}
\end{minipage}
\end{frame}
%
\begin{frame}{Scalable Modeling of Mantle Dynamics}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
Mantle dynamics is the only theory that can elucidate the earliest origins of our planet, describing the motion of tectonic plates, origin of mountain belts and rift zones, and spectacular physics at subduction zones, such as great earthquakes and volcanism. High resolution simulations combined with the explosion of observational data is creating an enormous opportunity for scientific advancement.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
Many papers from the late 1990s and early 2000s show that there exist efficient multigrid smoothers for the Stokes equation. However, these have fallen out of favor, I think largely due to insufficient software support in that linear algebra packages do not integrate topological information. In addition, there has been much more work on block Schur complement methods for nonlinear Stokes problems. We will test Vanka and Braess-Sarazin smoothers against Schur complement methods for the variable-viscosity Stokes problem arising from mantle dynamics.
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
We are seeking to create a large, scale thermal and stress map of the Earth's mantle, which has implications for geochemistry, volcanology, petrology, and economic geophysics. The coupling of regional and global scales, through inverse problems and sensitivity analysis, will enable scientists to look precisely at places on earth, rather than cartoons which lack the geometric and physical detail to trace develop over geologic time.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
We have already constructed the infrastructure for linear Vanka and Braess-Sarazin preconditioners for the Stoeks equations. Going forward, we will scale these implementations to very large problems, as well as extend the implementations to nonlinear problems.
\end{block}
\end{minipage}
\end{frame}
%
\begin{frame}{Incorporation of Poroelasticity into Crustal Dynamics}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
Simulation of earthquake rupture and the consequent post-seismic deformation are crucially important for risk management in the affected areas, as well as scientific understanding of fault triggering and plastic deformation mechanisms. It has been widely conjectured that subsurface fluids play an important role in both processes, but this has never been verified by either experiment or simulation.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
Using the PETSc Plex mesh and function space abstraction, we can assemble multiphysics systems for domains of any dimension, with any cell shape, and any finite element basis. We are now incorporating the faulting mechanism as a lower-dimensional manifold in the assembly process, in order to assemble and solve the fully coupled system.
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
The ability to quantify the effects of subsurface fluids on fault behavior, through a poroelastic model, would have critical implications for seismic hazard models, which in turn by law determine insurance rates in high risk areas. Nearly 3B USD is underwritten each year in the United States alone, measured by direct premiums (see \href{https://www.statista.com/statistics/186506/top-earthquake-insurance-writers-in-the-us-by-market-share/}{here}).
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
We have already produced homogeneous multiphysics systems in PyLith, the crustal physics simulator sponsored by the NSF and USGS. We are now working to incorporate fault behavior in the multiphysics context.
\end{block}
\end{minipage}
\end{frame}
%
\begin{frame}{Accurate Molecular Solvation Models}
\vskip-3ex
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Problem}\tiny
Implicit solvent models for molecules dissolved in fluid offer a solution to the enormous computational demands of molecular dynamics (MD) simulation. However, the dominant Poisson model currently requires tuning of hundreds of parameters, which can consume months and even years of researcher effort, and has cannot reproduce cruical phenomena such as charge-hydration asymmetry and entropies of solution.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Approach}\tiny
We have developed a simple 4 parameter implicit solvent model based upon a nonlinear boundary integral formulation of the fundamental physics. Our model correctly predicts charge-hydration asymmetry and energies of mixtures, as well as thermodynamic measures, such as entropy, that traditional Poisson models are incapable of reproducing.
\end{block}
\end{minipage}

\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Impact}\tiny
An accurate, robust, therodynamically viable implicit solvent model could revolutionize computational drug design, currently at the mercy of the extreme computational and interpretive demands of MD. Moreover, our model is compatible with the workhorse of computational chemistry, the Polarizible Comtinuum Model (PCM), which possesses the same model shortcomings as the Poisson model.
\end{block}
\end{minipage}
\hfil
\begin{minipage}[t]{0.45\textwidth}
\begin{block}{Plan}\tiny
We have developed a proof-of-concept code, and published numerous stuides indicating the superior accuracy and efficiency of this method. We are currently working to integrate our model into large scale community code in computational chemistry, such as Psi4 and NWChem.
\end{block}
\end{minipage}
\end{frame}

\end{document}
