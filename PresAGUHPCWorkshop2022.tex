\documentclass[dvipsnames]{beamer}

\input{talkPreambleNoBeamer.tex}
\mode<presentation>
{
  \usetheme{Warsaw}
  \useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}
\addbibresource{presentations.bib}

\title[SCIWS26]{Porting Machine Learning and Modeling\\from a Laptop to a Supercomputer:\\Numerical Libraries}
\author[M.~Knepley]{\LARGE Matthew~Knepley}
\date[]{}
% - Use the \inst command if there are several affiliations
%\institute[Buffalo]{
%  Computer Science and Engineering\\
%  University at Buffalo
%}
\subject{PETSc}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}
%
\begin{frame}\Huge
  \blue{P}ortable\\
  \hskip2em\blue{E}xtensible\\
  \hskip4em\blue{T}oolkit for\\
  \hskip6em\blue{S}cientific\\
  \hskip8em\blue{c}omputing

\bigskip

\begin{center}
  \blue{\href{https://petsc.org/}{https://petsc.org}}
\end{center}
\end{frame}
%
\begin{frame}{Earth Science with PETSc}\LARGE
\begin{itemize}
  \item \blue{\href{http://www.geodynamics.org/cig/software/pylith}{PyLith}} (CIG)

  \item \blue{\href{http://www.underworldproject.org/}{Underworld}} (Monash)

  \item \blue{\href{https://salvus.io/}{Salvus}} (ETHZ)

  \item \blue{\href{http://terraferma.github.io/}{TerraFERMA}} (Columbia)

  \item \blue{\href{http://ees.lanl.gov/pflotran/}{PFLOTRAN}} (DOE)

  \item \blue{\href{http://stomp.pnnl.gov/}{STOMP}} (DOE)

  \item \blue{\href{http://}{pTatin3d}} (UCSD)

  \item \blue{\href{http://}{Rhea}} (NYU)

  \item \blue{\href{http://}{Waiwera}} (U Auckland)
\end{itemize}
\end{frame}
%
\begin{frame}{Sparse, Parallel Linear Algebra}\Large

\begin{itemize}
  \item Vec/Mat bound to a compute resource at runtime\\
    \textit{``Toward Performance-Portable PETSc for GPU-based Exascale Systems''}\\
    \blue{\href{https://arxiv.org/abs/2011.00715}{arXiv:2011.00715}}

  \bigskip
  \bigskip

  \item Communication happens directly between GPUs\\
    \textit{``The {PetscSF} Scalable Communication Layer''}\\
    \blue{\href{https://arxiv.org/abs/2102.13018}{arXiv:2102.13018}}
\end{itemize}

\end{frame}
%
\begin{frame}{Implicitness}\Large

\begin{itemize}
  % Seismic example: Want higher resolution at the surface
  %                  Uniform radial grid shrinks cells at base
  %                  Waves are 10x faster at the base of the mantle
  \item Allow accuracy limits to supercede stability
  \medskip
  \begin{itemize}\large
    \item CFL limits
    \medskip
    \item Geometric limits
  \end{itemize}

  \bigskip

  \item Scale separation
  \medskip
  \begin{itemize}\large
    \item Parabolization % Gravity waves
    \medskip
    \item Localization % Faults
  \end{itemize}

  \bigskip

  \item Inverse problems
  \medskip
  \begin{itemize}\large
    \item Bayesian inference
    \medskip
    \item Parameter estimation
  \end{itemize}
\end{itemize}

\end{frame}
%
\begin{frame}{Composable Solvers}{Linear}

\includegraphics[width=\textwidth]{figures/Composable/ComposedLinearSolver_FarrellMitchellWechsung.pdf}
\begin{center}
  Farrell, Mitchell, and Wechsung (2018), \blue{\href{https://arxiv.org/abs/1810.03315}{arXiv:1810.03315}}
\end{center}
\end{frame}
%
\input{slides/SNES/SNESEx19NewtonNRich.tex}
%
\input{slides/Plex/Adaptation/p4estOverview.tex}
%
\input{slides/Plex/Adaptation/parmmgOverview.tex}
%
\begin{frame}{Differentiability}\Large

\begin{itemize}
  \item Low-level AD
  \medskip
  \begin{itemize}\large
    \item PyTorch, Tensorflow
    \medskip
    \item Enzyme\\
    ``Performance-Portable Solid Mechanics via Matrix-Free $p$-Multigrid''\\
    \blue{\href{https://arxiv.org/abs/2204.01722}{arXiv:2204.01722}}
  \end{itemize}

  \bigskip

  \item High-level AD
  \medskip
  \begin{itemize}\large
    \item pyadjoint (Firedrake, FEniCS) \\
    \blue{\href{https://github.com/dolfin-adjoint/pyadjoint}{github:dolfin-adjoint/pyadjoint}}
    \medskip
    \item libadjoint\\
    \blue{\href{https://bitbucket.org/dolfin-adjoint/libadjoint/src/master/}{bitbucket:dolfin-adjoint/libadjoint}}
  \end{itemize}
\end{itemize}

\end{frame}
%
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Hardware Details

\only<2->{\begin{textblock}{0.25}[0.0,0.0](0.05,0.50)\includegraphics[width=\textwidth]{figures/Hardware/MacAir.jpg}\end{textblock}}
\only<3->{\begin{textblock}{0.25}[0.0,0.0](0.30,0.60)\includegraphics[width=\textwidth]{figures/Hardware/NvidiaC2070.jpeg}\end{textblock}}
\only<4>{\begin{textblock}{0.60}[0.0,0.0](0.58,0.55)\includegraphics[width=\textwidth]{figures/Hardware/ALCF-Theta_111016-1000px-225x153.jpg}\end{textblock}}

\end{frame}
% SPEAK: Matrix and Krylov classes hide architectural optimizations
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Implementation Complexity

\only<2->{\begin{textblock}{0.40}[0.0,0.0](0.05,0.45)\includegraphics[width=\textwidth]{figures/FEM/GPU/threadTranspose2.png}\end{textblock}}
\only<3->{\begin{textblock}{0.35}[0.0,0.0](0.80,0.45)\includegraphics[width=\textwidth]{figures/Mat/BLISLoops.pdf}\end{textblock}}
\only<4>{\begin{textblock}{0.85}[0.0,0.0](0.20,0.65)
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L0_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L1_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L2_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L3_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L4_EV1.png}\end{textblock}}

\end{frame}
% SPEAK: Users implementing their own algorithms will not typically be aware of implementation nuances arrived at through decades of experience.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Accumulate\\
\hskip1em Best Practices

\bigskip

\begin{itemize}\Large
  \item<2->[] Classical Gram-Schmidt orthogonalization with selective reorthogonalization
  \medskip
  \item<3->[] Eigen-estimation for AMG with Krylov bootstrap
\end{itemize}

\medskip

\uncover<4>{\begin{center}\Large Improvement without code changes\end{center}}
\end{frame}
% SPEAK: Libraries greatly simplify the process of building, maintaining, and extending a simulation code. A coherent V\&V strategy demands that we can completely reconstruct the exact build for any computation.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Simplfy\\
\hskip1em Determining Provenance

\bigskip
\bigskip

\Large
\begin{overprint}
\onslide<2>
Rather than making build-time choices,
\onslide<3>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels
\onslide<4>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels\\
\hspace*{2em} with another layer of workflow scripts\\
\onslide<5>
Rather than making build-time choices,\\
\hspace*{1em} that must be plumbed through all levels\\
\hspace*{2em} with another layer of workflow scripts\\
\hspace*{2em} and brittle top-level interfaces,
\onslide<6>\Large
we use packages without modification,
\onslide<7>\Large
we use packages without modification,\\
\hspace*{1em} compiled in a standard way
\onslide<8>\Large
we use packages without modification,\\
\hspace*{1em} compiled in a standard way\\
\hspace*{2em} and controlled entirely via runtime options.
\end{overprint}
\end{frame}
%
% SPEAK: Early Numerical Libraries
%    71 Handbook for Automatic Computation: Linear Algebra, J. H. Wilkinson and C. Reinch
%    73 EISPACK, Brian Smith et.al.
%    79 BLAS, Lawson, Hanson, Kincaid and Krogh
%    90 LAPACK, many contributors 91 PETSc, Gropp and Smith 95 MPICH, Gropp and Lusk
%       All of these packages had their genesis at Argonne National Laboratory/MCS
\begin{frame}{Why PETSc?}
\Large
\setbeamercovered{transparent}

\begin{itemize}
  \item<1-> Dependability \& Maintainability
  \visible<2>{\begin{itemize}
    \item Nearly 30 years of continuous development
  \end{itemize}}
  \item<3-> Portability \& Robustness
  \visible<4>{\begin{itemize}
    \item Tested at every supercomputer installation and 10,000+ users
  \end{itemize}}
  \item<5-> Performance \& Scalability
  \visible<6>{\begin{itemize}
    \item Many Gordon Bell Winners
  \end{itemize}}
  \item<7-> Optimality \& Robustness
  \visible<8>{\begin{itemize}
    \item State-of-the-Art Linear and Nonlinear Solvers, Eigensolvers, Optimization Solvers, Timesteppers
  \end{itemize}}
  \item<9-> Flexibility \& Extensibility
  \visible<10>{\begin{itemize}
    \item More than 8000 citations and hundreds of application packages
    \item Aerodynamics, Arterial Flow, Corrosion, Combustion, Data Mining, Earthquake Mechanics, \ldots
  \end{itemize}}
\end{itemize}
\end{frame}
\end{document}

Not going to do an overview of PETSc, rather I am going to try to connect PETSc features with things that people need when scaling up simulations and trying to interpret results.

[1] @article{MILLS2021,
title = {Toward performance-portable {PETS}c for {GPU}-based exascale systems},
journal = {Parallel Computing},
volume = {108},
pages = {102831},
year = {2021},
issn = {0167-8191},
doi = {https://doi.org/10.1016/j.parco.2021.102831},
url = {https://www.sciencedirect.com/science/article/pii/S016781912100079X},
author = {Richard Tran Mills and Mark F. Adams and Satish Balay and Jed Brown and Alp Dener and Matthew Knepley and Scott E. Kruger and Hannah Morgan and Todd Munson and Karl Rupp and Barry F. Smith and Stefano Zampini and Hong Zhang and Junchao Zhang},
}

[2] @article{PetscSF2022,
  author={Zhang, Junchao and Brown, Jed and Balay, Satish and Faibussowitsch, Jacob and Knepley, Matthew and Marin, Oana and Mills, Richard Tran and Munson, Todd and Smith, Barry F. and Zampini, Stefano},
  journal={IEEE Transactions on Parallel and Distributed Systems},
  title={The {PetscSF} Scalable Communication Layer},
  year={2022},
  volume={33},
  number={4},
  pages={842-853},
  doi={10.1109/TPDS.2021.3084070}
}
