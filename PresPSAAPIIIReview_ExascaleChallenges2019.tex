\documentclass[dvipsnames]{beamer}

\input{talkPreambleNoBeamer.tex}
\mode<presentation>
{
  \usetheme{Warsaw}
  \useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}
\addbibresource{presentations.bib}

\title[CHREST]{CHREST: Exascale Challenges}
\author[M.~Knepley]{\LARGE Matthew~Knepley}
\date[]{}
% - Use the \inst command if there are several affiliations
%\institute[Buffalo]{
%  Computer Science and Engineering\\
%  University at Buffalo
%}
\subject{Rockets}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \vspace*{-2em}
  \begin{center}\includegraphics[width=0.8\textwidth]{figures/logos/CHRESTLogo.png}\end{center}
\end{frame}
%
%\section{Focused Technical Discussion: Exascale Challenges}
% 20 min
%
% What specific exascale challenges will you tackle, and what are your strategies?
% SPEAK: Our goal is to have optimal, scalable solvers for each phase of the analysis pipeline. To go from the laboratory (2nd slide) to space (3rd slide) entirely computationally.
\begin{frame}[t]{Exascale Challenges}\Huge

The main challenge is to run\\
\hskip1em the entire analysis pipeline\\
\hskip2em at scale.

\only<2->{\begin{textblock}{0.6}[0.0,0.0](0.05,0.60)\includegraphics[width=\textwidth]{figures/Rocket/LabBurn.pdf}\end{textblock}}
\only<3->{\begin{textblock}{0.8}[0.0,0.0](0.35,0.55)\includegraphics[width=\textwidth]{figures/Rocket/NASA_Peregrine.png}\end{textblock}}
\end{frame}
% SPEAK:
\begin{frame}[t]{Exascale Challenges}\Huge

Heterogeneous architectures\\
\hskip1em complicate\\
\hskip2em performance tradeoffs.

\end{frame}
\begin{frame}[t]{Exascale Challenges (Figure J. Brown, 2019)}
\includegraphics[width=\textwidth]{figures/Performance/CompareThroughputAndLatency.pdf}\\
\end{frame}
% SPEAK: In order to employ heterogeneous architecture efficiently for complex computation, we must make use of lower level objects are many different places in a high level computation, which different performance tradeoffs and accuracy requirements. For example, computations which need a rapid turnaround time, such as column solves, or ROM runs for Monte-Carlo evaluation, might be migrated to the CPU.
\begin{frame}[t]{Exascale Challenges}\Huge

Heterogeneous architectures\\
\hskip1em complicate\\
\hskip2em performance tradeoffs.
\pause

\bigskip

\Large
\begin{overprint}
\onslide<2>
Match computations\\
\hspace*{1em}  to the correct resource\\
\onslide<3>
Leverage lower level pieces
\onslide<4>
Leverage lower level pieces\\
\hspace*{1em} in many different places
\onslide<5>
Leverage lower level pieces\\
\hspace*{1em} in many different places\\
\hspace*{2em} in higher level computations.
\end{overprint}
\begin{textblock}{0.4}[0.0,0.0](0.8,0.70)\includegraphics[width=\textwidth]{figures/Performance/CompareThroughputAndLatency.pdf}\end{textblock}
\end{frame}
%
% SPEAK: This is where interface composability comes in. Composability is perhaps the most important benefit of strong library interfaces.
\begin{frame}[t]{Why Composability?}\Huge

Composability enables\\
\hskip1em Runtime Configuration
\pause

\bigskip

\Large
The optimal solver for a given problem depend on\\
the equation,\\
\pause
\hskip1em parameter choice,\\
\pause
\hskip2em solution regime,\\
\pause
\hskip3em discretization,\\
\pause
\hskip4em mesh,\\
\pause
\hskip5em desired accuracy,\\
\pause
\hskip6em etc.
\end{frame}
% SPEAK:
\begin{frame}[t]{Why Composability?}\Huge

Composability enables\\
\hskip1em Advanced Analysis
\pause

\bigskip

\Large
Each simulation feeds the next level of analysis,\\
\pause
\hskip1em so the top-level application is ephemeral,\\
\pause
\hskip2em and each level must compose with the next.\\
\pause
\hskip3em Linear solvers feed nonlinear solvers,\\
\pause
\hskip4em which feed Timesteppers,\\
\pause
\hskip5em which feed Eigensolvers\\
\hskip5em and Optimization solvers,\\
\pause
\hskip6em etc.
\end{frame}
% SPEAK: The forward solver must be reusable inside the optimization loop, or the sensitivity analysis, or the adjoint solve. The optimizer must be reusable in the design loop. Only by having strong interface at all levels can be hope to interrogate exascale data.
\begin{frame}[t]{Why Composability?}\Huge

Composability enables\\
\hskip1em Analysis on Big Data
\pause

\bigskip
\bigskip

\Large
\begin{overprint}
\onslide<2>
It takes about one hour to read or write
\onslide<3>
It takes about one hour to read or write\\
\hspace*{1em} the contents of volatile memory to global storage
\onslide<4>
It takes about one hour to read or write\\
\hspace*{1em} the contents of volatile memory to global storage\\
\hspace*{2em} on today's top machines,\\
\onslide<5>
It takes about one hour to read or write\\
\hspace*{1em} the contents of volatile memory to global storage\\
\hspace*{2em} on today's top machines,\\
\hspace*{3em} assuming peak I/O bandwidth is reached.
\onslide<6>
Thus, it is crucial
\onslide<7>
Thus, it is crucial\\
\hspace*{1em} that as much analysis as possible\\
\onslide<8>
Thus, it is crucial\\
\hspace*{1em} that as much analysis as possible\\
\hspace*{2em} be brought to bear while data is in memory.
\end{overprint}
\end{frame}
%
% SPEAK:
\begin{frame}[t]{Exascale Challenges}\Huge


The quality of system tools\\
\hskip1em and vendor libraries\\
\hskip2em need to improve.

\bigskip

\Large
\begin{itemize}
  \item CI and reliable builds
  \medskip
  \item GPU-aware MPI
  \medskip
  \item Better latency measurement
  \medskip
  \item More stable libraries
\end{itemize}
\end{frame}
\end{document}
