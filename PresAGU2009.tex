\documentclass{beamer}

\mode<presentation>
{
  %%\usetheme[right]{Hannover}
  %%\usetheme{Goettingen}
  %%\usetheme{Dresden}
  \usetheme{Warsaw}
  \useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamercovered{transparent}
}

\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage{times}
\usepackage[T1]{fontenc} % Note that the encoding and the font should match. If T1 does not look nice, try deleting this line
\usepackage{amsfonts, amsmath, subfigure, multirow}
\usepackage{hyperref}
\newlength\LL \settowidth\LL{1000}

%\usepackage{beamerseminar}
\usepackage{graphicx}

\usepackage{array}
\usepackage{tikz}
\usetikzlibrary{backgrounds}
\usetikzlibrary{snakes}

\newcommand{\uKern}{\mathbb{K}}

\newcommand{\D}{\mathcal{D}}\newcommand{\E}{\mathcal{E}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\I}{\mathcal{I}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\bigO}{\mathcal{O}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\kb}{\tt}
\newcommand{\blue}{\textcolor{blue}}
\newcommand{\green}{\textcolor{green}}
\newcommand{\red}{\textcolor{red}}
\newcommand{\brown}{\textcolor{brown}}
\newcommand{\cyan}{\textcolor{cyan}}
\newcommand{\magenta}{\textcolor{magenta}}
\newcommand{\yellow}{\textcolor{yellow}}
\newcommand{\mini}{\mathop{\rm minimize}}
\newcommand{\st}{\mbox{subject to }}
\newcommand{\lap}[1]{\Delta #1}
\newcommand{\grad}[1]{\nabla #1}
\renewcommand{\div}[1]{\nabla \cdot #1}
\newcommand{\vx}{{\bf x}}
\newcommand{\vy}{{\bf y}}
\def\code#1{{\tt #1}}
\def\Update#1{\frametitle{Code Update} \begin{center}\Huge Update to {Revision \green #1}\end{center}}

\title[GPU]{Stokes Preconditioning on a GPU}
\author[M.~Knepley]{Matthew~Knepley\inst1${}^,$\inst2, Dave A. Yuen, and Dave A. May}
\date[AGU09]{AGU '09\\San Francisco, CA\qquad December 15, 2009}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  \inst1
  Computation Institute\\
  University of Chicago

  \smallskip

  \inst2
  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}

\subject{GPU Computation}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>
\frametitle{abstract}

Title: GPU Implementation for High-Resolution 2-D Variable Viscosity Stokes Flow

Recently the mounting importance and advantage of GPU processing, especially for scientific applications, has become
clear. However, solvers for elliptic equations have been slow to utilize this hardware due to the bandwidth limited
nature of most implementations, such as those based upon sparse matrices and multigrid. The Stokes problem for mantle
convection has this elliptic character, and prior solvers have used large sparse matrices, see (May and Moresi 2008).
It is well known that variable viscosity exerts a dominant influence in mantle convection due to the rock rheology
(Ranalli 1995, Karato 2008). One advantage of the Fast Multipole Method (FMM) is the multiscale nature of the
 algorithm, leading to a solution time directly proportional to the number of unknowns, or $\mathcal{O}(N)$, and also FMM is
a direct method which does not require iteration. Building on recent work for the FMM on GPU (Cruz, Knepley and Barba
2009), we will develop both a high performance isoviscous Stokes and Laplacian solver using a Gaussian Radial Basis
Function (RBF) discretization, Green's function formulation, and FMM solution on a GPU. The Laplacian solver can be used
to build a variable viscosity Stokes preconditioner following May and Moresi. Specifically, we precondition Stokes with
a block triangular preconditioner, the upper diagonal being the Laplacian on the velocity space, and the lower diagonal
being the BFBT preconditioner for the Schur complement, which utilizes two Laplacian solves on the pressure space. The
entire variable viscosity Stokes problem will be solved with a preconditioned Krylov iterative method, executed in
double precision on the CPU. Since only the preconditioner is evaluated in single precision on the GPU, we do not
sacrifice accuracy. We will demonstrate solutions in two dimensions with more than 4 million RBF particles, occupying
only 250MB.

\end{frame}
%
\begin{frame}{Collaborators}

\begin{itemize}
  \item \magenta{\href{http://www.geo.umn.edu/people/profs/YUEN.html}{Prof. Dave Yuen}}
  \begin{itemize}
    \item Dept. of Geology and Geophysics, University of Minnesota
  \end{itemize}

  \medskip

  \item \magenta{\href{http://jupiter.ethz.ch/~dmay}{Dr. David May}}, developer of BFBT (in PETSc)
  \begin{itemize}
    \item Dept. of Earth Sciences, ETHZ
  \end{itemize}

  \medskip

  \item Felipe Cruz, developer of FMM-GPU
  \begin{itemize}
    \item Dept. of Applied Mathematics, University of Bristol
  \end{itemize}

  \item \magenta{\href{http://barbagroup.bu.edu/Barba_group/Home.html}{Prof. Lorena Barba}}
  \begin{itemize}
    \item Dept. of Mechanical Engineering, Boston University
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section{5 Slide Talk}
%
\begin{frame}{BFBT}

\Large BFBT preconditions the Schur complement using

\bigskip
\begin{center}
\begin{equation}
  S^{-1}_b = L^{-1}_p G^T K G L^{-1}_p
\end{equation}
\end{center}
where $L_p$ is the Laplacian in the pressure space.

\end{frame}
%
\begin{frame}{Current Problems}

\Large The current BFBT code is limited by

\bigskip

\begin{itemize}
  \item Bandwidth constraints
  \begin{itemize}
    \item Sparse matrix-vector product

    \item Achieves at most 10\% of peak performance

  \end{itemize}

  \bigskip

  \item Synchronization
  \begin{itemize}
    \item GMRES orthogonalization

    \item Coarse problem
  \end{itemize}

  \bigskip

  \item Convergence
  \begin{itemize}
    \item Viscosity variation

    \item Mesh dependence
  \end{itemize}
\end{itemize}
\end{frame}
%
\input{slides/BEM/BFBTAcceleration.tex}
%
\begin{frame}{Missing Pieces}

\Large
\begin{itemize}
  \item BEM discretization and assembly
  \begin{itemize}
    \item Matrix-free operator application using the \blue{F}ast \blue{M}ultipole \blue{M}ethod

    \item Overcomes bandwidth limit, \red{480 GF} on an NVIDIA 1060C GPU

    \item Overcomes coarse bottleneck by overlapping direct work
  \end{itemize}

  \bigskip

  \item Solver for BEM system
  \begin{itemize}
    \item Same total work as FEM due to well-conditioned operator

    \item Possibility of multilevel preconditioner (even better)
  \end{itemize}

  \bigskip

  \item Interpolation between FEM and BEM
  \begin{itemize}
    \item Boundary interpolation just averages

    \item Can again use FMM for interior
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Direct Fast Method for Variable-Viscosity Stokes}

\begin{itemize}
  \item<1-> Complexity not currently precisely quantified
  \begin{itemize}
    \item We would like a given number of flops/digit of accuracy
  \end{itemize}

  \bigskip

  \item<2-> Brute Force
  \begin{itemize}
    \item Use BEM to compute layers between regions of constant viscosity

    \item Better conditioned, but not direct
  \end{itemize}

  \bigskip

  \item<3-> Elegant method should be possible
  \begin{itemize}
    \item The operator is pseudo-differential

    \item ``Kernel-independent'' FMM exists
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section{What are the Problems?}
%
\begin{frame}{Problems}

\Large The current BFBT code is limited by

\bigskip

\begin{itemize}
  \item Bandwidth constraints

  \bigskip

  \item Synchronization

  \bigskip

  \item Convergence
\end{itemize}
\end{frame}
%
\subsection{Bandwidth}
\begin{frame}{Bandwidth}
  %% Homer to Brain: ``Explain how?'' BoyScoutz N the Hood

  Small bandwidth to main memory can limit performance

  \medskip

\begin{itemize}
  \item Sparse matrix-vector product

  \medskip

  \item Operator application

  \medskip

  \item AMG restriction and interpolation
\end{itemize}
\end{frame}
%
\input{slides/Performance/Streams.tex}
\input{slides/Performance/SMVAnalysis.tex}
\input{slides/Performance/ImprovingSerialPerformance.tex}
%
\subsection{Synchronization}
\begin{frame}{Synchronization}

  Synchronization penalties can come from

  \medskip

\begin{itemize}
  \item Reductions
  \begin{itemize}
    \item GMRES orthogonalization

    \item More than 20\% penalty for PFLOTRAN on Cray XT5
  \end{itemize}

  \medskip

  \item Small subproblems
  \begin{itemize}
    \item Multigrid coarse problem

    \item Lower levels of Fast Multipole Method tree
  \end{itemize}
\end{itemize}
\end{frame}
%
\subsection{Convergence}
\begin{frame}{Convergence}

Convergence of the BFBT solve depends on
\begin{itemize}
  \item Viscosity constrast (slightly)

  \item Viscosity topology

  \item Mesh
\end{itemize}

\bigskip

Convergence of the AMG Poisson solve depends on
\begin{itemize}
  \item Mesh
\end{itemize}
\end{frame}
%
%
\section{Can we do Better?}
\input{slides/BEM/BFBTAcceleration.tex}
%
\begin{frame}{Missing Pieces}

\Large
\begin{itemize}
  \item BEM discretization and assembly

  \bigskip

  \item Solver for BEM system

  \bigskip

  \item Interpolation between FEM and BEM
\end{itemize}
\end{frame}
%
\subsection{BEM Formulation}
\begin{frame}{Boundary Element Method}

\only<1>{
  The Poisson problem
\begin{center}
\begin{eqnarray}
  \Delta u(\vx) &=& f(\vx) \qquad \mathrm{on}\ \Omega \\
  u(\vx) \mid_{\partial\Omega} &=& g(\vx)
\end{eqnarray}
\end{center}
}
\only<2>{
  The Poisson problem (\blue{B}oundary \blue{I}ntegral \blue{E}quation formulation)
\begin{center}
\begin{eqnarray}
  C(\vx) u(\vx) &=& \int_{\partial\Omega} F(\vx,\vy) g(\vy) - G(\vx,\vy) \frac{\partial u(\vy)}{\partial n} dS(\vy) \\
     G(\vx,\vy) &=& -\frac{1}{2\pi} \log r \\
     F(\vx,\vy) &=& \frac{1}{2\pi r} \frac{\partial r}{\partial n}
\end{eqnarray}
\end{center}
}
\only<3>{
  Restricting to the boundary, we see that
\begin{center}
\begin{equation}
  \frac{1}{2} g(\vx) = \int_{\partial\Omega} F(\vx,\vy) g(\vy) - G(\vx,\vy) \frac{\partial u(\vy)}{\partial n} dS(\vy)
\end{equation}
\end{center}
}
\only<4>{
  Discretizing, we have
\begin{center}
\begin{equation}
  -G q = \left(\frac{1}{2} I - F\right) g
\end{equation}
\end{center}
}
\only<5>{
  Now we can evaluate $u$ in the interior
\begin{center}
\begin{equation}
  u(\vx) = \int_{\partial\Omega} F(\vx,\vy) g(\vy) - G(\vx,\vy) \frac{\partial u(\vy)}{\partial n} dS(\vy)
\end{equation}
\end{center}
}
\only<6>{
  Or in discrete form
\begin{center}
\begin{equation}
  u = F g - G q
\end{equation}
\end{center}
}
\only<7>{
  The sources in the interior may be added in using superposition
\begin{center}
\begin{eqnarray}
  \frac{1}{2} g(\vx) = \int_{\partial\Omega} F(\vx,\vy) g(\vy) - G(\vx,\vy) \left(\frac{\partial u(\vy)}{\partial n} - f\right) dS(\vy)
\end{eqnarray}
\end{center}
}
\end{frame}
%
\subsection{BEM Solver}
\begin{frame}{BEM Solver}

  The solve has two pieces:

\medskip

\begin{itemize}
  \item Operator application
  \begin{itemize}
    \item Boundary solve

    \item Interior evaluation

    \item Accomplished using the Fast Multipole Method
  \end{itemize}

  \bigskip

  \item Iterative solver
  \begin{itemize}
    \item Usually GMRES

    \item We use PETSc
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Operator Application}

  Using the \blue{F}ast \blue{M}ultiple \blue{M}ethod,\\
the Green's functions ($F$ and $G$) can be applied:
\begin{itemize}
  \item in $\mathcal{O}(N)$ time

  \item using small memory bandwidth

  \item in the interior and on the boundary

  \item with much higher serial and parallel performance
\end{itemize}
\end{frame}
%
\input{slides/FMM/Basics.tex}
\input{slides/FMM/PetFMM_Performance.tex}
\input{slides/FMM/ParallelLoadBalance.tex}
\input{slides/FMM/PetFMM_GPU_Timing.tex}
\input{slides/FMM/PetFMM.tex}
%
\begin{frame}{Convergence}

  BEM Laplace operator is well-conditioned
\begin{itemize}
  \item $\kappa = \mathcal{O}(N_B) = \mathcal{O}(\sqrt{N})$
  \begin{itemize}
    \item \magenta{\href{http://alexandria.tue.nl/repository/books/610540.pdf}{Dijkstra and Mattheij}}
  \end{itemize}

  \item Thus the total work is in $\mathcal{O}(N^2_B) = \mathcal{O}(N)$
  \begin{itemize}
    \item Same as MG
  \end{itemize}

  \item Regular integral operators require only two multigrid cycles
  \begin{itemize}
    \item \magenta{\href{http://books.google.com/books?id=_7N7QgAACAAJ}{Multigrid of the 2nd kind}} by \magenta{\href{http://www.mis.mpg.de/scicomp/hackbusch_e.html}{Hackbush}}
  \end{itemize}
\end{itemize}
\end{frame}
%
%% I believe that Dave's code never changes the BC, just the forcing f, so we should just need
%% a single FMM instead of BEM solve at each step. Is this true?
%
\subsection{Interpolation}
\begin{frame}{FEM $\longleftrightarrow$ BEM}

FEM $\longrightarrow$ BEM
\begin{itemize}
  \item FEM boundary conditions can be directly used in BEM

  \item May require a \code{VecScatter}
\end{itemize}

\bigskip

FEM $\longleftarrow$ BEM
\begin{itemize}
  \item BEM can evaluate the field at any domain point

  \item Cost is linear in the number of evaluations using FMM

  \item Can accomodate both
  \begin{itemize}
    \item pointwise values, and

    \item moments by quadrature
  \end{itemize}
\end{itemize}
\end{frame}
%
%
\section{Advantages and Disadvantages}
%
\subsection{Bandwidth}
\begin{frame}{Bandwidth and Serial Performance}

\begin{itemize}
  \item Provably low bandwidth
  \begin{itemize}
    \item \magenta{\href{http://portal.acm.org/citation.cfm?id=289842}{Shang-Hua Teng, SISC, 19(2), 635--656, 1998}}
  \end{itemize}

  \bigskip

  \item Key advantage over algebraic methods like FFT
  \begin{itemize}
    \item Similar to wavelet transform
  \end{itemize}

  \bigskip

  \item Amenable to GPU implementation
  \begin{itemize}
    \item Also highly concurrent
  \end{itemize}
\end{itemize}
\end{frame}
%
\subsection{Convergence}
\begin{frame}{Convergence and Synchronization}

\begin{itemize}
  \item BEM matrices are better conditioned
  \begin{itemize}
    \item However, FEM has better preconditioners

    \item Without better preconditioners, might see more synchronization

    \item Underexplored
  \end{itemize}

  \bigskip

  \item FMM can avoid bottleneck at lower levels
  \begin{itemize}
    \item Overlap direct work with lower tree levels

    \item Can provably eliminate bottleneck
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Debatable Advantages}

\begin{itemize}
  \item Small memory
  \begin{itemize}
    \item FEM can be done matrix-free
  \end{itemize}

  \bigskip

  \item Opens door to using Stokes operator for PC
  \begin{itemize}
    \item We currently do not know what to do here
  \end{itemize}
\end{itemize}
\end{frame}
%
\section{What is Next?}
%
\begin{frame}{Direct Fast Method for Variable-Viscosity Stokes}

\begin{itemize}
  \item<1-> Complexity not currently precisely quantified
  \begin{itemize}
    \item We would like a given number of flops/digit of accuracy
  \end{itemize}

  \bigskip

  \item<2-> Brute Force
  \begin{itemize}
    \item Use BEM to compute layers between regions of constant viscosity

    \item Better conditioned, but not direct
  \end{itemize}

  \bigskip

  \item<3-> Elegant method should be possible
  \begin{itemize}
    \item The operator is pseudo-differential

    \item ``Kernel-independent'' FMM exists
  \end{itemize}
\end{itemize}
\end{frame}

\end{document}
