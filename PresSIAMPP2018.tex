\documentclass{beamer}

\input{tex/talkPreamble.tex}
\usepackage{multimedia}
\usetikzlibrary{thplex,trees}

\title[PETSc]{Geodynamic Simulator Building}
\author[Matt]{Matthew~Knepley and Margarete~Jadamec}
\date[PP18]{SIAM Parallel Processing, \\Tokyo, Kant\=o JP \qquad March 10, 2018}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering \& Geology\\
  University at Buffalo\\
}
\subject{PETSc}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.15]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[height=0.65in]{figures/logos/UB_Primary.png}
  \end{center}
  \vskip0.4in
\end{frame}
%
% Speak: Research problems are by definition unique. It is difficult, or impossible, to anticipate what a user might
% want to change, and thus designing for general input is a quixotic task. Better to design for composability and
% transparency.
%
\begin{frame}{Main Idea}
\Huge
A Simulator is more Useful\\
\bigskip
\quad when the Researcher\\
\bigskip
\qquad Builds it Themselves
\end{frame}
%
% Speak: A lot of science can be done just by giving inputs to an existing simulator. However, when the problem changes due
% to new experiements, new theories, and new mechanisms at increasing resolution, the user has nowhere to go. I am not
% a fan of the current trend to ``team up'', so that a CS person does the CS stuff and Geo person does the Geo stuff.
% This is not computer science, its programming. Computer science is constructing the right abstractions, so that the
% user can do it themselves.
%
\section{Flexible Meshing}
%
\begin{frame}{Main Question}
\Huge
How do I handle\\
\bigskip
\quad many different mesh types\\
\bigskip
\qquad simply and efficiently?
\end{frame}
%
% Speak: People tend to bake the mesh type into their code, making it difficult/impossible to compare one to the other,
% or to try out new kinds of meshes.
%
\begin{frame}{Current Practice}\LARGE

Most packages handle one kind of mesh,\\
\pause
\bigskip
or have completely separate code paths\\
\bigskip
\qquad for different meshes

\end{frame}
%
\begin{frame}{Current Practice}\LARGE

This strategy means there is\\
\bigskip
\qquad a lot more code to maintain,\\
\pause
\bigskip
\qquad \qquad and results in \red{technical debt}.

\end{frame}
%
\begin{frame}{PETSc Strategy}\Large

The {\tt Plex} abstraction allows us to write code for
\medskip
\begin{itemize}
  \item<2->[] parallel distribution and load balancing,
  \item<3->[] traversal for function/operator assembly,
  \item<4->[] coarsening and refinement,
  \item<5->[] generation of missing edges/faces,
  \item<6->[] and surface extraction,
\end{itemize}
\medskip
\visible<7->{just \blue{\bf once}.}

\end{frame}
%
\input{slides/Plex/ExampleMeshes.tex}
%
\subsection{PyLith}
\begin{frame}{Example: \magenta{\href{https://geodynamics.org/cig/software/pylith/}{PyLith}}}\LARGE

\begin{columns}
\begin{column}[T]{0.7\textwidth}
\end{column}
\begin{column}[T]{0.3\textwidth}
\begin{itemize}
  \item<1->[] Many cell types
  \bigskip
  \item<5->[] Surface extraction
  \bigskip
  \item<6->[] Hybrid meshes
\end{itemize}
\end{column}
\end{columns}
\only<1-4>{\begin{textblock}{0.4}[0.0,0.0](0.0,0.15)\includegraphics[height=1.5in]{figures/PyLith/example_tri3.png}\end{textblock}}
\only<2-4>{\begin{textblock}{0.4}[0.0,0.0](0.1,0.25)\includegraphics[height=1.5in]{figures/PyLith/example_quad4.png}\end{textblock}}
\only<3-4>{\begin{textblock}{0.4}[0.0,0.0](0.2,0.35)\includegraphics[height=1.5in]{figures/PyLith/example_tet4.png}\end{textblock}}
\only<4>{\begin{textblock}{0.4}[0.0,0.0](0.3,0.45)\includegraphics[height=1.5in]{figures/PyLith/example_hex8.png}\end{textblock}}
\only<5>{\begin{textblock}{0.8}[0.0,0.0](0.0,0.25)\includegraphics[width=\textwidth]{figures/PyLith/beavan_subevents_slip.png}\end{textblock}}
\only<6>{\begin{textblock}{0.8}[0.0,0.0](0.0,0.25)\includegraphics[width=\textwidth]{figures/PyLith/cohesivecell.pdf}\end{textblock}}

\begin{center}\large
  \magenta{\href{http://onlinelibrary.wiley.com/doi/10.1002/jgrb.50217/abstract}{Aagaard, Knepley, Williams}}, J. of Geophysical Research, 2013.
\end{center}
\end{frame}

\subsection{DMNetwork}
\begin{frame}{Example: DMNetwork}\LARGE

{\tt Plex} on 30K cores of Edison\\
\quad for a finite volume\\
\qquad hydraulic flow application.

\bigskip

\begin{textblock}{0.3}[1.0,0.5](1.05,0.5)\includegraphics[width=\textwidth]{figures/DMNetwork/CorteSorensonHydraulicNetwork}\end{textblock}
\includegraphics[width=0.7\textwidth]{figures/DMNetwork/EdisonPerformanceTable.pdf}

\bigskip

\begin{center}\large
  \magenta{\href{http://www.mcs.anl.gov/papers/P7065-0617.pdf}{Maldonado, Abhyankar, Smith, Zhang}}, ACM TOMS, 2017
\end{center}
\end{frame}
%
%
% Speak: However, meshes are only a fraction of what is happening. You need much more to get the entire simulation
% going. You need to connect data to the mesh, and this is usually where codes really put their foot in it. I had tried
% to do this several times and ended up with something along the lines of the Hilbert Class Library, with objects
% everywhere and not orthogonality in the code. So...
%
\section{Interaction of Discretizations and Solvers}
%
\begin{frame}{Main Question}
\Huge
How do I handle\\
\bigskip
\ \ many different discretizations\\
\bigskip
\qquad simply and efficiently?
\end{frame}
%
% Speak: People tend to bake the discretization into their code, making it difficult/impossible to compare one to the other,
% or to try out new kinds of discretizations.
%
\begin{frame}{Current Practice}\LARGE

Most packages handle one discretization,\\
\pause
\bigskip
\quad FEniCS/Firedrake is a notable exception,\\
\pause
\bigskip
and interface poorly with solvers,\\
\bigskip
\qquad especially hierarchical solvers.

\end{frame}
%
\begin{frame}{PETSc Strategy}\Large
The {\tt Section} abstraction allows us to write code for
\medskip
\begin{itemize}
  \item<2->[] parallel data layout,
  \item<3->[] block/field decompositions,
  \item<4->[] (variable) point-block decompositions,
  \item<5->[] removing Dirichlet conditions,
  \item<6->[] (nonlinear) hierarchical rediscretization,
  \item<7->[] and partial assembly (BDDC/FETI),
\end{itemize}
\medskip
\visible<8->{just \blue{\bf once}.}

\end{frame}
%
\begin{frame}{Section}\Large

A {\tt Section} is a map
\begin{center}
  mesh point $\Longrightarrow$ (size, offset)
\end{center}
\medskip
\begin{overprint}
\onslide<2>\large
\begin{tabular}{ll}
Data Layout         & mesh point $\Longrightarrow$ \# dofs \\
Boundary conditions & mesh point $\Longrightarrow$ \# constrained dofs\\
Fields              & mesh point $\Longrightarrow$ \# field dofs
\end{tabular}
\onslide<3>
Decouples Mesh, Discretization, and Solver
\onslide<4>
Decouples \blue{Mesh}, \blue{Discretization}, and Solver\\
\medskip
\begin{center}
Assembly gets dofs on each point and mesh traversal,\\
no need for discretization spaces
\end{center}
\onslide<5>
Decouples \blue{Mesh}, Discretization, and \blue{Solver}\\
\medskip
\begin{center}
Solver gets data layout and ordering,\\
no need for mesh traversal
\end{center}
\onslide<6>
Decouples Mesh, \blue{Discretization}, and \blue{Solver}\\
\medskip
\begin{center}
Solver gets field and point blocking,\\
no need for discretization spaces
\end{center}
\onslide<7>
Decouples Mesh, Discretization, and Solver\\
\medskip
\begin{center}
Provides interface layer between PETSc and\\
discretization packages Firedrake and LibMesh
\end{center}
\end{overprint}
\end{frame}
%
%\begin{frame}{Do This}
%  pTatin Example
%
%\begin{center}\large
%  \magenta{\href{http://ieeexplore.ieee.org/abstract/document/7013010/}{May, Brown, Le Pourhiet}}, SC, 2014.
%\end{center}
%\end{frame}
%
\subsection{PCTelescope}
\begin{frame}[fragile]{Example: PCTelescope}\Large

{\tt PCTelescope} abstracts the parallel distribution\\
\smallskip
\quad of a linear system, so that\\
\bigskip
\begin{overprint}
\onslide<2>
a user can bring their coarse level\\
\smallskip
\quad onto a single process for a direct solve,
\begin{bash}
-pc_type mg
  -pc_mg_levels N
  -mg_coarse_pc_type telescope
    -mg_coarse_pc_telescope_reduction_factor nc
    -mg_coarse_telescope_pc_type lu
\end{bash}
\onslide<3>
or recreate the solver from the\\
\smallskip
\quad Gordon Bell Prize Winner 2015.
\begin{bash}
-pc_type mg
  -pc_mg_levels NR
  -mg_coarse_pc_type telescope
    -mg_coarse_pc_telescope_reduction_factor r
    -mg_coarse_telescope_pc_type mg
    -mg_coarse_telescope_pc_mg_levels NG
    -mg_coarse_telescope_pc_mg_galerkin
      -mg_coarse_telescope_mg_coarse_pc_type gamg
\end{bash}
\onslide<4>
The paper shows scaling up to\\
\smallskip
\quad $32^3$ processors on Piz Daint.
\onslide<5>
The paper shows scaling up to\\
\smallskip
\quad $32^3$ processors on Piz Daint,\\
\smallskip
\quad\quad and also hybrid CPU-GPU solvers.
\end{overprint}
\medskip
\begin{center}\large
  \magenta{\href{http://arxiv.org/abs/1604.07163}{May, Sanan, Rupp, Knepley, Smith}}, PASC, 2016. \magenta{\href{https://www.mcs.anl.gov/petsc/meetings/2016/slides/sanan.pdf}{slides}}
\end{center}
\end{frame}
%
\subsection{GMG with coefficients}
\begin{frame}{Geometric Multigrid with a Coefficient}\LARGE

\begin{overprint}
\onslide<1>
Regional mantle convection\\
\smallskip
\quad has highly variable viscosity,\\
\smallskip
\quad\quad due to temperature and strain rate.

\includegraphics[width=\textwidth]{figures/Subduction/Jadamec_RFIfig.pdf}

\onslide<2>
We will specify an initial temperature,\\
\smallskip
\quad on some initial mesh, and\\
\smallskip
\quad\quad let strain develop self-consistently.

\begin{center}
\includegraphics[width=0.4\textwidth]{figures/Subduction/3dmdl_InitTherm_RadSlicesEd4RevFINAL.png}
\end{center}

\onslide<3>
This temperature must be distributed,\\
\smallskip
\quad matching the mesh partition,\\
\smallskip
\quad\quad and interpolate/restrict to meshes.

\begin{center}
\includegraphics[width=0.4\textwidth]{figures/Subduction/3dmdl_InitTherm_RadSlicesEd4RevFINAL.png}
\end{center}

\end{overprint}

\begin{center}\large
  \magenta{\href{http://doi.org/10.1038/nature09053}{Jadamec, Billen}}, Nature, 2009.
\end{center}
\end{frame}
%
% Speak: Have to really explain why we do this. We need to coarse, but we do not have a
% coarsening routine for structured meshes. We could write one, but instead we cleverly use the existing infrastructure,
% and we can still redistribute on the fine level with DMPlexDistribute(), or the coarse level with PCTELESCOPE.
%
\begin{frame}[fragile]{Geometric Multigrid with a Coefficient: Part I}{Distribution}

\begin{enumerate}
  \item<1->[] Create {\tt Section} mapping temperature to coarse cells,\\
        using {\tt PetscFECreateDefault()} for a DG function space
  \medskip
  \item<2->[] Distribute the coarse mesh,\\
        using {\tt DMPlexDistribute()}
  \medskip
  \item<3->[] Distribute the cell temperatures,\\
        using {\tt DMPlexDistributeField()}
  \medskip
  \item<4->[] Transfer cell temperatures to finer cells (purely local)
  \medskip
  \item<5->[] Transfer cell temperatures to vertices on fine grid (purely local)
\end{enumerate}
\end{frame}
%
\begin{frame}[fragile]{Geometric Multigrid with a Coefficient: Part II}{Interpolation}

Interpolation is straightforward
\pause
\medskip
\begin{cprog}
DMRefine(coarseMesh, comm, &fineMesh);
DMCreateInterpolation(coarseMesh, fineMesh, &I, &Rscale);
MatMult(I, coarseTemp, fineTemp);
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Geometric Multigrid with a Coefficient: Part II}{Interpolation}

Now we restrict the input temperature to coarser meshes.
\pause
\medskip
\begin{cprog}
DMCreateInterpolation(coarseMesh, fineMesh, &I, &Rscale);
MatMultTranspose(I, fineTemp, coarseTemp);
VecPointwiseMult(coarseTemp, coarseTemp, Rscale);
\end{cprog}
\end{frame}
%
\begin{frame}{Geometric Multigrid with a Coefficient: Part II}{Interpolation}\Large

\begin{overprint}
\onslide<1>
  Mantle Temperature (Fine Grid, Level 3)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4Temp.png}
\onslide<2>
  Mantle Temperature (Fine Grid, Level 3)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom.png}
\onslide<3>
  Mantle Temperature (Level 2, Q1 Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L2_Q1Avg.png}
\onslide<4>
  Mantle Temperature (Level 1, Q1 Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L1_Q1Avg.png}
\onslide<5>
  Mantle Temperature (Level 0, Q1 Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L0_Q1Avg.png}
\end{overprint}

\end{frame}
%
\begin{frame}[fragile]{Geometric Multigrid with a Coefficient: Part II}{Interpolation}

The power mean could better preserve low temperatures
\begin{align*}
  \bar x = \left( \sum_i x_i^p \right)^{\frac{1}{p}}
\end{align*}

\begin{overprint}
\onslide<2>
\begin{cprog}
DMCreateInterpolation(coarseMesh, fineMesh, &I, &Rscale);
MatShellSetOperation(I, MATOP_MULT_TRANSPOSE,
  MatMultTransposePowerMean_SeqAIJ);
MatMultTranspose(I, fineTemp, coarseTemp);
VecPointwiseMult(coraseTemp, coarseTemp, Rscale);
VecPow(coarseTemp, p);
\end{cprog}
\onslide<3>
\begin{cprog}
for (i = 0; i < m; ++i) {
  idx = a->j + a->i[i];
  v   = a->a + a->i[i];
  n   = a->i[i+1] - a->i[i];
  xi  = x[i];
  for (j = 0; j < n; ++j) {
    y[idx[j]] += v[j]*PetscPowScalarReal(xi, p);
  }
}
\end{cprog}
It reuses the parallel {\tt MatMultTranspose()} implementation.
\end{overprint}
\end{frame}
%
\begin{frame}{Geometric Multigrid with a Coefficient: Part II}{Interpolation}\Large

\begin{overprint}
\onslide<1>
  Mantle Temperature (Fine Grid, Level 3)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom.png}
\onslide<2>
  Mantle Temperature (Level 2, Harmonic Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L2_HarmAvg.png}
\onslide<3>
  Mantle Temperature (Level 1, Harmonic Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L1_HarmAvg.png}
\onslide<4>
  Mantle Temperature (Level 0, Harmonic Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L0_HarmAvg.png}
\onslide<5>
  Mantle Temperature (Level 2, $p=-1.5$ Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L2_Harm15Avg.png}
\onslide<6>
  Mantle Temperature (Level 1, $p=-1.5$ Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L1_Harm15Avg.png}
\onslide<7>
  Mantle Temperature (Level 0, $p=-1.5$ Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L0_Harm15Avg.png}
\onslide<8>
  Mantle Temperature (Level 0, Q1 Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L0_Q1Avg.png}
\onslide<9>
  Mantle Temperature (Level 0, Harmonic Restriction)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L0_HarmAvg.png}
\onslide<10>
  Mantle Temperature (Level 0, Q1 Avg - Harmonic Avg)\\
  \includegraphics[width=0.9\textwidth]{figures/Subduction/uf4TempZoom_L0_Q1AvgHarmDiff.png}
\end{overprint}

\end{frame}
%
\begin{frame}[fragile]{Geometric Multigrid with a Coefficient: Part III}{Solving}
\setbeamercolor{alerted text}{fg=blue!80!black}
\begin{semiverbatim}\scriptsize
\alert<2>{-snes_rtol 1e-7 -snes_atol 1e-12 -snes_linesearch_maxstep 1e20}
\alert<2>{  -ksp_rtol 1e-5}
\alert<3>{  -pc_type fieldsplit}
\alert<3>{    -pc_fieldsplit_diag_use_amat}
\alert<3>{    -pc_fieldsplit_type schur}
\alert<3>{    -pc_fieldsplit_schur_factorization_type full}
\alert<3>{    -pc_fieldsplit_schur_precondition a11}
\alert<5>{      -fieldsplit_velocity_ksp_type gmres}
\alert<5>{        -fieldsplit_velocity_ksp_rtol 1e-8}
\alert<5>{      -fieldsplit_velocity_pc_type mg}
\alert<5>{        -fieldsplit_velocity_pc_mg_levels n}
\alert<6>{          -fieldsplit_velocity_mg_levels_ksp_type gmres}
\alert<6>{            -fieldsplit_velocity_mg_levels_ksp_max_it 4}
\alert<6>{            -fieldsplit_velocity_mg_levels_pc_type pbjacobi}
\alert<6>{            -fieldsplit_velocity_mg_levels_pc_pbjacobi_variable}
\alert<6>{            -fieldsplit_velocity_mg_levels_pc_use_amat}
\alert<4>{      -fieldsplit_pressure_pc_type asm}
\alert<4>{        -fieldsplit_pressure_sub_pc_type ilu}
\alert<4>{        -fieldsplit_pressure_ksp_rtol 1e-4}
\alert<4>{        -fieldsplit_pressure_ksp_max_it 20}
\end{semiverbatim}
\end{frame}
%
% Speak: Notice that when we ask for GMG on the velocity, PETSc retrieves the mesh hierarchy
% information, subsets the section and discretization information by field, and constructs the
% coarse operators (using the already constructed temperatures). The discretization information
% is unnecessary if we use -fieldsplit_velocity_mg_galerkin
%
\subsection{Comparison of Discretizations}
\begin{frame}{Example: TAS Performance Analysis}\Large

\begin{columns}
\begin{column}{0.68\textwidth}
\begin{overprint}
\onslide<1>
\begin{center}\LARGE
  Static Scaling (1K procs)
\end{center}
\includegraphics[width=\textwidth]{/Users/knepley/PETSc4/papers/performance/tas-spectrum/Figures/solvers_dofsec.pdf}
\onslide<2>
\begin{center}\LARGE
  Accuracy Scaling (1K procs)
\end{center}
\includegraphics[width=\textwidth]{/Users/knepley/PETSc4/papers/performance/tas-spectrum/Figures/solvers_errtime.pdf}
\end{overprint}
\end{column}
\begin{column}{0.32\textwidth}
\begin{overprint}
\onslide<1>
Measure\\
dof/s,\\

\medskip

fixed parallelism,\\

\medskip

different sizes.
\onslide<2>
Measure\\
error $\times$ time\\

\medskip

fixed parallelism\\

\medskip

different sizes.
\end{overprint}
\end{column}
\end{columns}
\medskip

\medskip
\begin{center}\large
  \magenta{\href{http://arxiv.org/abs/1802.07832}{Chang, Fabien, Knepley, Mills}}, submitted, 2018.
\end{center}
\end{frame}
%
\begin{frame}{Conclusions}\Huge

\begin{overprint}
\onslide<1>
Abstractions for\\
\smallskip
\quad Topology and Geometry,\\
\smallskip
\quad\quad Data Layout, and\\
\smallskip
\quad\quad\quad Operator Composition\\
\smallskip
\hfil let the \hfil\\
User construct the Simulator.
\onslide<2>
\begin{center}

  \vskip0.5in

  \magenta{\href{http://bitbucket.org/petsc/petsc}{http://bitbucket.org/petsc/petsc}}

  \bigskip

  \magenta{\href{http://github.com/petsc/petsc}{http://github.com/petsc/petsc}}
\end{center}
\end{overprint}
\end{frame}
%
\begin{frame}{Example: Magma Dynamics}

  Show magma performance using FAS

\begin{center}\large
  \magenta{\href{https://www.cse.buffalo.edu/~knepley/presentations/PresCambridge2016.pdf}{Knepley}}, Melt in the Mantle Program, Newton Institute, 2016.
\end{center}
\end{frame}

\end{document}
