\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\beamertemplatenavigationsymbolsempty

\title[NPC]{Composing Nonlinear Solvers}
\author[M.~Knepley]{Matthew~Knepley}
\date[ICERM]{Numerical Methods for\\Large-Scale Nonlinear Problems and Their Applications\\ICERM, Providence, RI \qquad September 4, 2015}
% - Use the \inst command if there are several affiliations
\institute[Rice]{
  Computational and Applied Mathematics\\
  Rice University
}
\subject{Me}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.10]{figures/logos/RiceLogo_TMCMYK300DPI.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

%
\begin{frame}<testing>{Abstract}
Title: Composing Nonlinear Solvers

Abstract:
We present, in analogy with the linear preconditioning operation, a framework for the composition of nonlinear solvers,
which we call \textit{nonlinear preconditioning}, in order to accelerate convergence and improve solver performance. A
unified software design is proposed, and illustrated with some examples of complex solver organization.

Bio:
Matthew G. Knepley received his B.S. in Physics from Case Western Reserve University in 1994, an M.S. in Computer
Science from the University of Minnesota in 1996, and a Ph.D. in Computer Science from Purdue University in 2000. He
was a Research Scientist at Akamai Technologies in 2000 and 2001. Afterwards, he joined the Mathematics and Computer
Science department at Argonne National Laboratory (ANL), where he was an Assistant Computational Mathematician, and a
Fellow in the Computation Institute at University of Chicago. In 2009, he joined the Computation Institute as a Senior
Research Associate. His research focuses on scientific computation, including fast methods, parallel computing, software
development, numerical analysis, and multi/manycore architectures. He is an author of the widely used PETSc library for
scientific computing from ANL, and is a principal designer of the PyLith library for the solution of dynamic and
quasi-static tectonic deformation problems. He developed the PETSc scalable unstructured mesh support based upon ideas
from combinatorial topology. He was a J. T. Oden Faculty Research Fellow at the Institute for Computation
Engineering and Sciences, UT Austin, in 2008, and won the R\&D 100 Award in 2009, and the SIAM/ACM Prize in
Computational Science and Engineering in 2015 as part of the PETSc team.

\end{frame}
%
\input{slides/PETSc/ProgrammingWithOptions.tex}
\input{slides/PETSc/MagmaFASOptions.tex}
%
%
\section{Composition Strategies}
\input{slides/SNES/BasicSystem.tex}
\input{slides/SNES/LeftPrec.tex}
\input{slides/SNES/AdditiveNPrec.tex}
\input{slides/SNES/LeftNPrec.tex}
\input{slides/SNES/MultiplicativeNPrec.tex}
\input{slides/SNES/RightNPrec.tex}
\input{slides/SNES/NPrecTable.tex}
%
\section{Algebra}
\input{slides/SNES/AdditiveAlgebra.tex}
\input{slides/SNES/MultiplicativeAlgebra.tex}
\input{slides/SNES/NearRings.tex}
\input{slides/SNES/PolynomialDecomposition.tex}
%
\section{Solvers}
\subsection{Richardson}
\input{slides/SNES/NRICH.tex}
\input{slides/SNES/NRICH-LP.tex}
\subsection{Newton}
\input{slides/SNES/NK.tex}
\input{slides/SNES/LP-NK.tex}
\input{slides/SNES/RP-NK.tex}
\subsection{Generalized Broyden}
\input{slides/SNES/GB.tex}
\input{slides/SNES/LP-GB.tex}
\input{slides/SNES/RP-GB.tex}
%
%
\section{Examples}
\begin{frame}
\begin{center}\Huge
I ran NPC on some problem\\

and it worked.
\end{center}
\end{frame}
%
%
\section{Convergence}
\input{slides/NDI/RateOfConvergence.tex}
\input{slides/NDI/NDI.tex}
\input{slides/NDI/Newton.tex}
\input{slides/SNES/NPCLeftVsRight.tex}
\input{slides/NDI/RP.tex}
\input{slides/NDI/NonAbelian.tex}
\input{slides/NDI/AsymmetryExample.tex}
%
%\begin{frame}{Chord Method}
%\begin{overprint}
%\onslide<1>
%\begin{align*}
%  \omega_\N(r) &= \frac{1}{2} k_0 r^2 + r \left(1 - \sqrt{k^2_0 a^2 + 2 k_0 r}\right) \\
%  \sigma_\N(r) &= \sqrt{2 k^{-1}_0 r + a^2} - a
%\end{align*}
%\onslide<2>
%\begin{align*}
%  \omega_\N(r) &= \frac{1}{2} k_0 r^2 + r \left(1 - \sqrt{1 - 2 k_0 (r - r_0)}\right) \\
%  \sigma_\N(r) &= \sqrt{2 k^{-1}_0 r + a^2} - a
%\end{align*}
%\end{overprint}
%\end{frame}
%
%
\section{Further Questions}
\begin{frame}
\begin{center}
  \bf\LARGE Further Questions
\end{center}
\begin{itemize}
  \item Can we say something general about left preconditioning?
  \bigskip
  \item Can the composed iteration have a larger region of convergence?
  \bigskip
  \item What should be a nonlinear smoother?
  \bigskip
  \item Can we usefully predict the convergence of NPC solvers?
\end{itemize}
\end{frame}

\end{document}
%
%
\section*{Software Organization}
%
\begin{frame}[fragile]{Generic Solve Routine}
\begin{cprog}
SNESSolve(SNES snes) {
  SNESComputeInitialResidual(snes, X, F, &fnorm); /* Apply left  NPC */
  /* Test for convergence */
  /* Outer Iteration */
  for (i = 0; i < maxits; ++i) {
    SNESApplyNPCRight(snes, X, B, F, &fnorm);     /* Apply right NPC */
    SNESComputeUpdate(snes, X, F, Y);
    /* Check for divergence or iterative failure */
    SNESComputeNextResidual(snes, X, F, &fnorm);  /* Apply left  NPC */
    SNESRestart(snes, &restartCount, X, F);
    /* Test for convergence */
  }
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Richardson Update}
\begin{cprog}
SNESComputeUpdate_NRichardson(SNES snes, Vec X, Vec F, Vec Y)
{
  VecCopy(F, Y);
  VecNorm(F, NORM_2, &fnorm);
  SNESLineSearchApply(snes->linesearch, X, F, &fnorm, F);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Newton Update}
\begin{cprog}
SNESComputeUpdate_Newton(SNES snes, Vec X, Vec F, Vec Y)
{
  /* Solve J Y = F, where J is Jacobian matrix */
  SNESComputeJacobian(snes, X, snes->jacobian, snes->jacobian_pre);
  KSPSetOperators(snes->ksp, snes->jacobian, snes->jacobian_pre);
  KSPSolve(snes->ksp, F, Y);
  /* Compute new solution */
  VecAXPY(X, -1.0, Y);
}
\end{cprog}
\end{frame}
%
\begin{frame}[fragile]{Generalized Broyden Update}
\begin{cprog}
SNESComputeUpdate_GenBroyden(SNES snes, Vec X, Vec F, Vec Y)
{
  /* Minimize || f_k - F_k gamma_k || to get gamma_k */
  /* Now gamma_k = H^{-1} b where H = F^T_k F_k and b = F^T_k f_k */
  /* Save old solution for restart checks */
  ierr = VecCopy(X, XM);CHKERRQ(ierr);
  ierr = VecCopy(F, FM);CHKERRQ(ierr);
  /* Update solution */
  for (i = 0; i < l; ++i) gammaSum += gamma[i];
  ierr = VecScale(X, 1.0 - gammaSum);CHKERRQ(ierr);
  ierr = VecMAXPY(X, l, gamma, DeltaX);CHKERRQ(ierr);
  if (andersonBeta != 0.0) {
    ierr = VecAXPY(X, (1.0 - gammaSum)*andersonBeta);CHKERRQ(ierr);
    for (i = 0; i < l; ++i) gamma[i] *= andersonBeta;
    ierr = VecMAXPY(X, l, gamma, DeltaF);CHKERRQ(ierr);
  }
  /* Calculate update */
  ierr = VecCopy(X, Y);CHKERRQ(ierr);
  ierr = VecAXPY(Y, -1.0, XM);CHKERRQ(ierr);
}
\end{cprog}
\end{frame}
%
%
\section*{Multilevel Example}
\input{slides/SNES/Ex19Equations.tex}
\input{slides/SNES/SNESEx19.tex}
\input{slides/SNES/NPCEx19.tex}
