% Outline (35 min)
% - Unstructured and Semistructured grids (DMPlex, DMForest)
%   - Look for pictures in presentations for National Grid and CHREST (Mars)
% - Discretizatons (PetscFE and PetscFV)
%   - FEM examples
%   - CEED interface
% - Particles (DMSwarm)
%   - Get Joe's scalability graph
%   - Radiation example?
% - Solvers
%   - SNES
%   - TS
%   - TAO/SLEPc (just mention)
% - Conservative Projection
%   - Mixes grids/disc and particles
% - GMG and Patch Solvers
%   - Mixes grids and solvers
% - Landau Damping
%   - Mixes grids/disc, particles, solvers
%
%
\documentclass[aspectratio=169,dvipsnames,18pt]{beamer}

\input{talkPreambleNoBeamer.tex}
\usepackage{physics}
\usetikzlibrary{tikzmark}
\usepackage{hf-tikz}
\usetikzlibrary{shapes, arrows.meta,arrows}
\usetikzlibrary{positioning}
\usepackage{multimedia}
%
\mode<presentation>
{
  \usetheme{Malmoe}
  \usefonttheme{serif}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamerfont{title}{size=\normalsize}
\setbeamerfont{author}{size=\footnotesize,series=\bfseries}
\setbeamerfont{title page}{size=\scriptsize}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxcitenames=3, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{presentations.bib}
\renewcommand*{\bibfont}{\tiny}

\title[Plex]{Simulation Components in PETSc}
\author[Knepley]{Matt Knepley, Mark Adams, Joseph Pusztay, Danny Finn}
\date[Nov]{NumKin 2023\\Max Planck Institut f\"ur Plasmaphysik, Garching, DE\\Nov 7, 2023}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{Plex}

\begin{document}
\beamertemplatenavigationsymbolsempty

{
\usebackgroundtemplate{\includegraphics[width=\paperwidth]{figures/logos/CHREST_Background.pdf}}%
\setbeamercolor{title}{fg=white}\usebeamercolor*{title}
\setbeamercolor{normal text}{fg=white}\usebeamercolor*{normal text}
\begin{frame}
  \titlepage
\end{frame}
}
%
\begin{frame}
\Large
\begin{center}
  \Huge Never believe anything\\

  \bigskip
  \bigskip

  \qquad until you run it.
\end{center}
\end{frame}
%
%
\section{Unstructured and Semistructured grids}
%
\begin{frame}{Unstructured Meshes}{PETSc DMPlex}\Large

  PETSc supports unstructured and structured adaptive meshes\\
  \qquad using the DMPlex object.
\smallskip
\only<1-4>{
\begin{itemize}
  \item<1-> Dimension independent

  \item<2-> Supports hybrid meshes

  \item<3-> Structured adaptive through \blue{\href{https://www.p4est.org/}{p4est}} \parencite{isaacknepley2017}
\end{itemize}
}
\only<5->{
\begin{itemize}
  \item<5-> Dimension independent

  \item<5-> Supports hybrid meshes

  \item<5-> AMR: \parencite{barralknepleylangepiggottgorman2016}, \parencite{wallworkknepleybarralpiggott2022}
\end{itemize}
}

\begin{overprint}
\onslide<1-2>
\newcommand{\vStart}{2}
\newcommand{\vEnd}{5}
\newcommand{\numVertices}{4}
\newcommand{\vShift}{3.50}
\newcommand{\eStart}{6}
\newcommand{\eEnd}{10}
\newcommand{\eShift}{3.00}
\newcommand{\numEdges}{5}
\newcommand{\cStart}{0}
\newcommand{\cEnd}{1}
\newcommand{\numCells}{2}
\newcommand{\cShift}{4.50}
\begin{center}
\begin{tikzpicture}[scale = 1.5,font=\fontsize{8}{8}\selectfont]
\path (0.,0.) node(2_0) [draw,shape=circle,color=gray] {2};
\path (2.,0.) node(3_0) [draw,shape=circle,color=gray] {3};
\path (0.,2.) node(4_0) [draw,shape=circle,color=gray] {4};
\path (2.,2.) node(5_0) [draw,shape=circle,color=gray] {5};
\path
(0.,1.) node(6_0) [draw,shape=circle,color=orange] {6} --
(1.,0.) node(7_0) [draw,shape=circle,color=orange] {7} --
(1.,1.) node(8_0) [draw,shape=circle,color=orange] {8} --
(2.,1.) node(9_0) [draw,shape=circle,color=orange] {9} --
(1.,2.) node(10_0) [draw,shape=circle,color=orange] {10} --
(0,0);
\draw[color=gray] (4_0) -- (6_0) -- (2_0) -- (7_0) -- (3_0) -- (8_0) -- (4_0);
\draw[color=gray] (3_0) -- (9_0) -- (5_0) -- (10_0) -- (4_0) -- (8_0) -- (3_0);
\path (0.666667,0.666667) node(0_0) [draw,shape=circle,color=green] {0};
\path (1.33333,1.33333) node(1_0) [draw,shape=circle,color=green] {1};
% Cells
\foreach \c in {\cStart,...,\cEnd}
{
  \node(\c_0) [draw,shape=circle,color=green,minimum size = 6mm] at (\cShift+\c-\cStart,0) {\c};
}
% Edges
\foreach \e in {\eStart,...,\eEnd}
{
  \node(\e_0) [draw,shape=circle,color=orange,minimum size = 6mm] at (\eShift+\e-\eStart,1) {\e};
}
% Vertices
\foreach \v in {\vStart,...,\vEnd}
{
  \node(\v_0) [draw,shape=circle,color=gray,minimum size = 6mm] at (\vShift+\v-\vStart,2) {\v};
}
\draw[->, shorten >=1pt] (6_0) -- (0_0);
\draw[->, shorten >=1pt] (7_0) -- (0_0);
\draw[->, shorten >=1pt] (8_0) -- (0_0);
\draw[->, shorten >=1pt] (9_0) -- (1_0);
\draw[->, shorten >=1pt] (10_0) -- (1_0);
\draw[->, shorten >=1pt] (8_0) -- (1_0);
\draw[->, shorten >=1pt] (4_0) -- (6_0);
\draw[->, shorten >=1pt] (2_0) -- (6_0);
\draw[->, shorten >=1pt] (2_0) -- (7_0);
\draw[->, shorten >=1pt] (3_0) -- (7_0);
\draw[->, shorten >=1pt] (3_0) -- (8_0);
\draw[->, shorten >=1pt] (4_0) -- (8_0);
\draw[->, shorten >=1pt] (3_0) -- (9_0);
\draw[->, shorten >=1pt] (5_0) -- (9_0);
\draw[->, shorten >=1pt] (5_0) -- (10_0);
\draw[->, shorten >=1pt] (4_0) -- (10_0);
\end{tikzpicture}
\end{center}
\onslide<3>
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/AMR/earth_3slice.png}\\
\textit{\scriptsize Issac, 2019}
\end{center}
\onslide<4>
\bigskip

Plex supports many common operations, in parallel,
\begin{itemize}
  \item refinement
  \item extrusion
  \item cell conversion
  \item edge/face creation
\end{itemize}
\onslide<5>
\includegraphics[width=0.55\textwidth]{figures/AMR/ParMMGTracer_Mesh_Wallwork.png}
\includegraphics[width=0.3\textwidth]{figures/AMR/bubble3d_20_mesh.png}
\onslide<6>
\bigskip
\bigskip

\begin{center}
  Plex publications:
  \smallskip
  \begin{enumerate}
    \item[] \parencite{knepleykarpeev09}
    \medskip
    \item[] \parencite{langemitchellknepleygorman2015}
    \medskip
    \item[] \parencite{knepleylangegorman2017}
    \medskip
    \item[] \parencite{haplaknepleyafanasievboehmdrielkrischerfichtner2020}
  \end{enumerate}
\end{center}
\end{overprint}
\end{frame}
%
\input{slides/Plex/Applications.tex}
%
\begin{frame}[fragile]{Example: Toroidal Mesh}\Large

\pause
Phase 1: Create one poloidal plane and distribute
\begin{bash}
  -phase1_dm_plex_filename /path/to/plane.mesh
  -phase1_dm_plex_simplex 0 -phase1_dm_refine_pre 1
\end{bash}
\pause
Phase 2: Refine in parallel
\begin{bash}
  -phase2_dm_refine 2
\end{bash}
\pause
Phase 3: Extrude periodically
\begin{bash}
  -phase3_dm_plex_transform_type extrude -phase3_dm_extrude 16
  -phase3_dm_plex_transform_extrude_thickness 1
  -phase3_dm_plex_transform_extrude_use_tensor false
  -phase3_dm_plex_transform_extrude_periodic
  -phase3_dm_extrude_levels 2
\end{bash}
\end{frame}
%
\begin{frame}[fragile]{Example: Toroidal Mesh}
\begin{center}
\includegraphics[height=\textheight]{figures/Plasma/tokamakGrid.png}
\end{center}
\end{frame}
%
\section{Discretizatons}
%
\begin{frame}{Continuum Discretizations}{PetscFE}\Large
\begin{itemize}
  \item[] Build spaces using
  \begin{itemize}
    \item $\mathcal{P}$
    \item $\mathcal{P}^-$
    \item direct sum
    \item direct product
  \end{itemize}
  \medskip
  \item[] Pointwise dual spaces are built topologically
  \medskip
  \item[] Support $L_2$, $H^1$, $H(\mathrm{div})$, $H(\mathrm{curl})$ spaces
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{Raviart-Thomas on a Quadrilateral}\LARGE
\begin{bash}
  -petscspace_type sum
  -petscspace_variables 2
  -petscspace_components 2
  -petscspace_sum_spaces 2
  -petscspace_sum_concatenate true
\end{bash}
\pause
\begin{bash}
  -sumcomp_0_petscspace_variables 2
  -sumcomp_0_petscspace_type tensor
  -sumcomp_0_petscspace_tensor_spaces 2
  -sumcomp_0_petscspace_tensor_uniform false
  -sumcomp_0_tensorcomp_0_petscspace_degree 1
  -sumcomp_0_tensorcomp_1_petscspace_degree 0
\end{bash}
\pause
\begin{bash}
  -sumcomp_1_petscspace_variables 2
  -sumcomp_1_petscspace_type tensor
  -sumcomp_1_petscspace_tensor_spaces 2
  -sumcomp_1_petscspace_tensor_uniform false
  -sumcomp_1_tensorcomp_0_petscspace_degree 0
  -sumcomp_1_tensorcomp_1_petscspace_degree 1
\end{bash}
\pause
\begin{bash}
  -petscdualspace_form_degree -1
  -petscdualspace_order 1
  -petscdualspace_lagrange_trimmed true
\end{bash}
\end{frame}
%
\begin{frame}{Continuum Discretizations}{PetscFV}\Large
\begin{itemize}
  \item[] Constant and linear reconstruction
  \medskip
  \item[] Pointwise Riemann solvers
  \medskip
  \item[] Library of limiters
  \medskip
  \item[] Interoperates with PetscFE
  \medskip
  \item[] Handles hanging nodes
\end{itemize}
\end{frame}
%
\begin{frame}{Particle Discretizations}{PETSc DMSwarm}\Large

  PETSc supports particle discretizations\\
  \qquad using the DMSwarm object.
\smallskip
\begin{itemize}
  \item Arbitrary particle data

  \item Scalable particle push

  \item Scalable point location for unstructured meshes

  \item Conservative projection from/onto FEM bases
\end{itemize}

\bigskip
\bigskip

\end{frame}
%
\begin{frame}{Particle Discretizations}{Swarm Applications}\Large

  Swarm has been used in a few of large-scale applications:
\smallskip
\begin{itemize}
  \item<1-> Plasma kinetics in \blue{\href{https://hbps.pppl.gov/computing/xgc-1}{XGC1}}
  \item<3-> Geodynamics in \blue{\href{https://bitbucket.org/ptatin/ptatin3d/src/master/}{ptatin3d}}
  \item<4-> Plan to use for droplets in \blue{\href{https://github.com/UBCHREST/ablate}{Ablate}}
\end{itemize}

\begin{overprint}
\onslide<1-2>
\begin{center}
  \includegraphics[width=0.8\textwidth]{figures/Plex/Applications/SwarmXGC1KE.pdf}
\end{center}
\onslide<3>
\begin{center}
  \includegraphics[width=0.6\textwidth]{figures/Swarm/Applications/pTatin3DTopography.pdf}
\end{center}
\onslide<4>
\raisebox{1in}{\parbox[c]{5em}{\LARGE Gravity Droplet}}
\includegraphics[width=0.10\textwidth]{figures/Droplet/AMR_paraffin.png}\hspace*{2em}
\includegraphics[width=0.10\textwidth]{figures/Droplet/AMR_paraffin_shear_20.png}
\raisebox{1in}{\parbox[c]{5em}{\LARGE Shear Droplet}}
\end{overprint}
\only<2>{\begin{textblock}{0.5}[0.0,0.0](0.65,0.3)\includegraphics[width=\textwidth]{figures/Plasma/2stream_rollup_rt_198.png}\end{textblock}}
\end{frame}
%
%
\section{Solvers}
%
\begin{frame}{Linear and Nonlinear}\Large

\begin{itemize}
  \item Many, many solvers and preconditioners
  \medskip
  \item Focus on optimal, multilevel solvers
  \medskip
  \item Patch multgrid
  \medskip
  \item Nonlinear preconditioners
\end{itemize}
\end{frame}
%
\begin{frame}[fragile]{Solving the Discrete Gradients System}\Large
\begin{itemize}
  \item<6-7>[] \alt<6>{Preconditioning matrix might be far from true matrix\\\phantom{(\bashinline{-snes_npc_type nrichardson})}}{Precondition Newton using Richardson\\(\bashinline{-snes_npc_type nrichardson})}
  \medskip
  \item<1->[] Line-Search Newton (\bashinline{-snes_type newtonls})
  \begin{itemize}\Large
    \item<2->[] \alt<2>{Green function for Laplace is dense\\\phantom{(\bashinline{-snes_mf_operator -pc_type lu -pc_factor_mat_solver_type mumps})}}{Act with FD residual, include block-diagonal in pmat\\(\bashinline{-snes_mf_operator -pc_type lu -pc_factor_mat_solver_type mumps})}
    \medskip
    \item<4->[] \alt<4>{DG Gonzalez term has global coupling\\\phantom{(use \bashinline{MatLRC} type)}}{Represent preconditioning matrix as $A + U C V^T$\\(use \bashinline{MatLRC} type)}
  \end{itemize}
\end{itemize}
\end{frame}
%
\begin{frame}{Timestepping}\Large

\begin{itemize}
  \item Explicit, Implicit, and IMEX formulations
  \medskip
  \item Symplectic methods
  \medskip
  \item Index 1 and 2 Differential-Algebraic Equations (DAE)
  \medskip
  \item Adaptivity and error control
  \medskip
  \item Discrete Adjoints and Sensitivity Analysis
  \medskip
  \item Handles events and discontinuities
\end{itemize}
\end{frame}
%
\begin{frame}{Optimization}\Large

\begin{itemize}
  \item Minimization
  \begin{itemize}
    \item Unconstrained
    \smallskip
    \item Bound-Constrained Optimization
    \smallskip
    \item Generally Constrained Solvers
  \end{itemize}
  \medskip
  \item Nonlinear Least-Squares
  \begin{itemize}
    \item Bound-constrained Regularized Gauss-Newton
    \smallskip
    \item Derivative-Free
  \end{itemize}
  \medskip
  \item Complementarity
  \medskip
  \item PDE-constrained Optimization
\end{itemize}
\end{frame}
%
\begin{frame}{Eigensolves}\Large

\begin{itemize}
  \item Linear Eigenvalue Problem
  \medskip
  \item Singular Value Decomposition
  \medskip
  \item Polynomial Eigenvalue Problem
  \medskip
  \item Nonlinear Eigenvalue Problem
  \medskip
  \item Matrix Function
  \medskip
  \item Linear Matrix Equation
\end{itemize}
\end{frame}
%
%
\section{Examples}
  \subsection{Conservative Projection}
  \begin{frame}\Huge
  Integrating into PETSc,\\

  \bigskip
  \bigskip

  \quad we get support and\\

  \bigskip
  \bigskip

  \quad\quad long term maintenance.
  \end{frame}
  %
  % Speak: Paraphrase slide
  \begin{frame}\Huge
  Using linear algebra interfaces,\\

  \bigskip
  \bigskip

  \quad we get portability\\

  \bigskip
  \bigskip

  \quad\quad and optimization.
  \end{frame}
  %
  % Speak: Paraphrase slide
  \begin{frame}\Huge
  Abstracting operations,\\

  \bigskip
  \bigskip

  \quad enables composition and\\

  \bigskip
  \bigskip

  \quad\quad new algorithmic choices.
  \end{frame}
  %
  % Speak: Paraphrase slide
  \begin{frame}\Huge
  By designing library interfaces,\\

  \bigskip
  \bigskip

  \quad we can embed PIC in\\

  \bigskip
  \bigskip

  \quad\quad more complex simulations.
  \end{frame}
  %
  % Speak: Paraphrase slide
  \begin{frame}{Linear Algebra}\huge
  As an example, let us derive:\\

  \bigskip
  \bigskip

  Conservative projection operators\\

  \bigskip

  \ \ between \blue{Particle} and \red{FEM} spaces.
  \end{frame}
  %
  % Speak: In what sense do we want $f_{fe} \approx f_P$? Suppose that $f$ is a distribution function in phase space.
  %        Then we might want to require that the total amount of substance (mass) is the same in both representations,
  %        (slide 2) and momentum, (slide 3) and energy, (slide 4) and in general we want weak equivalence.
  %        Thus if our basis F contains at least quadratic functions, we can conserve the first 3 moments
  \begin{frame}{Linear Algebra}\huge
  \begin{overprint}
  \onslide<1>
  \begin{align*}
    \int \red{f} = \int \blue{f}
  \end{align*}
  \onslide<2>
  \begin{align*}
    \int \vx \red{f} = \int \vx \blue{f}
  \end{align*}
  \onslide<3>
  \begin{align*}
    \int x^2 \red{f} = \int x^2 \blue{f}
  \end{align*}
  \onslide<4>
  \begin{align*}
    \int \phi_i \red{f} = \int \phi_i \blue{f} \qquad \forall \phi_i \in \red{\mathcal{F}}
  \end{align*}
  \end{overprint}

  \bigskip

  where

  \begin{align*}
    \red{f} \in \red{\mathcal{F}} \qquad \blue{f} \in \blue{\mathcal{P}}
  \end{align*}
  \end{frame}
  %
  % Speak: If we expand each field in the canonical basis for that space, we see the following relation,
  %        (slide 2) which linear algebraically is
  \begin{frame}{Linear Algebra}\huge
  \begin{align*}
    \int \phi_i \sum_j \red{c_j} \,\phi_j = \int \phi_i \sum_p \blue{w_p} \,\delta(\vx - \vx_p)
  \end{align*}
  \pause
  which in linear algebra terms is
  \begin{align*}
    M \red{c} = V \blue{w}
  \end{align*}
  \end{frame}
  %
  % Speak: M is just the traditional mass matrix, and V is the interpolation of the finite element basis functions to the particle locations
  \begin{frame}{Linear Algebra}\huge
  \begin{align*}
    M_{ij} = \int \phi_i \phi_j
  \end{align*}

  \bigskip

  \begin{align*}
    V_{ip} = \int \phi_i \,\delta(\vx - \vx_p)
  \end{align*}
  \end{frame}
  %
  % Speak: To get finite element coefficients from a particle field, we invert the mass matrix.
  %         However, for our Poisson solve, we want to get the weak rhs (M c) from particle weights, so we just apply V
  \begin{frame}{Linear Algebra}\huge
  FEM coef. from particle weights:
  \begin{align*}
    \red{c} = M^{-1} V \blue{w}
  \end{align*}

  \bigskip

  FEM rhs from particle weights:
  \begin{align*}
    \int \phi_i \,\red{f} = M \red{c} = V \blue{w}
  \end{align*}
  \end{frame}
  %
  % Speak: To get particle weights from a finite element field, we apply the pseudo-inverse of V.
  %        We need the pseudo-inverse because V is likely not square.
  %        Note that is we use a DG basis for F, this pseudo-inverse will be block-diagonal and thus efficiently calculated.
  %        Hirovicki and Kraus take advantage of this in theor papers on GEMPIC for plasma physics.
  \begin{frame}{Linear Algebra}\huge
  Particle weights from FEM field:
  \begin{align*}
    \blue{w} = V^+ M \red{c}
  \end{align*}

  \bigskip

  \begin{center}\LARGE
    Pseudo-inverse is block-diagonal\\with a DG basis.
  \end{center}
  \end{frame}
  %
  \begin{frame}{Scaling}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{figures/Swarm/ProjectionSimplicialWeakScaling.png}
    \includegraphics[width=0.48\textwidth]{figures/Swarm/ProjectionTensorWeakScaling.png}
  \end{center}
  \end{frame}
  %
  % Speak:
  \begin{frame}{Embedding}{Two Stream Instability}\LARGE
  \begin{center}
    PETSc TS tutorials/hamiltonian/ex2\\
    \bigskip
    \includegraphics[width=0.45\textwidth]{figures/Plasma/2stream_rollup.png}\hfill
    \includegraphics[width=0.45\textwidth]{figures/Plasma/2stream_rollup2.png}
  \end{center}
  \end{frame}
  %
  % Speak: With the library design (does not take main, not output by default, not have fixed parallelism, does propagate errors),
  %        users can employ the PIC operations directly, such as conservative projection,
  %        or use a whole PIC simulation as a subcomponent of a larger computation.
  %        In fact, all the results shown here can be run by the audience
  \begin{frame}{Embedding}\LARGE

  All tests may be reproduced:
  \smallskip
  \begin{itemize}
    \item Install PETSc \magenta{\href{https://www.mcs.anl.gov/petsc/documentation/installation.html}{www.mcs.anl.gov/petsc}}
  \end{itemize}
  \begin{itemize}
    \item[] {\small \texttt{cd \$PETSC\_DIR}}
    \item[] {\small \texttt{make -f ./gmakefile test}\\ \vspace*{-10pt}\qquad\texttt{search="ts\_tutorials\_hamiltonian-ex2*"}}
  \end{itemize}
  \begin{itemize}
    \item[] {\small \texttt{cd src/ts/tutorials/hamiltonian}}
    \vspace*{-5pt}
    \item[] {\small \texttt{make ex2}}
  \end{itemize}
  \begin{itemize}
    \item Test options at the end of the source file
  \end{itemize}
  \end{frame}
%
  %\subsection{GMG and Patch Solvers}
  %\parencite{farrellknepleywechsungmitchell2020}
  % Landau Damping: \parencite{finnknepleypusztayadams2023,finnthesis2023}
  % Landau collisions operator in the continuum \parencite{adamsbrennanknepleywang2022}
  % MHD \parencite{adamsknepley2023}
%
%
\section*{Conclusion}
%
\begin{frame}{My Focus}
\Huge
\visible<2->{Efficient,}\\
\quad \visible<3->{Scalable,}\\
\quad\quad \blue{Solvers}\\
\quad\quad\quad \visible<4->{for Multiscale,}\\
\quad\quad\quad\quad \visible<5->{Multiphysics}\\
\quad\quad\quad\quad\quad \visible<5->{Problems}
\end{frame}
%
% Speak:
\begin{frame}{My Focus}\huge
Ingredients for GEMPIC:
\medskip
\begin{itemize}
  \item Symplectic integrator
  \smallskip
  \item Conservative Projection
  \smallskip
  \item Continuous E-field
  \smallskip
  \item Entropic Integrator
  \smallskip
  \item Collision Operator
\end{itemize}
\end{frame}
% Speak:
\begin{frame}{Abstraction}\huge
We can dynamically select:
\medskip
\begin{itemize}
  \item FEM discretization
  \bigskip
  \item Particle layout
  \bigskip
  \item Background mesh
  \bigskip
  \item Poisson solver
  \bigskip
  \item Projection solver
  \bigskip
  \item Time integrators
\end{itemize}
\end{frame}
%
\begin{frame}[allowframebreaks]{References}
  \printbibliography
\end{frame}
%
\end{document}
