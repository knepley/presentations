\documentclass{beamer}

\input{tex/talkPreamble.tex}

% Reduce space above equations
\setlength\abovedisplayskip{0pt minus 30pt}

\title[PyLith]{Solvers in PyLith}
\author[M.~Knepley]{Brad Aagaard, Matthew~Knepley${}^*$, Charles Williams}
\date[CDM '11]{Crustal Deformation Modeling Tutorial\\Cyberspace, Jun. 19--24, 2011}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  Computation Institute\\
  University of Chicago

  \smallskip

  Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}

\subject{GPU Computation}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\begin{document}
\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}
%
\begin{frame}{How Do We Solve a System of Linear Equations?}
For a set of linear equations,
\begin{center}
\begin{tikzpicture}[scale = 0.3,font=\fontsize{24}{24}\selectfont]
  % A
  \draw[ultra thick]  (0,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (10,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (5,5) node {$A$};
  % x
  \draw[ultra thick] (12,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (15,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (13.5,5) node {$x$};
  % =
  \draw (17,5) node {$=$};
  % b
  \draw[ultra thick] (19,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (22,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (20.5,5) node {$b$};
\end{tikzpicture}
\end{center}
we can get the solution $x$ using
\begin{itemize}
  \item Gaussian elimination (LU)

  \item QR factorization and backsolve (QR)

  \item SVD factorization (SVD)
\end{itemize}
However, these methods use lots of memory and time.
\end{frame}
%
\begin{frame}[fragile]{Always start with LU}

For any new problem, always begin with a small, serial version which can be solved by LU factorization using the options below:
\begin{verbatim}
  --petsc.ksp_type preonly --petsc.pc_type lu
\end{verbatim}
This eliminates the solver as a variable, so that non-convergent or inaccurate solutions do not complicate
interpretation of the model. You can move to slightly larger and parallel problems by adding
\begin{verbatim}
  --petsc.pc_factor_mat_solver_package mumps
\end{verbatim}

\end{frame}
%
\begin{frame}{How Do We Solve a System of Linear Equations?}
We can reduce the problem size using \blue{subspace projection}, where $V$ is the basis for a small subspace. We solve a
\red{small} system, $V^T A V$, and then project that solution into the full solution space.
\begin{center}
\vskip -10pt
\begin{tikzpicture}[scale = 0.25,font=\fontsize{20}{20}\selectfont]
  % V^T
  \draw[ultra thick] (-12,5) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick]  (-2,5) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-7,7.5) node {$V^T$};
  % A
  \draw[ultra thick]  (0,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (10,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (5,5) node {$A$};
  % V
  \draw[ultra thick] (12,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (17,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (14.5,5) node {$V$};
  % y
  \draw[ultra thick] (19,5) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (22,5) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (20.5,7.5) node {$y$};
  % =
  \draw (24,7.5) node {$=$};
  % V^T
  \draw[ultra thick] (26,5) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (31,5) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (28.5,7.5) node {$V^T$};
  % b
  \draw[ultra thick] (33,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (36,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (34.5,5) node {$b$};

  % x
  \draw[ultra thick] (19,-12) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (22,-12) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (20.5,-7) node {$x$};
  % =
  \draw (24,-7) node {$=$};
  % V
  \draw[ultra thick] (26,-12) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (31,-12) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (28.5,-7) node {$V$};
  % y
  \draw[ultra thick] (33,-7) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (36,-7) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (34.5,-4.5) node {$y$};
\end{tikzpicture}
\end{center}
\end{frame}
%
\begin{frame}{What is a Krylov Method?}
A \blue{Krylov Method} uses matrix vector products with the system matrix $A$ to generate a subspace $V$ in which the
problem is solved.
{\Large
\begin{center}
\vskip -20pt
\begin{equation*}
  V = \mathrm{span}\left\{ b, Ab, A^2b, \ldots, A^{k-1} b\right\}
\end{equation*}
\end{center}
}

\bigskip

\red{GMRES} is a Krylov method which generates an approximate solution $\hat x$ that minimizes the residual, $||b - A\hat
x||$, over all choices $\hat x \in V$.
\end{frame}
%
\begin{frame}{What is a Krylov Method?}
Krylov methods are not robust solvers. They are generally used to accelerate other methods, called
\blue{preconditioners},
\begin{itemize}
  \item Relaxation (Jacobi, Gauss-Seidel, Successive Over-Relaxation)

  \item Additive Schwarz Method and Block-Jacobi

  \item Multigrid (MG)

  \item Incomplete Factorization (Cholesky, LU)
\end{itemize}
which can stagnate. Thus Krylov solvers are used to make existing solvers more robust.
\end{frame}
%
\begin{frame}{What are Schwarz Methods?}

Schwarz Methods, a particular kind of Domain Decomposition, divide the domain into overlapping subdomains and repeatedly
solve the equations over each subdomain. In terms of linear algebra, using only two subdomains, we have
\begin{center}
\begin{tikzpicture}[scale = 0.3,font=\fontsize{12}{12}\selectfont]
  % Background matrix
  \draw[ultra thick,draw=black!50!white]  (0,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick,draw=black!50!white] (10,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  % 0 block
  \draw[ultra thick]  (0,4) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,6) -- ++( 0.5,0);
  \draw[ultra thick]  (6,4) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,6) -- ++(-0.5,0);
  \draw (3,7) node {$R^T_0 A R_0$};
  % 1 block
  \draw[ultra thick]  (4,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,6) -- ++( 0.5,0);
  \draw[ultra thick] (10,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,6) -- ++(-0.5,0);
  \draw (7,3) node {$R^T_1 A R_1$};
  % x_0
  \draw[ultra thick] (12,4) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,6) -- ++( 0.5,0);
  \draw[ultra thick] (15,4) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,6) -- ++(-0.5,0);
  \draw (13.5,7) node {$x_0$};
  % x_1
  \draw[ultra thick] (12,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,6) -- ++( 0.5,0);
  \draw[ultra thick] (15,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,6) -- ++(-0.5,0);
  \draw (13.5,3) node {$x_1$};
  % =
  \draw (17,5) node {$=$};
  % x_0
  \draw[ultra thick] (19,4) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,6) -- ++( 0.5,0);
  \draw[ultra thick] (22,4) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,6) -- ++(-0.5,0);
  \draw (20.5,7) node {$R^T_0 b$};
  % x_1
  \draw[ultra thick] (19,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,6) -- ++( 0.5,0);
  \draw[ultra thick] (22,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,6) -- ++(-0.5,0);
  \draw (20.5,3) node {$R^T_1 b$};
\end{tikzpicture}
\end{center}
The solution is repeatedly updated using $x = x_0 + x_1$. The Block-Jacobi method is the degenerate case where we have
no overlap between subdomains.
\end{frame}
%

% Why might these be inadequate? Elliptic equations need global information.
\begin{frame}{Why are these Methods Inadequate?}

The traditional preconditioners we have described perform poorly for PyLith. They are designed for\\
\smallskip
\red{scalar} problems, \\
\smallskip
which model a \red{single physics}, \\
\smallskip
and are dominated by \red{local interactions}.\\
\smallskip
These three limitations must be overcome to achieve efficient, scalable solutions in PyLith.

\end{frame}
%
% What is Two-level DD?
%
\begin{frame}{Why is PyLith Nonlocal?}

Elliptic equations, the kind we solve for the quasistatic problem in PyLith, have nonlocal behavior. For example, a
\blue{small perturbation} to a boundary condition changes not just the solution around that part of the boundary, but the
\red{entire} solution. Prototypical elliptic equations are electrostatics
\begin{equation*}
  \Delta \phi = -\rho
\end{equation*}
and linear elasticity
\begin{equation*}
  \nabla \cdot \left( \nabla {\bf u} + \nabla^T {\bf u} \right) = -{\bf f}
\end{equation*}

\end{frame}
\begin{frame}{What is Multigrid?}
Multigrid uses the traditional preconditioners to solve problems on a series of \blue{coarse grids}. Since coarse grids
are small, they can be solved much faster than the original problem. The coarse grid solutions provide \blue{global
  information}, which is used to speed up the original solve. However, this requires knowledge of the user's mesh, and
the ability to coarsen it.
\begin{center}
  \includegraphics[width=4.8in]{figures/Coarsening/fichera_hierarchy_2.png}
\end{center}
\end{frame}
%
\begin{frame}{What is Algebraic Multigrid?}
Algebraic Multigrid (AMG) also constructs coarse problems in order to generate global information. However, it does not use
coarse meshes. Instead, AMG builds \blue{coarse linear systems} using only information from the original system. Thus,
it can be applied to any problem, although it is only effective on certain classes of problems.

\bigskip

In PyLith, we use AMG to solve \red{single components} of the momentum balance equation using the ML package from
Sandia, which can be automatically installed and used through PETSc.
\end{frame}
%
\begin{frame}{What is PCFIELDSPLIT?}
The FieldSplit preconditioner in PETSc solves individual systems, such as different components of momentum balance, and
then combines these solves to produce the overall solution.
\begin{changemargin}{-0.50cm}{0cm}
\begin{tikzpicture}[scale = 0.25,font=\fontsize{10}{10}\selectfont]
  % u_x system
  \draw[ultra thick] (-30,12) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (-17,12) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-23.5,14.5) node {$(\lambda+2\mu) \frac{\partial^2}{\partial x^2} + \mu \frac{\partial^2}{\partial y^2}$};
  \draw[ultra thick] (-16,12) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (-14,12) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-15,14.5) node {$u$};
  \draw[draw=red] (0,13.5) .. controls (-1,17) and (-6,17) .. (-14,17) -- (-14,12) .. controls (-4,15) and (-1,10) .. (0,3.5) -- cycle;
  \draw (-12,14.5) node[anchor=west,font=\fontsize{7}{7}\selectfont] {$u$ component};
  % Solve arrow
  \draw[->,very thick] (-29,11.9) |- (-27.1,8.5);
  \draw (-29.6,8.5) node[anchor=north,font=\fontsize{7}{7}\selectfont] {then solve};
  % u_y system
  \draw[ultra thick] (-27,6) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (-14,6) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-20.5,8.5) node {$(\lambda+2\mu) \frac{\partial^2}{\partial y^2} + \mu \frac{\partial^2}{\partial x^2}$};
  \draw[ultra thick] (-13,6) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (-11,6) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-12,8.5) node {$v$};
  \draw[draw=green] (0,13.5) .. controls (-1,10) and (-6,9) .. (-11,11) -- (-11,6) .. controls (-6,8) and (-1,7) .. (0,3.5) -- cycle;
  \draw (-10.5,8.5) node[anchor=west,font=\fontsize{7}{7}\selectfont] {$v$ component};
  % Solve arrow
  \draw[->,very thick] (-26,5.9) |- (-24.1,2.5);
  \draw (-26.6,2.5) node[anchor=north,font=\fontsize{7}{7}\selectfont] {then solve};
  % Schur complement system
  \draw[ultra thick] (-24,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (-9,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-16.5,2.5) node {$R^T \left( \nabla\cdot\left(\nabla + \nabla^T\right) \right)^{-1} R$};
  \draw[ultra thick] (-8,0) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,5) -- ++( 0.5,0);
  \draw[ultra thick] (-6,0) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,5) -- ++(-0.5,0);
  \draw (-7,2.5) node {${\bf \lambda}$};
  \draw[draw=blue] (0,13.5) .. controls (-1,5) and (-3,4) .. (-6,5) -- (-6,0) .. controls (-1,0) and (-0.5,-2) .. (0,3.5) -- cycle;
  \draw (-6.3,2.5) node[anchor=west,font=\fontsize{7}{7}\selectfont] {$\lambda$ component};
  % A
  \draw[ultra thick] (0,3.5) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (12,3.5) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (4.5,11) node {$\nabla\cdot\left(\nabla + \nabla^T\right)$};
  \draw (9.5,11) node {$R$};
  \draw (4.5,6)  node {$R^T$};
  \draw (9.5,6)  node {$0$};
  \draw[help lines] (1,8.5) -- (11,8.5);
  \draw[help lines] (8.8,4.5) -- (8.8,12.5);
  % x
  \draw[ultra thick] (13,3.5) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (15,3.5) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (14,11) node {${\bf u}$};
  \draw (14,6) node {${\bf \lambda}$};
  % =
  \draw (16,8.5) node {$=$};
  % b
  \draw[ultra thick] (17,3.5) ++( 0.5,0.0) -- ++(-0.5,0) -- ++(0,10) -- ++( 0.5,0);
  \draw[ultra thick] (19,3.5) ++(-0.5,0.0) -- ++( 0.5,0) -- ++(0,10) -- ++(-0.5,0);
  \draw (18,11) node {${\bf f}$};
  \draw (18,6) node {${\bf d}$};
\end{tikzpicture}
\end{changemargin}
By combining traditional preconditioners, effective for simple scalar equations, we can construct a very
effective overall solver.
\end{frame}
%
\begin{frame}{What is Recommended for PyLith?}

Two examples, {\tt examples/3d/tet4/step02.cfg} and {\tt examples/3d/tet4/step04.cfg}, use the field-split solver.
\begin{table}\footnotesize
\begin{tabular}{llp{5cm}}
Property & Value & Description \\
\hline
fs\_pc\_type                       & field\_split   & Precondition fields separately \\
\multicolumn{2}{l}{fs\_pc\_fieldsplit\_real\_diagonal} & Use diagonal blocks from the true operator, rather than the preconditioner \\
fs\_pc\_fieldsplit\_type           & multiplicative & Apply each field preconditioning in sequence, which is stronger than all-at-once (additive) \\
fs\_fieldsplit\_n\_pc\_type        & ml             & Multilevel algebraic multigrid preconditioning using Trilinos/ML via PETSc, n=0,\ldots,N+1 \\
fs\_fieldsplit\_n\_ksp\_type       & jacobi         & Jacobi is sometimes the best option for fault preconditioning, however we often use ML, n=0,\ldots,N+1
\end{tabular}
\caption{PETSc Options which activate PCFIELDSPLIT}
\end{table}
\end{frame}
%
\begin{frame}{More Information}

{\Large
The \magenta{\href{http://www.geodynamics.org/cig/software/pylith/pylith_manual-1.6.0.pdf\#page=49}{PyLith Manual
Section on PETSc}} has a thorough discussion of PETSc solver options for PyLith, as well as default values.

\bigskip

The \magenta{\href{http://www.mcs.anl.gov/petsc}{PETSc Homepage}} contains a user manual, FAQ, webpage documentation for
each function, and hundreds of example codes.
}
\end{frame}

\end{document}
