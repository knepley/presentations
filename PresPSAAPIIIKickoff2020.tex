\documentclass[dvipsnames]{beamer}

\input{talkPreambleNoBeamer.tex}
\mode<presentation>
{
  \usetheme{Warsaw}
  \useoutertheme{infolines}
  \useinnertheme{rounded}

  \setbeamertemplate{headline}{}
  \setbeamertemplate{footline}{}
  \setbeamercovered{transparent}
}

\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}
\addbibresource{presentations.bib}

% Figure this out
\newcounter{ccount}
\newcounter{num2}
\newcounter{num_prev}

\title[CHREST]{CHREST: Exascale Computation}
\author[M.~Knepley]{Matthew~Knepley}
\date[PSAAPIII]{CHREST Kickoff, University at Buffalo\\Buffalo, NY \quad August 18-19, 2020}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{Rockets}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}
%
% SPEAK: At the lowest level, we will use vendor standard compilers, language extensions, and libraries to deliver optimum performance on new exascale architectures. It is important to use vendor recommended systems (languages and libraries) since performance on supported but de-emphasized systems can badly lag the recommended path, as is clearly shown by OpenCL. Moreover, vendor libraries must be tightly integrated in the build process so that users can build seamlessly across laptops, clusters, and supercomputers. The difficulty here is demonstrated by the CUDA libraries, which has frequent release cycles, version incompatibilities, and severe performance implications for misuse.
%
% SPEAK: The overarching objective is to efficiently use existing bandwidth, since most of our computations are bandwidth-limited, and to minimize latency. A prime consideration is what limits the turnaround time for our target computations. We must move computations to the right level in the computation and memory heirarchy. For example, small problems might belong on the CPU for turnaround time minimization. For node-aware communication, we have a custom implementation in PETSc, and will also support the RAPtor system from Bienz, Gropp, and Olson. These low-level optimization will benefit all parallel linear algebraic operations in the overlying solves.
% GPU-aware MPI https://devblogs.nvidia.com/introduction-cuda-aware-mpi/
%   Avoids CPU involvement (latency)
%   Avoids redundant memory copies (GPUDirect/RDMA)
%   This technology has been used previously in MPI to directly copy on-node messages
% Karl's Latency Plots https://docs.google.com/spreadsheets/d/1amFJIbpvs9oJcUc-WntsFHO_C0LE7xFJeor-oElt0LY/edit#gid=0
%   CPU-GPU is 2x GPU-GPU which 10x CPU-CPU
% Mail "Re: [petsc-dev] MatMult on Summit" Sept. 21st
%   Junchao does Summit STREAMS and good discussion
%
% SPEAK: FAS reduces bandwidth requirements since the full system Jacobian is not pulled through memory at each iteration. In addition, We can reduce synchronization by only evaluating the convergence criterion after a full multigrid cycle, rather than at each iteration of the V-cycle on Newton MG. Nonlinear preconditioning can substitute higher performance iterations, such as Anderson Acceleration and Nonlinear GMRES, for Newton iterates, and also enlarge the convergence basin of the iteration.
%   Consider using a graph from the DD Keynote
\begin{frame}\Huge
\begin{center}
  Exascale Plan
\end{center}
\end{frame}
%
\begin{frame}[t]{Exascale Challenge}\Huge

The main challenge is to run\\
\hskip1em the entire analysis pipeline\\
\hskip2em at scale.

\only<2->{\begin{textblock}{0.6}[0.0,0.0](0.05,0.60)\includegraphics[width=\textwidth]{figures/Rocket/LabBurn.pdf}\end{textblock}}
\only<3->{\begin{textblock}{0.8}[0.0,0.0](0.35,0.55)\includegraphics[width=\textwidth]{figures/Rocket/NASA_Peregrine.png}\end{textblock}}
\end{frame}
\begin{frame}{Exascale Plan}

{\Large A library compatibility layer will enable\\ \quad low-level optimizations:}
\begin{itemize}
  \item Support Vendor Standards {\scriptsize (CUDA, SYCL, HIP)}                     % Library compatiblity layer
  \item Leverage Vendor Libraries {\scriptsize (cuBLAS/cuSparse, ACML, MKL, Kokkos)} % Fully integrated build framework
  \item Node-aware Communication {\scriptsize (batch messages, use PDE structure)}
  \item GPU-aware Communication {\scriptsize (custom message pack/unpack)}
\end{itemize}

\bigskip

{\Large Composable abstractions will enable\\ \quad high-level optimizations:}
\begin{itemize}
  \item Develop library of ML primitives {\scriptsize (Isomap, MSVM, $k$-means, DNN surrogates)}
  \item Computation-Aware Partitioning {\scriptsize (for heterogeneous machines)}
  \item Robust solvers for multiphysics {\scriptsize (FAS, nonlinear elimination, bootstrap)}
  \item Optimization and Eigensolves {\scriptsize (one-shot multigrid, bootstrap, integrated design)}
\end{itemize}
\end{frame}
%
%
% SPEAK: Let me illustrate composability specifically in the context of numerics. When we write a routine for vector dot product, we ten to assume that any time I need a dot product, I can use that routine. I should not have to write many versions of it for each situation, and if there are specific specializations, they are handled automatically through a unified interface. This reuse has largely been achieved for Linear Algebra libraries (BLAS, PETSc, Hypre, Trilinos).
% SPEAK: The uniformity of interface allows us to easily swap components from one library for another, to optimize larger units such as assembly, and to construct higher level oeprations, such as interpolation, out of reusable parts.
% SPEAK: In fact, linear solvers also benefit from this reusability. Krylov solvers have become largely interchangable. For example, PETSc, Hypre, Trilinos, and LBNL (SuperLU) work together through common interfaces defined in the xSDK project.
% SPEAK: Nonlinear solvers build upon linear solvers, and we have had success reusing different linear solvers inside Newton's method. However, composing nonlinear solvers themselves, often called nonlinear preconditioning, is not widespread. We laid out a mathematical and computational framework for this in a 2015 article in SIAM Review, and PETSc allows general composition, but I am not aware of another package supporting this. These powerful nonlinear solvers will be key to scaling up our highly nonlinear PDE simulations.
% SPEAK: Eigensolvers leverage linear and nonlinear solvers to uncover properties of the operators. There has been recent success in unifying the interfaces, so that packages such as SLEPc, PRIMME, and Feast can be used interchangably. Leveraging our topological mesh interface, we are developing a multilevel eigensolver with James Brannick (Penn State) for elliptic operators. The eigensolver will be reused to optimize preconditioners, in particular to implement bootstrap multigrid.
% SPEAK: Optimization solvers will be a key component of work in this project. They are at the heart of our machine learning approach, for which we will be developing new library components specifically targeting exascale machines, of which MLSVM is a specific example. Whole optimization solves will be packaged up to serve as components in other solver, for instance to construct Reduce Order Models (ROMs) used as preconditioners and in nonlinear solves.
% SPEAK: In this project, we will combine our optimized assembly and linear solvers to construct and solve adjoint problems for scalable sensitivity analysis, and nonlinear and optimization solvers to build design of experiments engines.
%
% It seems that you are heavily reliant on libraries for performance; why do you believe that PETSc is an appropriate CS framework for the proposed work? (20 min.)
% PrepLibraryDev.tex
% SPEAK: MPI does for this for machines and networks
\begin{frame}\Huge
\begin{center}
  Software Plan
\end{center}
\end{frame}
%
\begin{frame}{SoftwarePlan}

\Huge
To build composable libraries,\\

\bigskip
\bigskip

\quad based on PETSc,\\

\bigskip
\bigskip

for the entire analysis pipeline.
\end{frame}
%
\begin{frame}{CCR Deployment}\LARGE

CCR scalable development pipeline enables:\\
\begin{itemize}\Large
  \item rapid prototyping on desktop,
  \medskip
  \item deployment on CCR resources,
  \medskip
  \item scaling up to national facilities.
\end{itemize}

\medskip

CCR provides early access to:\\
\begin{itemize}\Large
  \item XMS/XDMoD metrics service\\
        could be used for arch-aware partitioning
  \medskip
  \item ``Ookami'' ARM64-SVE testbed
\end{itemize}
\end{frame}
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Hardware Details

\only<2->{\begin{textblock}{0.25}[0.0,0.0](0.05,0.50)\includegraphics[width=\textwidth]{figures/Hardware/MacAir.jpg}\end{textblock}}
\only<3->{\begin{textblock}{0.25}[0.0,0.0](0.30,0.60)\includegraphics[width=\textwidth]{figures/Hardware/NvidiaC2070.jpeg}\end{textblock}}
\only<4>{\begin{textblock}{0.60}[0.0,0.0](0.58,0.55)\includegraphics[width=\textwidth]{figures/Hardware/ALCF-Theta_111016-1000px-225x153.jpg}\end{textblock}}

\end{frame}
% SPEAK: Matrix and Krylov classes hide architectural optimizations
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Hide\\
\hskip1em Implementation Complexity

\only<2->{\begin{textblock}{0.40}[0.0,0.0](0.05,0.45)\includegraphics[width=\textwidth]{figures/FEM/GPU/threadTranspose2.png}\end{textblock}}
\only<3->{\begin{textblock}{0.35}[0.0,0.0](0.80,0.45)\includegraphics[width=\textwidth]{figures/Mat/BLISLoops.pdf}\end{textblock}}
\only<4>{\begin{textblock}{0.85}[0.0,0.0](0.20,0.65)
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L0_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L1_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L2_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L3_EV1.png}
  \includegraphics[height=0.2\textwidth]{figures/MG/Bootstrap/L4_EV1.png}\end{textblock}}

\end{frame}
% SPEAK: Users implementing their own algorithms will not typically be aware of implementation nuances arrived at through decades of experience.
\begin{frame}[t]{Why Libraries?}\Huge

Libraries Disseminate\\
\hskip1em Project Advances

\bigskip
\bigskip

\begin{itemize}\Large
  \item<2->[] Validated Ablation and Droplet Burning Models
  \bigskip
  \item<3->[] Scalable Isomap and Physics-Based Encoders
  \bigskip
  \item<4->[] Scalable Nonlinear Solvers
\end{itemize}
\end{frame}
%
% SPEAK: Early Numerical Libraries
%    71 Handbook for Automatic Computation: Linear Algebra, J. H. Wilkinson and C. Reinch
%    73 EISPACK, Brian Smith et.al.
%    79 BLAS, Lawson, Hanson, Kincaid and Krogh
%    90 LAPACK, many contributors 91 PETSc, Gropp and Smith 95 MPICH, Gropp and Lusk
%       All of these packages had their genesis at Argonne National Laboratory/MCS
\begin{frame}{Why PETSc?}
\Large
\setbeamercovered{transparent}

\begin{itemize}
  \item<1-> Dependability \& Maintainability
  \visible<2>{\begin{itemize}
    \item Nearly 30 years of continuous development
  \end{itemize}}
  \item<3-> Portability \& Robustness
  \visible<4>{\begin{itemize}
    \item Tested at every supercomputer installation and 10,000+ users
  \end{itemize}}
  \item<5-> Performance \& Scalability
  \visible<6>{\begin{itemize}
    \item Many Gordon Bell Winners
  \end{itemize}}
  \item<7-> Optimality \& Robustness
  \visible<8>{\begin{itemize}
    \item State-of-the-Art Linear and Nonlinear Solvers, Eigensolvers, Optimization Solvers, Timesteppers
  \end{itemize}}
  \item<9-> Flexibility \& Extensibility
  \visible<10>{\begin{itemize}
    \item More than 8000 citations and hundreds of application packages
    \item Aerodynamics, Arterial Flow, Corrosion, Combustion, Data Mining, Earthquake Mechanics, \ldots
  \end{itemize}}
\end{itemize}
\end{frame}
\end{document}

%(Paper with Jed, https://www.mcs.anl.gov/papers/P5174-0814.pdf)
%  Select best parts and user on series of slides
%  "I'd like you to use my new web browser, Firetran! It renders HTML 10\% faster than Firefox, but only if there is no JavaScript. You can recompile if you want JavaScript, but our performance tests don’t show that configuration. The character encoding is compiled in, for efficiency. It has a great plugin community—developers add code directly to the web browser core, guarded by a #ifdef. Some developers change things and distribute their own mutually incompatible versions of Firetran. Naturally, users of those packages submit bug Reports to me, but I haven’t been able to reproduce with my version. Proxy configuration is compiled in so you don’t have to worry about run-time configuration dialogs, just edit a makefile and recompile. To keep you secure, the https version of Firetran cannot use http, and vice versa. Although Firetran is open source, our development is done in private; if you submit a bug report or a patch, you’ll likely hear “We fixed that in the private repository last year; we’ll release when the paper comes out. If you have to view that website, fill out the attached form and fax us a signed copy.” Firetran has a parental filter feature: you can list a maximum of 16 websites in a source file, in which case Firetran will refuse to visit any site not on the list. Firetran can be compiled only with last year’s version of the ACME Fortran77 compiler. The build system consists of csh, perl, m4, and BSD make. There is no URL entry box in Firetran; to visit a page, you edit a configuration file and run the program. A grad student wrote a Tcl script with a text entry box to automate editing the configuration file and rerunning the Firetran executable. The script is hard to understand, but many in the community believe the way forward is to enhance the script to detect whether the website needs https or http, JavaScript or not, etc., and recompile Firetran on the spot. Needless to say, Firetran struggles with market share."
