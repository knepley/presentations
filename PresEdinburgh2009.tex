\documentclass{beamer}

\input{tex/talkPreamble.tex}

\title[GPU]{Implications for Library Developers\\of GPU Hardware}
\author[M.~Knepley]{Matthew~Knepley\inst1${}^,$\inst2}
\date[NAIS]{Intelligent Software Workshop\\Edinburgh, Scotland\qquad October 20, 2009}
% - Use the \inst command if there are several affiliations
\institute[UC]{
  \inst1 Computation Institute\\
  University of Chicago

  \smallskip

  \inst2 Department of Molecular Biology and Physiology\\
  Rush University Medical Center
}

\subject{GPU Computation}

% Delete this, if you do not want the table of contents to pop up at the beginning of each subsection:
\AtBeginSection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[currentsection,hideothersubsections]
  \end{frame}
}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Outline}
    \tableofcontents[sectionstyle=show/hide,subsectionstyle=show/shaded/hide]
  \end{frame}
}

% If you wish to uncover everything in a step-wise fashion, uncomment the following command: 
%%\beamerdefaultoverlayspecification{<+->}

\begin{document}

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.3]{figures/logos/rush_logo.png}\hspace{1.0in}
  \includegraphics[scale=0.24]{figures/logos/uc-logo-official.jpg}
  \end{center}
  \vskip0.4in
\end{frame}

\begin{frame}<testing>
\frametitle{Main Idea}
Extensive use of GPU hardware will not only change the way library code is developed, but also the interaction between
developer and user. API will have to evolve, and I believe code generation will become much more useful. Users will have
to give up more control to developers, but in return they will benefit from both better performance and increased
complexity. Of course, we will not deny users the ability to ``do it themselves'', but this will now entail much wider
expertise.
\end{frame}
%
%
\section{Library Developers}
%
\begin{frame}{Biggest Changes}

\LARGE
\begin{center}
  \red{Multi-language programming} is necessary, for at least the near future

  \pause\bigskip

  Interfaces will have to be \red{fluid} as hardware changes rapidly.
\end{center}
\end{frame}
%
\subsection{Multiple Languages}
\begin{frame}{Build System}

\begin{itemize}
  \item Hardware detection during configure more difficult
  \begin{itemize}
    \item Need a community solution
  \end{itemize}

  \item New language (\magenta{\href{http://www.nvidia.com/object/cuda_home.html}{CUDA}}, \magenta{\href{http://domino.research.ibm.com/comm/research_projects.nsf/pages/cellcompiler.index.html}{Cell Broadband Engine}})
  \begin{itemize}
    \item Necessitates new compiler

    \item Source and library segregation

    \item Interaction issues with other languages/compilers/libraries

    \item There are some libraries (\magenta{\href{http://www.threadingbuildingblocks.org}{TBB}})
  \end{itemize}

  \item Still not clear how to multiplex over different approaches
  \begin{itemize}
    \item \magenta{\href{http://www.khronos.org/opencl}{OpenCL}} is far from mature, and future is uncertain

    \item \code{\#define} is not enough to cope with different underlying builds
  \end{itemize}
\end{itemize}

\begin{center}\small
  PETSc Configure System: \url{http://petsc.cs.iit.edu/petsc/BuildSystem}
\end{center}
\end{frame}
%
\begin{frame}{Interaction with MPI}

\only<1>{
  There are several possible models:
\begin{itemize}
  \item One process controls a single GPU
  \begin{itemize}
    \item No extra work
  \end{itemize}

  \item One process controls several GPUs
  \begin{itemize}
    \item Need allocation strategy for kernels (multiple queues)
  \end{itemize}

  \item Several processes control one GPU
  \begin{itemize}
    \item Need standard locking mechanism
  \end{itemize}

  \item Several processes control several GPUs
  \begin{itemize}
    \item Just a combination of above, harder to optimize
  \end{itemize}
\end{itemize}
}
\only<2>{
Do not anticipate GPU-to-GPU communication:
\begin{itemize}
  \item At least not in the short term

  \item Requires hardware and/or OS changes
\end{itemize}

\bigskip

Partitioning will become more involved:
\begin{itemize}
  \item Multilevel
  \begin{itemize}
    \item MPI Processes

    \item Multicore Threads
  \end{itemize}

  \item Weighted
  \begin{itemize}
    \item Different processing speeds

    \item Different memory bandwidth
  \end{itemize}
\end{itemize}
}
\end{frame}
%
\begin{frame}{Performance and Memory Logging}

\begin{itemize}
  \item On CPU can use standard packages
  \begin{itemize}
    \item \magenta{\href{http://www.cs.utah.edu/dept/old/texinfo/as/gprof_toc.html}{gprof}}, \magenta{\href{http://www.cs.uoregon.edu/research/tau}{TAU}}, \magenta{\href{http://icl.cs.utk.edu/papi}{PAPI}}

    \item PETSc defines an extensible logging system (\magenta{\href{http://www.mcs.anl.gov/petsc/petsc-as/snapshots/petsc-current/docs/manualpages/Profiling/PetscLogStagePush.html}{stages}})
  \end{itemize}

  \medskip

  \item For kernel, count manually
  \begin{itemize}
    \item Might use source analysis on kernel

    \item Hardware counters need better interface
  \end{itemize}

  \medskip

  \item Need better modeling
  \begin{itemize}
    \item Very large number of interacting threads
  \end{itemize}
\end{itemize}
\end{frame}
%
\input{slides/Model/Importance.tex}
%
\subsection{Changing Interfaces}
\input{slides/ScientificComputing/RobustDevelopment.tex}
\input{slides/GPU/Analytics/TestMethodology.tex}
%
%
\section{Developer--User Interaction}
%
\begin{frame}{What Will Change?}

\begin{center}\Large
  More control will pass from user to library/compiler
\end{center}
\begin{itemize}
  \item<2-> Kernels will be generated by the library
  \begin{itemize}
    \item[Ex]<3-> Autogenerated FEM integration
  \end{itemize}

  \item<4-> Partitioning will be controlled by the library
  \begin{itemize}
    \item[Ex]<5-> Partition for MPI and then for GPU
  \end{itemize}

  \item<6-> Communication will be managed by the library
  \begin{itemize}
    \item[Ex]<7-> Marshalling to GPU
  \end{itemize}

  \item<8-> Assembly will be controlled by the algorithm
  \begin{itemize}
    \item[Ex]<9-> Substructuring (\code{PCFieldSplit})
  \end{itemize}
\end{itemize}
\end{frame}
%
\subsection{API Changes}
%
\input{slides/Sieve/GlobalAndLocal.tex}
\input{slides/Sieve/HierarchicalInterface.tex}
\input{slides/DA/Vectors.tex}
\input{slides/DA/GhostValues.tex}
\input{slides/DA/GlobalNumberings.tex}
\input{slides/DA/LocalNumbering.tex}
\input{slides/DA/LocalFunction.tex}
\input{slides/DA/BratuResidual.tex}
\input{slides/DA/LocalJacobian.tex}
\input{slides/DA/BratuJacobian.tex}
\input{slides/DA/UpdatingGhosts.tex}
\input{slides/Sieve/HierarchicalInterfaceII.tex}
%
\begin{frame}{GPU Interaction}
\begin{center}\Large
  Analytic routines become GPU \red{kernels}.
\end{center}

  Kernels can be
\begin{itemize}
  \item FD Stencils

  \item FEM and FV Integrals

  \item Domain Cells for Integral Equations
\end{itemize}

\medskip

  Storage can be reached by appropriate \code{restrict()} call
\begin{itemize}
  \item Usually includes the closure

  \item Building block for marshalling
\end{itemize}
\end{frame}
%
\input{slides/GPU/GeneralProgramming.tex}
\input{slides/GPU/Locality.tex}
\input{slides/MultiField/LocalEvaluation.tex}
\input{slides/MultiField/Preconditioning.tex}
%
\subsection{Code Generation}
%
\input{slides/FIAT/FIAT.tex}
\input{slides/FIAT/PETScIntegration.tex}
\input{slides/FFC/FFC.tex}
%
%
\section*{Conclusions}
\begin{frame}{What Is Most Important?}

\Large
\begin{itemize}
  \item<2-> Multiple Languages will be Necessary
  \begin{itemize}
    \item \large Build systems need the most work
  \end{itemize}

  \bigskip

  \item<3-> Users will give up more Control
  \begin{itemize}
    \item \large Move toward a hierarchical paradigm
  \end{itemize}
\end{itemize}

\bigskip

\begin{center}
\visible<4>{\emph{Change alone is unchanging\\--- Heraclitus, 544--483 \sc{BC}}}
\end{center}

\end{frame}

\end{document}
