\documentclass[dvipsnames]{beamer}

\input{tex/talkPreamble.tex}
\setbeamercovered{invisible}

% Bibliography
%   http://tex.stackexchange.com/questions/12806/guidelines-for-customizing-biblatex-styles
\usepackage[sorting=none, backend=biber, style=authoryear, maxbibnames=20]{biblatex}
\addbibresource{petsc.bib}
\addbibresource{petscapp.bib}
\addbibresource{presentations.bib}

% Figure this out
\newcounter{ccount}
\newcounter{num2}
\newcounter{num_prev}

\title[SLIC]{The Impact of Solvers on Modeling:\\The Solvation-Layer Interface Condition}
\author[M.~Knepley]{Matthew~Knepley and Jaydeep Bardhan}
\date[PP2020]{SIAM Parallel Processing 2020\\Seattle, WA \quad February 12th, 2020}
% - Use the \inst command if there are several affiliations
\institute[Buffalo]{
  Computer Science and Engineering\\
  University at Buffalo
}
\subject{PETSc}

\begin{document}
\beamertemplatenavigationsymbolsempty

\begin{frame}
  \titlepage
  \begin{center}
  \includegraphics[scale=0.12]{figures/logos/anl-white-background-modern.jpg}\hspace{1.0in}
  \includegraphics[scale=0.05]{figures/logos/UB_Primary.png}
  \end{center}
\end{frame}
%
\begin{frame}<testing>{Abstract}\small

\end{frame}
%
\input{slides/Collaborators/BEM.tex}
%
% SPEAK: People hem in the models they will consider based upon what they think they can solve.
\begin{frame}{Main Question}
  \begin{center}\Huge
    How do available solvers\\
    influence our choice of model?
  \end{center}
\end{frame}
%
\begin{frame}{Nonlinear BEM}\Huge

  \begin{center}
  Nonlinear BEM is powerful,\\
  \medskip
  but neglected because\\
  \medskip
  it requires specialized solvers.
  \end{center}

\end{frame}
%
%
\section{Modeling}
%
\input{slides/Bioelectrostatics/Lysozyme.tex}
\input{slides/Bioelectrostatics/ContinuumModel.tex}
\input{slides/Bioelectrostatics/SecondKindModel.tex}
%
\begin{frame}{Biomolecular Modeling}{Problems}
\begin{center}\Huge
  This model is inaccurate for solvation energy.
\end{center}

\pause
\bigskip

\begin{center}\LARGE
  Thus practitioners adjust atomic radii to fit full atomistic simulation energies.
\end{center}
\end{frame}
%
% SPEAK: Does not seem to make sense, since you solve the problem already to get that radius
\begin{frame}{Biomolecular Modeling}{Problems}\Large

However, a given atom
\begin{itemize}
  \item<1-> can have two different radii in different molecules
  \bigskip
  \item<2-> can have two different radii in the \blue{same} molecule
  \bigskip
  \item<3-> has a solvent-dependent radius
  \bigskip
  \item<4-> has a temperature-dependent radius
\end{itemize}
\visible<5>{
\begin{center}
  For example, the volume of a carbon atom can vary by \red{50\%} in a single molecule.
\end{center}}
\end{frame}
%
% SPEAK: It does not appear that our model revels the physics, since its generalizability is very poor.
\begin{frame}{Biomolecular Modeling}{Problems}\Large

Why is this a bad model?
\begin{itemize}
  \item<1-> Ignores experimental data (crystal radii)
  \bigskip
  \item<2-> Not robust to solute/solvent/temperature changes
  \bigskip
  \item<3-> Misses sensitivity to local electrostatic conditions
  \bigskip
  \item<4-> Gives nonsense for the entropy
\end{itemize}
\end{frame}
%
\input{slides/Bioelectrostatics/OriginElectrostaticAsymmetry.tex}
% SPEAK: Talk about Maxwell BC, then modified BC
\input{slides/Bioelectrostatics/SLICIdea.tex}
\input{slides/Bioelectrostatics/SLICModel.tex}
%
%
\section{Solvers}
\begin{frame}{Solving SLIC}{Sphere}
\includegraphics[width=\textwidth]{figures/BEM/SLICConvSphere.pdf}
\end{frame}
%
% SPEAK: Picard requires breaking the residual/Jacobian into pieces, which is hard to handle in a library.
\begin{frame}[fragile]{Nonlinear Solvers}\Large

\begin{overprint}
If we are solving
\onslide<1-4,8-12>
\begin{align*}
  F(u) = b
\end{align*}
\onslide<5>
\begin{align*}
  A u + N(u) u = b
\end{align*}
\onslide<6-7>
\begin{align*}
  A u + \mathrm{diag}(h(u)) u = b
\end{align*}
\end{overprint}

\bigskip
\bigskip

\begin{overprint}
\onslide<1>
with Richardson's Method
\begin{align*}
  u_{n+1} = u_n + \lambda \left( F(u_n) - b \right)
\end{align*}
\onslide<2>
with Richardson's Method \large
\begin{verbatim}
  -snes_type nrichardson
  -snes_linesearch_type l2
  -snes_linesearch_damping 0.05
\end{verbatim}
\onslide<3>
with Newton's Method
\begin{align*}
  u_{n+1} = u_n + J^{-1}(u_n) \left( F(u_n) - b \right)
\end{align*}
\onslide<4>
with Newton's Method \large
\begin{verbatim}
  -snes_type newtonls
  -snes_linesearch_type basic
\end{verbatim}
\onslide<5-6>
with Picard's Method
\begin{align*}
  \left( A + \mathrm{diag}(h(u_n)) \right) u_{n+1} = b
\end{align*}
\onslide<7>
with Picard's Method
\begin{align*}
  J   &= A + \mathrm{diag}(h(u_n)) + \mathrm{diag}(h'(u_n)) K' \\
  J_P &= A + \mathrm{diag}(h(u_n))
\end{align*}
\onslide<8>
with Generalized Broyden Method
\begin{align*}
  u_{n+1}  &= u_n + \beta \left( F(u_n) - b \right) - \left( \mathbb{X}_k + \beta F(u_k) \right) \gamma_k \\
  \gamma_i &= (\F^T(u_k) \F(u_k))^{-1} \F^T(u_i) \left( F(u_i) - b \right)
\end{align*}
\onslide<9>
with Generalized Broyden Method \large
\begin{verbatim}
  -snes_type ngmres
  -snes_linesearch_type l2
\end{verbatim}
\onslide<10>
with Newton $-_{R}$ Generalized Broyden Method
\begin{align*}
  y       &= \mathcal{N}(F(\mathcal{GB}(F, \cdot, b)), x_n, b) \\
  x_{n+1} &= \mathcal{GB}(F, y, b)
\end{align*}
\onslide<11>
with Newton $-_{R}$ Generalized Broyden Method
\begin{align*}
       u' &= \mathcal{GB}(F, u_n, b) \\
  u_{n+1} &= u' + J^{-1}(u') \left( F(u') - b \right)
\end{align*}
\onslide<12>
with Newton $-_{R}$ Generalized Broyden Method \large
\begin{verbatim}
  -snes_type newtonls
  -snes_linesearch_type basic
  -npc_snes_type ngmres
  -npc_snes_max_it 2
\end{verbatim}
\end{overprint}
\end{frame}
%
% SPEAK: Richardson was divergent, but Anderson works. This shows the power of subspace correction over line search.
% TODO: Plot Newton, Picard, and Newton-NGMRES with linear iterates on x
\begin{frame}{Solving SLIC}{Sphere}
\includegraphics[width=\textwidth]{figures/BEM/SLICConvSphere.pdf}
\end{frame}
%
\begin{frame}{Solving SLIC}{Sphere}
\includegraphics[width=\textwidth]{figures/BEM/SLICConvSphereLinearIts.pdf}
\end{frame}
%
\begin{frame}{ARG}
\begin{center}
  \includegraphics[width=\textwidth]{figures/Bioelectrostatics/ArginineCharge.png}
\end{center}
\end{frame}
%
\begin{frame}{Solving SLIC}{Arginine}
\includegraphics[width=\textwidth]{figures/BEM/SLICConvArginine.pdf}
\end{frame}
%
\begin{frame}{Solving SLIC}{Arginine}
\includegraphics[width=\textwidth]{figures/BEM/SLICConvArginineNPC.pdf}
\end{frame}
%
\begin{frame}{Solving SLIC}{Arginine}
\includegraphics[width=\textwidth]{figures/BEM/SLICConvArginineLinearIts.pdf}
\end{frame}
%
%
\section{Conclusions}
\begin{frame}{Main Conclusion}\Huge
\begin{center}
  Nonlinear Preconditioning\\
  \bigskip
  can lead to significant speedups,\\
  \bigskip
  and is accessible without recoding.
\end{center}
\end{frame}
%
\begin{frame}{Charge Distribution}{Maxwell}
\includegraphics[width=\textwidth]{figures/Bioelectrostatics/ArginineChargeMaxwell.png}
\end{frame}
%
\begin{frame}{Charge Distribution}{SLIC}
\includegraphics[width=\textwidth]{figures/Bioelectrostatics/ArginineChargeSLIC.png}
\end{frame}
%
\begin{frame}{Charge Distribution}{Difference}
% Made with Programmable Filter (highlight both plots when creating):
% sigma_0 = inputs[0].PointData['Reaction_Surface_Charge_PetscFE_0x84000000_3']
% sigma_1 = inputs[1].PointData['Reaction_Surface_Charge_PetscFE_0x84000000_3']
% output.PointData.append(sigma_1 - sigma_0, 'DeltaSigma')
\includegraphics[width=\textwidth]{figures/Bioelectrostatics/ArginineDeltaCharge.png}
\end{frame}
\begin{frame}{Charge Distribution}{Difference (Rescaled)}
\includegraphics[width=\textwidth]{figures/Bioelectrostatics/ArginineDeltaChargeRescale.png}
\end{frame}
%
\begin{frame}{Future Work}\LARGE

\begin{itemize}
  \item Cathodic Protection/Corrosion Prevention

  \bigskip

  \item Homogenized boundary conditions

  \bigskip

  \item Models of the hydrophobic interaction
\end{itemize}
\end{frame}
%
\begin{frame}[plain]

\begin{center}
\Huge\bf Thank You!
\end{center}

\bigskip

\begin{center}
\LARGE \magenta{\href{http://cse.buffalo.edu/~knepley}{http://cse.buffalo.edu/\textasciitilde knepley}}
\end{center}
\end{frame}

\end{document}
