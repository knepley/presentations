\documentclass[11pt]{article}

\usepackage{hyperref}
\newcommand{\itemskip}{\vspace*{8pt}}
\usepackage[tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in]{geometry}
\usepackage[english]{isodate}\isodate
%\usepackage{fullpage}
\usepackage{url}
\usepackage{textcomp}
% Fonts
\usepackage[T1]{fontenc}
%\usepackage[urw-garamond]{mathdesign}

\newenvironment{packedDescription}{
\begin{description}
  \setlength{\itemsep}{1pt}
  \setlength{\parskip}{0pt}
  \setlength{\parsep}{0pt}
}{\end{description}}

%% Define a new style for the url package that will use a smaller font.
\makeatletter
\def\url@leostyle{%
  \@ifundefined{selectfont}{\def\UrlFont{\sf}}{\def\UrlFont{\footnotesize\sffamily}}}
\makeatother
%% Now actually use the newly defined style.
\urlstyle{leo}

\usepackage[resetlabels]{multibib}
\newcites{relevant,extra}{Publications,Other Significant Publications}

% Set your name here
\def\name{Matthew G. Knepley}

% The following metadata will show up in the PDF properties
\hypersetup{
  colorlinks = true,
  urlcolor = blue,
  pdfauthor = {\name},
  pdfkeywords = {computational science, partial differential equations, finite element methods, incompressible flow, non-Newtonian flow, code generation},
  pdftitle = {\name: Curriculum Vitae},
  pdfsubject = {Curriculum Vitae},
  pdfpagemode = UseNone
}

% Custom section fonts
\usepackage{sectsty}
\sectionfont{\rmfamily\mdseries\large}
\subsectionfont{\rmfamily\mdseries\itshape\normalsize}

\newcommand\ptitle[1]{\textit{#1}} % Formatting for paper titles
\usepackage{fancyvrb}
\newcommand\doi[1][]{\SaveVerb[%
    aftersave={\textnormal{doi:\UseVerb[#1]{vsave}}}]{vsave}}

% Don't indent paragraphs.
\setlength\parindent{0em}

% Make lists without bullets and compact spacing
\renewenvironment{itemize}{
  \begin{list}{}{
    \setlength{\leftmargin}{1.5em}
    \setlength{\itemsep}{0.25em}
    \setlength{\parskip}{0pt}
    \setlength{\parsep}{0.25em}
  }
}{
  \end{list}
}

%\topmargin-0.5in
%%\headheight0.0in
%\textheight9in
%\oddsidemargin-0.25in
%\textwidth7.0in

\begin{document}

{\large
\centerline{\large\bf Biographical Sketch Matthew G. Knepley}
}

\itemskip\noindent
{\bf Address}
\begin{packedDescription}
  \item Department of Computer Science and Engineering, University at Buffalo
  \item 211A Capen Hall, Box 602000, Buffalo, NY 14260-2500
  \item Phone: (716) 645-0747
  \item Email: \url{knepley@buffalo.edu}
  \item Web: \url{https://www.cse.buffalo.edu/\~knepley}
\end{packedDescription}

\itemskip\noindent
{\bf Education and Training}
\begin{packedDescription}
  \item Case Western Reserve University, Mathematical Physics, Cleveland, OH, B.S. 1994
  \item University of Minnesota, Computer Science, Minneapolis, MN, M.S. 1996
  \item Purdue University, Computer Science, West Lafayette, IN, Ph.D. 2000
  \item Argonne National Laboratory, MCS, Lemont, IL, 2001 -- 2004
\end{packedDescription}

\itemskip\noindent
{\bf Research and Professional Experience}
\begin{packedDescription}
  \item Professor, Dept. of Comp. Sci. \& Eng., University at Buffalo, 2024 -- \textit{Present}
  \item Associate Professor, Dept. of Comp. Sci. \& Eng., University at Buffalo, 2017 -- 2024
  \item Assistant Professor, Dept. of Comp. \& App. Math., Rice University, 2015 -- 2017
  \item Senior Res. Assoc., Computation Institute, University of Chicago, 2009 -- 2015
  \item Fellow, Computation Institute, University of Chicago, 2008--2015
  \item Visting Asst. Prof., Dept. of Mol. Bio. and Phys., Rush Uni. Medical Center, 2006 -- 2014
  \item Adj. Sen. Res. Fellow, Dept. of Mathematics, Monash University, 2010 -- 2013
  \item Asst. Comp. Math., MCS Division, Argonne National Laboratory, 2005 -- 2009
  \item Postdoctoral Researcher, Mathematics and Computer Science, Argonne Nat. Lab., 2001--2004
  \item Research Scientist, Distributed Data Collection, Akamai Technologies Inc., 2000 -- 2001
\end{packedDescription}

\nociterelevant{
  FarrellKnepleyWechsungMitchell2020,
  HaplaKnepleyAfanasievBoehmDrielKrischerFichtner2020,
  ChangFabienKnepleyMills2018,
  MaySananRuppKnepleySmith2016,
  bruneknepleysmithtu15}
\bibliographystylerelevant{siamplain}
\bibliographyrelevant{petsc}

\nociteextra{
  AfanasievBoehmDrielKrischerRietmannMayKnepleyFichtner2019,
  ThompsonRiviereKnepley2018,
  LangeMitchellKnepleyGorman2015,
  AagaardKnepleyWilliams13,
  petsc-user-ref}
\bibliographystyleextra{siamplain}
\bibliographyextra{petsc}

\itemskip\noindent
{\bf Synergistic Activities}
\begin{packedDescription}
\item Former Director, Rice \href{https://software.intel.com/en-us/articles/intel-parallel-computing-center-at-rice-university}{Intel Parallel Computing Center}
\item Served on \href{https://geodynamics.org/cig/about/governance/}{Executive Committee} for NSF \href{https://geodynamics.org/}{CIG} project
\item Developer of \href{http://www.mcs.anl.gov/petsc}{PETSc}, \href{https://geodynamics.org/cig/software/pylith/}{PyLith}, \href{https://bitbucket.org/petfmm/petfmm-dev}{PetFMM}, and \href{https://github.com/barbagroup/petrbf}{PetRBF}
\end{packedDescription}

\itemskip\noindent
{\bf Collaborators (48 months)}
\begin{packedDescription}
\item Brad Aagaard (United States Geological Survey), Jaydeep P. Bardhan (Northeastern University), Nicolas Barral (Imperial College London), Amneet Bhalla (SDSU), Gautam Bisht (PNNL), Dylan Brennan (Princeton), C.-S. Chang (PPPL), Nathan Collier (ORNL), Christopher Cooper (UTFSM), Maurice Fabien (Brown University), Patrick Farrell (Oxford), Jennifer Frederick (SNL), Gerard Gorman (Imperial College London), Boyce Griffith (UNC), Laura Grigori (UPMC/INRIA), Robert Guy (UC Davis), Glenn Hammond (SNL), Eero Hirvijoki (PPPL), Margarete Jadamec (University at Buffalo), Satish Karra (LANL), Michael Lange (ECMWF), Dave May (Oxford University), Lawrence Mitchell (Durham University), Hannah Morgan (Argonne National Laboratory), Louis Moresi (ANU), Kalyana Nakshatrala (University of Houston), Beatrice Riviere (Rice University), Karl Rupp (Technical University of Vienna), Ravi Samtaney (KAUST), Patrick Sanan (Universit’a della Svizzera italiana), Olaf Schenk (USI Lugano), L. Ridgway Scott (University of Chicago), Andreas Stathopoulos (William and Mary), Amir Tabrizi (Northeastern University), Danish Tejani (Northeastern University), Xuemin Tu (University of Kansas), Richard Vuduc (Georgia Tech), Charles Williams (GNS Science, Inc.), Noah Wieckowski (Northeastern University), Hestaven, Jan (EPFL)
\end{packedDescription}

\itemskip\noindent
{\bf Thesis and Postdoctoral Advisors: Prof. Ahmed Sameh, Dr. Barry Smith}

\itemskip\noindent
{\bf Graduate Advisees }
\begin{packedDescription}
\item Peter Brune (Google, Inc.), Andy Terrel (REX, Inc.), Tom Klotz (Rice University), Eric Buras (Aptima, Inc.), Jeremy Tillay (Chevron), Jonas Actor (Rice University), Albert Cowie (University at Buffalo), Daniel Finn (University at Buffalo), Abhishek Mishra (University at Buffalo), Darsh Kiritbhai Nathawani (University at Buffalo), Joseph Pusztay (University at Buffalo), Alexander Stone (University at Buffalo), Feng-Mao Tsai (University at Buffalo)
\end{packedDescription}

\end{document}
