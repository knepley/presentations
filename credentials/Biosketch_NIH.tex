\documentclass[12pt]{article}
\topmargin-1.0in
\textheight10.0in
\textwidth6.5in
\oddsidemargin-0.5in
\evensidemargin0.0in

\newcommand{\itemskip}{\vspace*{8pt}}

\begin{document}

{\large
\centerline{\large\bf Biographical Sketch}
}

\itemskip\noindent\begin{tabular}{lcl}
{\bf Name}                  & Matthew G. Knepley & {\bf Position} \\
{\bf eRA Commons User Name} &                    & Senior Research Associate \\
                            &                    & Computation Institute \\
                            &                    & University of Chicago \\
\end{tabular}

\vskip0.5in
\noindent
{\bf Personal Statement}\\
Jaydeep Bardhan and I would like to establish a disciplined approach to bioelectrostatic approximation, which uses the mathematics to
greatly reduce the number of fit parameters and enable systematic adaptivity. It also provides insight into the
biological configurations which prove difficult for solvers and points to remedies. Armed with this simplicity, we can
attack more complex dieletric models, incorporating nonlocality, nonlinear response and saturation, and smooth
transitions.

We have successfully analyzed the BIBEE approximation model analytically in several common scenarios,
such as the Kirkwood model for both spherical and ellipsoidal geometry. We will extend this to the Poisson-Boltzmann
model, and incorporate a Stern layer. Another analytic tool, multiple reciprocity, will allow us to apply BIBEE to a
wider range of models, such as those exhibiting a smooth dieletric transition and nonlinear response. Moreover, we will
solidify these advances in community software, allowing easy replication and comparison to our results.

I have long experience developing widely used scientific software, most prominently the PETSc pacakge from Argonne
National Laboratory which has tens of thousands of users. I am architect of the PetFMM package for parallel fast
multipole method calculation, as well as a classical Density Functional Theory package for efficient simulation of
permeation through an ion channel. We developed several new multilevel algorithms, including a vast simplification
for the calculation of electrostatic screening in ionic solution which is crucial for obtaining good agreement with
experiment in our ion channel calculations.

\vskip0.5in
\noindent
{\bf Education and Training}\\
\begin{tabular}{|l|l|c|l|}
\hline
Institution and Location & Degree & Years & Field of Study \\
\hline
Case Western Reserve University & B.S     & 1990--1994 & Mathematical Physics \\
University of Minnesota         & M.S     & 1994--1996 & Computer Science \\
Purdue University               & Ph.D    & 1997--2000 & Computer Science \\
Argonne National Laboratory     & Postdoc & 2001--2004 & Computational Mathematics \\
\hline
\end{tabular}

\vskip0.5in
\noindent
{\bf Position}\\
\begin{tabular}{|l|l|c|l|}
\hline
Title & Institution & Years \\
\hline
Research Scientist                    & Akamai Technologies Inc.       & 2000--2001 \\
Postdoctoral Researcher               & Argonne National Laboratory    & 2001--2004 \\
Fellow                                & University of Chicago, CI      & 2008--2009 \\
Assistant Computational Mathematician & Argonne National Laboratory    & 2005--2009 \\
Assistant Professor                   & Rush University Medical Center & 2006--present \\
Assistant Professor                   & Monash University              & 2010--2013 \\
Senior Research Associate             & University of Chicago, CI      & 2009--present \\
\hline
\end{tabular}

\vskip0.5in
\noindent
{\bf Honors}\\
\begin{tabular}{|l|l|c|l|}
\hline
Award & Institution & Year \\
\hline
R\&D 100 Award                     & PETSc team                  & 2009 \\
J.~T. Oden Faculty Research Fellow & UT Austin                   & 2008 \\
Member                             & Upsilon Pi Epsilon          & 2000 \\
Givens Fellow                      & Argonne National Laboratory & 1997 \\
Member                             & Golden Key                  & 1994 \\
Member                             & Phi Beta Kappa              & 1994 \\
Member                             & Sigma Xi                    & 1993 \\
\hline
\end{tabular}

\vskip0.5in
\noindent
{\bf Publications}
\begin{enumerate}
\item {\it Analysis of fast boundary-integral approximations for modeling electrostatic contributions of molecular binding},
  Amy Kreienkamp, Lucy~Y. Liu, Mona~S. Minkara, Matthew~G. Knepley, Jaydeep~P. Bardhan, and Mala~L. Radhakrishnan,
  Molecular Based Mathematical Biology, 1:124--150, June 2013.

\item {\it Unstructured geometric multigrid in two and three dimensions on complex and graded meshes},
  Peter~R. Brune, Matthew~G. Knepley, and L.~Ridgway Scott,
  SIAM Journal on Scientific Computing, 35(1):A173--A191, 2013.

\item {\it Implementation of a multigrid solver on {GPU} for stokes equations with strongly variable viscosity based on Matlab and CUDA},
  Liang Zheng, Taras Gerya, Matthew~G. Knepley, David~A. Yuen, Huai Zhang, and Yaolin Shi,
  Int. Journal High Performance Computer Aapplications, 2013.

\item {\it Finite element integration on {GPUs}},
  Matthew~G. Knepley and Andy~R. Terrel,
  {ACM} Transactions on Mathematical Software, 39(2), 2013.

\item {\it Computational science and re-discovery: open-source implementations of ellipsoidal harmonics for problems in potential theory},
  Jaydeep~P. Bardhan and Matthew~G. Knepley,
  Computational Science \& Discovery, 5:014006, 2012.

\item {\it {PyClaw}: Accessible, extensible, scalable tools for wave propagation problems},
  David~I. Ketcheson, Kyle~T. Mandli, Aron~J. Ahmadia, Amal Alghamdi, Manuel~Quezada de~Luna, Matteo Parsani, Matthew~G. Knepley, and Matthew Emmett,
  SIAM Journal on Scientific Computing, 34(4):C210--C231, 2012.

\item {\it Mathematical analysis of the {BIBEE} approximation for molecular solvation: Exact results for spherical inclusions},
  Jaydeep~P. Bardhan and Matthew~G. Knepley,
  Journal of Chemical Physics, 135(12):124107--124117, 2011.

\item {\it Biomolecular electrostatics using a fast multipole {BEM} on up to 512 {GPU}s and a billion unknowns},
  Rio Yokota, Jaydeep~P. Bardhan, Matthew~G. Knepley, L.A. Barba, and Tsuyoshi Hamada,
  Computer Physics Communications, 182(6):1272--1283, 2011.

\item {\it An Efficient Algorithm for Classical Density Functional Theory in Three Dimensions: Ionic Solutions},
  Matthew G. Knepley, Dmitry A. Karpeev, Seth Davidovits, Robert S. Eisenberg, and Dirk Gillespie,
  Journal of Physical Chemistry, 132(12):124101--124111, 2010.

\item {\it {PetFMM} -- a dynamically load-balancing parallel fast multipole library},
  Felipe~A Cruz, Matthew~G Knepley, and L~A Barba.
  International Journal of Numerical Methods in Engineering, 85(4):403--428, 2010.

\item {\it {PetRBF} -- a parallel {O(N)} algorithm for radial basis function interpolation},
  Rio Yokota, L~A Barba, and Matthew~G Knepley,
  Computer Methods in Applied Mechanics and Engineering, 199(25-28):1793--1804, 2010.

\item {\it Mesh Algorithms for PDE with Sieve I: Mesh Distribution},
  Dmitry A. Karpeev, and Matthew G. Knepley,
  {\bf 17}(3), Scientific Programming, 2009.

\item {\it Bounding the Electrostatic Free Energies Associated with Linear Continuum Models of Molecular Solvation},
  Jaydeep P. Bardhan, Matthew G. Knepley, and Mihai Anitescu,
  {\bf 130}(10), Journal of Chemical Physics, 2008. PMID:19292524.

\item {\it Optimal Evaluation of Finite Element Matrices},
  Robert C. Kirby, Matthew G. Knepley, and L. Ridgway Scott,
  SIAM Journal on Scientific Computing, 27(3), pp.741--758, 2005.

\item {\it PETSc 2.0 Users Manual}, S. Balay, K. Buschelman, W. D. Gropp, D. Kaushik, 
  M. Knepley, L. C. McInnes, B. F. Smith, and H. Zhang,
  MCS Technical Report, ANL-95-11, Revision 3.0.0, 2008, 
  (see {\tt http://www.mcs.anl.gov/petsc}).
\end{enumerate}

\itemskip\noindent
{\bf Software Toolkits}
\begin{enumerate}

  \item A main focus of the MCS division at Argonne National Laboratory, in addition to research and publication, is the
creation and maintenance of industrial quality numerical and computational software. I am a lead developer of the
Portable, Extensible, Toolkit for Scientific Computation (PETSc) for PDE simulation, one of the premier packages in the
field.
  \begin{itemize}
    \item  300+ scientific publications have used PETSc, including combustion, brain surgery, subsurface flow,
blackholes, fusion, cardiology, and economics. See \begin{verbatim}http://www.mcs.anl.gov/petsc/petsc-as/publications\end{verbatim}

    \item  30 community scientific simulators are built on PETSc, in areas ranging from micromagnetics to geosciences

    \item  20,000 processes have been used efficiently on the Cray XT4

    \item  500 million unknowns used in the simulation of an actual human vertebra

    \item  3+ teraflops realized on groundwater reactive flow using PFLOTRAN

    \item  Used at all DOE laboratories and Supercomputing Centers, Boeing, Shell, GM, and downloaded thousands of times each release
  \end{itemize}
\end{enumerate}

\vskip0.5in
\noindent
{\bf Research Support}
\begin{enumerate}
\item Co-PI, NSF SI2, 2012--2015
\begin{description}
  \item This project is a collaboration with colleagues in Mechanical Engineering, and aims to develop robust solvers
    which function well on GPUs. Dr. Knepley participates in algorithm development, and incorporation into the PETSc
    framework.
\end{description}
\item Co-PI, DOE Applied Mathematics Research, 2012--2015
\begin{description}
  \item This project at ANL is focused on developing compsable solvers for multiscale, multiphysics
    problems. Dr. Knepley will investigate low-communication multigrid and block nonlinear solvers, and will participate
    in the development of a PETSc implementation.
\end{description}
\item Subcontract, NSF CIG, 2010--2015
\begin{description}
  \item This NSF center was formed to create and maintain computational tools for earth scientists. PETSc forms the
basis for almost every tool in the collection. As part of this effort, Dr. Knepley has greatly improved and expanded the
handling of unstructured and multiscale grids. This work was used for simulations in mantle convection, earthquake
physics, and magma migration.
\end{description}
\item Co-PI, DOE Math/CS Institute, 2009--2014
\begin{description}
  \item This is a major multi-institutional project to develop both theory and efficient implementations for nonlinear
multigrid, or FAS. Dr. Knepley will investigate the use of homotopic polynomial solvers as nonlinear smoothers, and will
participate in the development of a PETSc implementation.
\end{description}
\item Co-PI, NSF OCI STCI, 2008--2011
\begin{description}
  \item In collaboration with UT Austin, Dr. Knepley is investigating the use of formal logic environments, such as
FLAME, in elucidating the structure of finite element computation. This project is also looking at formal methods for
Krylov subspace solvers.
\end{description}
\item Co-PI, DOE SciDAC ISIC TOPS, 2001--2011
\begin{description}
  \item This is a major multi-institutional project to develop the next generation of computational tools. PETSc, from
ANL, is a major component of this effort. We have enabled PETSc to scale to the largest DOE machines, with over 30,000
processor cores, and Dr. Knepley has done work specifically on fast methods for PDE  and integral equations,
unstructured grids, and iterative solvers. This work has been applied to problems as diverse as groundwater flow,
fusion, and stellar collapse in other SciDAC projects.
\end{description}
\item Subcontract, NSF CIG, 2005--2009
\begin{description}
  \item This NSF center was formed to create and maintain computational tools for earth scientists. PETSc forms the
basis for almost every tool in the collection. As part of this effort, Dr. Knepley has greatly improved and expanded the
handling of unstructured and multiscale grids. This work was used for simulations in mantle convection, earthquake
physics, and magma migration.
\end{description}
\item Subcontract, DOE Reactor Core Modeling, 2005--2007
\begin{description}
  \item The new, flexible representation of unstructured grids developed by Drs. Knepley and Karpeev was applied to the
simulation of a full reactor core. This allowed easy parallelization of a leading method for compressible flow,
PC-ICE. They also developed scalable, nonlinear algeabraic solvers which would later be used in our work on ion channels.
\end{description}
\end{enumerate}

\end{document}
